#!/bin/sh

# remove the previous files

# unzip the files
unzip submit-hw2.zip -d final_project
cd final_project

# set up the credentials
cd ~
sudo mkdir .aws
cd ~/final_project
sudo scp credentials  ~/.aws
# run the program
