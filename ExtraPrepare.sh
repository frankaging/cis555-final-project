#!/bin/sh

rm -r store
ant pack

scp -i ~/.ec2/login.pem /home/cis555/workspace/CIS555-final/submit-hw2.zip ec2-user@ec2-54-92-232-109.compute-1.amazonaws.com:./
scp -i ~/.ec2/login.pem /home/cis555/workspace/CIS555-final/EC2Prepare.sh ec2-user@ec2-54-92-232-109.compute-1.amazonaws.com:./

scp -i ~/.ec2/login.pem /home/cis555/workspace/CIS555-final/submit-hw2.zip ec2-user@ec2-54-87-152-13.compute-1.amazonaws.com:./
scp -i ~/.ec2/login.pem /home/cis555/workspace/CIS555-final/EC2Prepare.sh ec2-user@ec2-54-87-152-13.compute-1.amazonaws.com:./

scp -i ~/.ec2/login.pem /home/cis555/workspace/CIS555-final/submit-hw2.zip ec2-user@ec2-52-90-1-183.compute-1.amazonaws.com:./
scp -i ~/.ec2/login.pem /home/cis555/workspace/CIS555-final/EC2Prepare.sh ec2-user@ec2-52-90-1-183.compute-1.amazonaws.com:./

scp -i ~/.ec2/login.pem /home/cis555/workspace/CIS555-final/submit-hw2.zip ec2-user@ec2-54-87-135-165.compute-1.amazonaws.com:./
scp -i ~/.ec2/login.pem /home/cis555/workspace/CIS555-final/EC2Prepare.sh ec2-user@ec2-54-87-135-165.compute-1.amazonaws.com:./

scp -i ~/.ec2/login.pem /home/cis555/workspace/CIS555-final/submit-hw2.zip ec2-user@ec2-54-160-118-31.compute-1.amazonaws.com:./
scp -i ~/.ec2/login.pem /home/cis555/workspace/CIS555-final/EC2Prepare.sh ec2-user@ec2-54-160-118-31.compute-1.amazonaws.com:./
