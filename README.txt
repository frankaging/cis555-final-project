Final Project CIS 555
Mini-Google Search Engine With Distributed Crawler, Indexing and Pageranking features.

Team Members:

Xiaozhuo Cheng
Zhengxuan Wu
Yufei Ye
Xiaoyan Zhu

Project Config:
-Project  Path: /home/cis555/workspace/CIS555-final

Instruction on running the crawler on EC2:
-Lanuch EC2 instances with AWS Linux Kernel With Default Setting And Correct SSH Pathway Settings.
-Config All The Public IP Address Into The Java Source Code (TODO: Auto Mode)
-Zip All The Source Code, And scp -i /path/to/login/pem ec2-user@public-dns:/path/to/copy
-Upzip The File Into Destination Folder
-(If First Time Running), You Need To Run The Auto-Config sh. Script To Config The Enviroment. It Auto Config Everything You Will Needed For Crawler.
-Go Back To The Project Folder
-Master Node: Using <ant runmaster> To Run
-Worker Node: Using <ant worker#number> To Run
-Jetty Server Will Launch On The Master Node
-You Will Have To Connect To IP:Port/status To Invoke The Worker, And You Can Monitoring On The Website
-The Data On EBS with EC2 Will Be Checking Into S3 Periodically.

Extra Credit: Content-seen is implemented to ensure two different urls with the same content will not be crawled, and we increment the hits when the content has been seen. The way we implemented is through splitting the content into several smaller pieces and hashing each component. 

-Indexing, Pagerank DB Will Start From Here!

Instruction on running the Indexing on EC2:
- 

Instruction on running the Pagerank on EC2:
- Using <ant rundownloader> to Download Files from S3
- Using <ant runpagerank> to Run PageRank Algorithm 
  - default input path: /storage
  - default output.txt path: /output/finalOutput
  - default BDB path: /dbRank

Instruction on running the Mini-Google on EC2:
- ant runsearch
The home page url is /search/home. Once user enters a word and hit search, the server will start computation and renders the result in a new page, a list of 200 results will be available immediately, if user goes beyond that, the program will compute again to retrieve the next 200 results. The algorithm we used is the harmonic mean of pageRank score and tf-idf, which is 2*(cosine-score * pagerank) / (cosine-score + pagerank).

It is embedded with Yelp API, every time a user runs a search, the result from
 our server will be displayed on the left and the top three ones from yelp will
be sitting on the right. It is also integrated with a location api to dertermine the current city of the user based on his/her ip address. Voice command is also available through Google Speech Recognition API.