package edu.upenn.cis455.hadoop.pagerank;

import java.io.IOException;

import org.apache.hadoop.io.Text;
import org.apache.hadoop.mapreduce.Mapper;

public class InitMapper extends Mapper<Text, Text, Text, Text> {
	   
	public void map(Text key, Text value, Context context) 
			throws IOException, InterruptedException {

		// String parent = key.toString();
		String children = value.toString().trim();		
		String childrenArray[] = children.split(" ");	
		
		// write: parent, 1
		if(!children.isEmpty())
			context.write(key, new Text("1"));
		
		// write: child, parent
		for(String child:childrenArray){
			if(!child.isEmpty())
				context.write(new Text(child), key);
		}
		
	}
}
