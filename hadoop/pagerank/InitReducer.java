package edu.upenn.cis455.hadoop.pagerank;

import java.io.IOException;
import java.util.ArrayList;

import org.apache.hadoop.io.Text;
import org.apache.hadoop.mapreduce.Reducer;

public class InitReducer extends Reducer<Text, Text, Text, Text> {

	public void reduce(Text key, Iterable<Text> values, Context context) 
			throws IOException, InterruptedException {

		String child = key.toString();

		ArrayList<String> parents = new ArrayList<String>();

		boolean hasNoChildren = false;
		for(Text value: values){
			// if no children: parent, 1(value)
			// else: child, parent(value)
			String parent = value.toString();
			if(parent.equals("1"))
				// this is not a parent string
				hasNoChildren = true;
			else
				parents.add(parent);
		}

		if(!hasNoChildren){
			for(String parent: parents)
				context.write(new Text(parent), new Text(child + " 1"));
		} 
		else {
			for(String parent: parents)
				context.write(new Text(parent), new Text(child + " 0"));	
		}
	}
	
}

