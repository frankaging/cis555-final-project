package edu.upenn.cis455.hadoop.pagerank;

import java.io.IOException;

import org.apache.hadoop.conf.Configuration;
import org.apache.hadoop.fs.Path;
import org.apache.hadoop.io.Text;
import org.apache.hadoop.mapreduce.Job;
import org.apache.hadoop.mapreduce.lib.input.FileInputFormat;
import org.apache.hadoop.mapreduce.lib.input.KeyValueTextInputFormat;
import org.apache.hadoop.mapreduce.lib.output.FileOutputFormat;
import org.apache.hadoop.mapreduce.lib.output.TextOutputFormat;

public class PageRank {
    // Ref: http://bnrg.cs.berkeley.edu/~randy/Courses/toce12-mr-assignment/WordCount.java
    public static void main(String[] args)
    throws IOException, ClassNotFoundException, InterruptedException {
        
        // TODO: path?
        String path = "s3://pagerank/";
        String time = "-" + System.currentTimeMillis();
        
        // init step
        Configuration initConfig = new Configuration();
        Job initJob = new Job(initConfig, "pagerank");
        initJob.setJarByClass(PageRank.class);
        // Set the key/value class for the map output data.
        initJob.setMapOutputKeyClass(Text.class);
        initJob.setMapOutputValueClass(Text.class);
        // Set the key/value class for the job output data.
        initJob.setOutputKeyClass(Text.class);
        initJob.setOutputValueClass(Text.class);
        // Set the Mapper/Reducer for the job.
        initJob.setMapperClass(InitMapper.class);
        initJob.setReducerClass(InitReducer.class);
        // Set the Input/Output Format for the job.
        initJob.setInputFormatClass(KeyValueTextInputFormat.class);
        initJob.setOutputFormatClass(TextOutputFormat.class);
        // Add a Path to the list of inputs/outputs for the map-reduce job.
        FileInputFormat.addInputPath(initJob, new Path(path + "input" + time));
        FileOutputFormat.setOutputPath(initJob, new Path(path + "initOutput" + time));
        initJob.waitForCompletion(true);
        
        // prepare step
        Configuration prepareConfig = new Configuration();
        Job prepareJob = new Job(prepareConfig, "pagerank");
        prepareJob.setJarByClass(PageRank.class);
        // Set the key/value class for the map output data.
        prepareJob.setMapOutputKeyClass(Text.class);
        prepareJob.setMapOutputValueClass(Text.class);
        // Set the key/value class for the job output data.
        prepareJob.setOutputKeyClass(Text.class);
        prepareJob.setOutputValueClass(Text.class);
        // Set the Mapper/Reducer for the job.
        prepareJob.setMapperClass(PrepareMapper.class);
        prepareJob.setReducerClass(PrepareReducer.class);
        // Set the Input/Output Format for the job.
        prepareJob.setInputFormatClass(KeyValueTextInputFormat.class);
        prepareJob.setOutputFormatClass(TextOutputFormat.class);
        // Add a Path to the list of inputs/outputs for the map-reduce job.
        FileInputFormat.addInputPath(prepareJob, new Path(path + "initOutput" + time));
        FileOutputFormat.setOutputPath(prepareJob, new Path(path + "prepareOutput" + time));
        prepareJob.waitForCompletion(true);
        
        // page rank step
        int iters = 30;
        for(int i=0; i<iters; i++){
            Configuration rankConfig = new Configuration();
            Job rankJob = new Job(rankConfig, "pagerank");
            rankJob.setJarByClass(PageRank.class);
            // Set the key/value class for the map output data.
            rankJob.setMapOutputKeyClass(Text.class);
            rankJob.setMapOutputValueClass(Text.class);
            // Set the key/value class for the job output data.
            rankJob.setOutputKeyClass(Text.class);
            rankJob.setOutputValueClass(Text.class);
            // Set the Mapper/Reducer for the job.
            rankJob.setMapperClass(RankMapper.class);
            rankJob.setReducerClass(RankReducer.class);
            // Set the Input/Output Format for the job.
            rankJob.setInputFormatClass(KeyValueTextInputFormat.class);
            rankJob.setOutputFormatClass(TextOutputFormat.class);
            
            if(i == 0){
                FileInputFormat.addInputPath(rankJob, new Path(path + "prepareOutput"+ time));
                FileOutputFormat.setOutputPath(rankJob, new Path(path + "rankOutput-" + i + time));
            }
            else{
                FileInputFormat.addInputPath(rankJob, new Path(path + "rankOutput-" + (i-1) + time));
                FileOutputFormat.setOutputPath(rankJob, new Path(path + "rankOutput-" + i + time));
            }
            job.waitForCompletion(true);
        }
        
        // final step
        Configuration finalConfig = new Configuration();
        Job finalJob = new Job(finalConfig, "pagerank");
        finalJob.setJarByClass(PageRank.class);
        // Set the key/value class for the map output data.
        finalJob.setMapOutputKeyClass(Text.class);
        finalJob.setMapOutputValueClass(Text.class);
        // Set the key/value class for the job output data.
        finalJob.setOutputKeyClass(Text.class);
        finalJob.setOutputValueClass(Text.class);
        // Set the Mapper/Reducer for the job.
        finalJob.setMapperClass(TestMapper.class);
        finalJob.setReducerClass(TestReducer.class);
        // Set the Input/Output Format for the job.
        finalJob.setInputFormatClass(KeyValueTextInputFormat.class);
        finalJob.setOutputFormatClass(TextOutputFormat.class);
        // Add a Path to the list of inputs/outputs for the map-reduce job.
        FileInputFormat.addInputPath(finalJob, new Path(path + "rankOutput-" + (iters-1) + time));
        FileOutputFormat.setOutputPath(finalJob, new Path(path + "rankOutput" + time));
        prepareJob.waitForCompletion(true);
        
    }
}
