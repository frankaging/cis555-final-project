package edu.upenn.cis455.hadoop.pagerank;

import java.io.IOException;

import org.apache.hadoop.io.Text;
import org.apache.hadoop.mapreduce.Mapper;

public class RankMapper extends Mapper<Text, Text, Text, Text> {
	
	public void map(Text key, Text value, Context context) 
			throws IOException, InterruptedException {
		
		// parent 1.0, child1 1  child2  0
		String parentStr = key.toString();
		String parentStrSplit[] = parentStr.split(" ");
		if (parentStrSplit.length < 2)
			return;
		String parent = parentStrSplit[0];
		Float parentRank = Float.parseFloat(parentStrSplit[1]);
		
		String children = value.toString();
		String childrenArray[] = children.split("  ");

		// write parent and children for iteration
		context.write(new Text(parent), value);

		// get number of children
		int numOfChildren = 0;
		String childSplit[];
		for(String child: childrenArray){
			if(child.contains(" ")){
				childSplit = child.split(" ");
				if(childSplit[1].equals("0"))
					numOfChildren += 1;
			}
		}

		// compute assigned value
		float rankAssigned = (float)0.0;
		if(numOfChildren == 0)
			rankAssigned = (float)0.15;
		else 
			rankAssigned = parentRank / (float)numOfChildren;

		// write assigned value
		for(String child: childrenArray){
			if(child.contains(" ")){
				childSplit = child.split(" ");
				context.write(new Text(childSplit[0]), new Text(Float.toString(rankAssigned)));
			}	
		}
		
	}
}