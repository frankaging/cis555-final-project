package edu.upenn.cis455.hadoop.pagerank;

import java.io.IOException;

import org.apache.hadoop.io.Text;
import org.apache.hadoop.mapreduce.Reducer;

public class RankReducer extends Reducer<Text, Text, Text, Text> {
	   
    public void reduce(Text key, Iterable<Text> values, Context context)
    throws IOException, InterruptedException {
        // node, assigned value
        
        String parent = key.toString();
        String children = "";
        
        float rank = (float)0.0;
        for(Text value: values){
            String valueString = value.toString();
            if(valueString.length() == 0)
                continue;
            else if(valueString.contains("  ")){
                children = valueString;
                continue;
            }
            else
                rank += Float.parseFloat(valueString);
        }
        rank = (float)0.15 + (float)0.85 * rank;
        
        context.write(new Text(parent + " " + Float.toString(rank)), new Text(children));
    }
}
