package edu.upenn.cis455.hadoop.pagerank;

import java.io.IOException;

import org.apache.hadoop.io.Text;
import org.apache.hadoop.mapreduce.Mapper;

public class TestMapper extends Mapper<Text, Text, Text, Text> {
	   
    public void map(Text key, Text value, Context context)
    throws IOException, InterruptedException {
        
        String URLandRank = key.toString();
        String URLandRankSplit[] = URLandRank.split(" ");
        String URL = URLandRankSplit[0];
        String Rank = URLandRankSplit[1];
        context.write(new Text(URL), new Text(Rank));
        
    }

}
