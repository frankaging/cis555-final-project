package edu.upenn.cis.stormlite.bolt;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.StringReader;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.UUID;

import org.apache.log4j.Logger;

import edu.upenn.cis.stormlite.OutputFieldsDeclarer;
import edu.upenn.cis.stormlite.TopologyContext;
import edu.upenn.cis.stormlite.routers.StreamRouter;
import edu.upenn.cis.stormlite.tuple.Fields;
import edu.upenn.cis.stormlite.tuple.Tuple;
import edu.upenn.cis.stormlite.tuple.Values;
import edu.upenn.cis455.mapreduce.worker.MainWorker;
import edu.upenn.cis455.storage.DBWrapper;
import edu.upenn.cis455.storage.Docs;
import edu.upenn.cis455.storage.VisitedUrl;
import edu.upenn.cis455.storage.XMLDoc;
/**
 * This bolt sends the head and get request to fetch content
 * @author Kevin Ye
 *
 */
public class CrawlerBolt implements IRichBolt {

	static Logger log = Logger.getLogger(CrawlerBolt.class);

	public final static String userAgent = "cis455crawler";

	String executorId = UUID.randomUUID().toString();

	Fields schema = new Fields("content", "url", "type");
	private OutputCollector collector;

	@Override
	public String getExecutorId() {
		// TODO Auto-generated method stub
		return this.executorId;
	}

	@Override
	public void declareOutputFields(OutputFieldsDeclarer declarer) {
		// TODO Auto-generated method stub
		declarer.declare(schema);
	}

	@Override
	public void cleanup() {
		// TODO Auto-generated method stub
	}

	/*
	 * crawl the site and send content to documentparser bolt(non-Javadoc)
	 * 
	 * @see
	 * edu.upenn.cis.stormlite.bolt.IRichBolt#execute(edu.upenn.cis.stormlite.
	 * tuple.Tuple)
	 */
	@Override
	public void execute(Tuple input) {
		// TODO Auto-generated method stub
		URLInfo url = input.getUrlByField("url");
		//if (DBWrapper.isVisit(url)) return;
		
		HttpClient client = new HttpClient();
		String completeUrl = url.getUrl();
		String host = url.getHostName();
		String filePath = url.getFilePath();

		RobotsTxtInfo robot = DBWrapper.getRobot(host);
		if (robot == null) {
			URLInfo robotInfo = new URLInfo(host, "/robots.txt", url.isHttps());
			String content = client.crawl(robotInfo);
			robot = robotsParser(content);
			
			robot.setUrl(host);
			DBWrapper.storeRobot(robot);
		}

		if (!checkIfAllow(url, robot)) {
			System.out.println(completeUrl + ": Restricted. Not Downloading");
			return;
		}

		long delay = 0;

		if (robot.crawlContainAgent(userAgent)) {
			delay = (long) robot.getCrawlDelay(userAgent);
		} else if (robot.crawlContainAgent("*")) {
			delay = (long) robot.getCrawlDelay("*");
		}
		delay *= 1000;
		long crawlDelay = delay;

		// get the correct delay time
		delay = checkDelay(delay, host);

		if (delay > 0) {
			try {
				MainWorker.urlQueue.push(url.getUrl());
			} catch (IOException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
			return;
		}
		
		String contentType = "text/html";
		String mainContent = "";

		DBWrapper.setCrawlTime(host, System.currentTimeMillis());
		
		// check if head request has been sent
//		if ((DBWrapper.containDocs(completeUrl) && DBWrapper.getDocs(completeUrl).getContent() == null)
//				|| DBWrapper.containXml(completeUrl) && DBWrapper.getXml(completeUrl).getContent() == null) {
//			mainContent = client.crawl(url);
//			if (DBWrapper.containDocs(completeUrl)){
//				contentType = DBWrapper.getDocs(completeUrl).getContentType();
//			} else {
//				contentType = DBWrapper.getXml(completeUrl).getContentType();
//			}
//			
//		} else {
//			System.out.println("heading " + url.getUrl());
		
		if (DBWrapper.isVisit(url)) {
			mainContent = client.crawl(url);
		} else {
			HashMap<String, List<String>> headers = client.sendHeadRequest(url);
			VisitedUrl visit = new VisitedUrl(url.getUrl());
			DBWrapper.storeUrl(visit);
			if (headers == null){
				return;
			}

			if (headers.containsKey("content-type")) {
				contentType = headers.get("content-type").get(0).split(";")[0];
			}
			if (!contentType.equals("text/html") && !contentType.endsWith("xml")) {
				return;
			}

//			if ((DBWrapper.containDocs(completeUrl) || DBWrapper.containXml(completeUrl)) && headers.containsKey("last-modified")) {
//				Docs existedDoc = DBWrapper.getDocs(completeUrl);
//				Date lastCrawled = existedDoc.getLastAccess();
//				SimpleDateFormat format = new SimpleDateFormat("EEE, dd MMM yyyy HH:mm:ss zzz");
//				try {
//					Date lastModified = format.parse(headers.get("last-modified").get(0));
//					if (lastModified.getTime() < lastCrawled.getTime()) {
//						System.out.println(completeUrl + ": Not modified");
//						return;
//					}
//				} catch (ParseException e) {
//					// TODO Auto-generated catch block
//					log.info(e);
//				}
//			}
			
			double contentSize = 0.0;
			if (headers.containsKey("content-length")) {
				contentSize = Double.parseDouble(headers.get("content-length").get(0));
			}

			if (contentSize / (Math.pow(1000, 2)) > 10) {
				System.out.println(completeUrl + ": Exceeds size");
				return;
			}
			
//			if (!isHtml(contentType, filePath)) {
//				XMLDoc xmlDoc = new XMLDoc(url.getUrl(), new Date(), contentType);
//				DBWrapper.putXmlDOM(xmlDoc);
//			} else {
//				Docs doc = new Docs(url.getUrl(), contentType, new Date());
//				DBWrapper.putDocs(doc);
//			}
			
			crawlDelay = checkDelay(crawlDelay, host);
//			
			// if there is delay, put it back to queue
			if (crawlDelay > 0) {
				try {
					MainWorker.urlQueue.push(url.getUrl());
				} catch (IOException e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
				}
				return;
			}
			
			mainContent = client.crawl(url);
		}

//		}

		if (mainContent.equals(""))
			return;

		collector.emit(new Values<Object>(mainContent, url, contentType));
	}
	
	/*
	 * Check if the doc is html
	 */
	public boolean isHtml(String contentType, String filePath) {
		if (contentType.equals("text/html") || filePath.endsWith(".html"))
			return true;

		return false;
	}
	
	/*
	 * check delay against current time
	 */
	public long checkDelay(long delay, String host){
		Date currentTime = new Date();
		if (DBWrapper.containRobot(host)) {
			long lastCrawled = DBWrapper.getNextCrawlTime(host);
			long timePassed = currentTime.getTime() - lastCrawled;
			delay -= timePassed;
			if (delay < 0) {
				delay = 0;
			}
		} else {
			delay = 0;
		}
		
		return delay;
	}

	@Override
	public void prepare(Map<String, String> stormConf, TopologyContext context, OutputCollector collector) {
		// TODO Auto-generated method stub
		this.collector = collector;

	}

	@Override
	public void setRouter(StreamRouter router) {
		// TODO Auto-generated method stub
		this.collector.setRouter(router);
	}

	@Override
	public Fields getSchema() {
		// TODO Auto-generated method stub
		return schema;
	}

	/*
	 * check if robot allow an url
	 */
	public boolean checkIfAllow(URLInfo url, RobotsTxtInfo robot) {
		String path = url.getFilePath();
		List<String> allowed = new ArrayList<String>();
		List<String> disallowed = new ArrayList<String>();

		if (robot.containsUserAgent(userAgent)){
			if (robot.getAllowedLinks(userAgent) != null){
				allowed.addAll(robot.getAllowedLinks(userAgent));
			}
			if (robot.getDisallowedLinks(userAgent) != null){
				disallowed.addAll(robot.getDisallowedLinks(userAgent));
			}
		} else if (robot.containsUserAgent("*")){
			if (robot.getAllowedLinks("*") != null){
				allowed.addAll(robot.getAllowedLinks("*"));
			}
			if (robot.getDisallowedLinks("*") != null){
				disallowed.addAll(robot.getDisallowedLinks("*"));
			}
		}

		if (comparePath(allowed, path))
			return true;
		if (comparePath(disallowed, path))
			return false;

		return true;
	}

	/*
	 * compare two path and see if they match up
	 */
	public boolean comparePath(List<String> robots, String path) {
		if (robots == null || robots.size() == 0)
			return false;
		for (String robotPath : robots) {
			if (robotPath.equals("/"))
				return true;
			if (robotPath.equals(path))
				return true;

			// adding wildcard for matching purpose
			if (robotPath.endsWith("/"))
				robotPath += "*";
			String[] robotElements = robotPath.split("/");
			String[] urlElements = path.split("/");
			if (robotElements.length > urlElements.length)
				continue;

			int i = 0;
			boolean match = true;
			while (i < robotElements.length && i < urlElements.length) {
				if (!robotElements[i].equals(urlElements[i]) && !robotElements[i].equals("*")) {
					match = false;
					break;
				}
				i++;
			}

			if (match) {
				// if url has more sub path
				if (i >= robotElements.length && i <= urlElements.length) {
					return true;
				}
			}
		}

		return false;
	}

	/*
	 * parse robots.txt
	 */
	public RobotsTxtInfo robotsParser(String content) {
		RobotsTxtInfo robot = new RobotsTxtInfo();
//		String notFound = "Page Not Found";
		if (content.equals("")) return robot;
		BufferedReader bf = new BufferedReader(new StringReader(content));
		String line = null;
		try {
			line = bf.readLine();
			String userAgent = "";
			while (line != null) {
				String trimLine = line.trim();
				if (trimLine.startsWith("#") || trimLine.equals("")) {
					line = bf.readLine();
					continue;
				}
				int splitIndex = trimLine.indexOf(":");
				if (splitIndex == 0 || splitIndex == -1) {
					line = bf.readLine();
					continue;
				}
				String key = trimLine.substring(0, splitIndex).trim().toLowerCase();
				String value = trimLine.substring(splitIndex+1).trim();
				if (key.equals("user-agent")) {
					userAgent = value;
					robot.addUserAgent(value);
				} else if (key.equals("disallow")) {
					robot.addDisallowedLink(userAgent, value);
				} else if (key.equals("allow")) {
					robot.addAllowedLink(userAgent, value);
				} else if (key.equals("crawl-delay")) {
					try {
						robot.addCrawlDelay(userAgent, (int)Float.parseFloat(value));
					} catch (Exception e) {
						robot.addCrawlDelay(userAgent, 0);
					}
				}
				line = bf.readLine();
			}
		} catch (IOException e) {
			// TODO Auto-generated catch block
			log.info(e);
		}

		return robot;
	}

}
