package edu.upenn.cis.stormlite.bolt;

import java.io.File;
import java.io.IOException;
import java.io.UnsupportedEncodingException;
import java.math.BigInteger;
import java.nio.charset.Charset;
import java.nio.charset.CharsetEncoder;
import java.security.MessageDigest;
import java.security.NoSuchAlgorithmException;
import java.util.ArrayList;
import java.util.HashSet;
import java.util.List;
import java.util.Map;
import java.util.Set;
import java.util.UUID;
import java.util.concurrent.locks.ReentrantLock;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

import org.apache.log4j.Logger;

import com.amazonaws.AmazonClientException;
import com.amazonaws.AmazonServiceException;

import edu.upenn.cis.stormlite.OutputFieldsDeclarer;
import edu.upenn.cis.stormlite.TopologyContext;
import edu.upenn.cis.stormlite.distributed.WorkerHelper;
import edu.upenn.cis.stormlite.routers.StreamRouter;
import edu.upenn.cis.stormlite.tuple.Fields;
import edu.upenn.cis.stormlite.tuple.Tuple;
import edu.upenn.cis.stormlite.tuple.Values;
import edu.upenn.cis455.storage.Content;
import edu.upenn.cis455.storage.DBWrapper;
import edu.upenn.cis455.storage.S3CreateBucket;
import edu.upenn.cis455.storage.S3FileWriter;
/**
 * This bolt parses the document and extracts all links from it
 * @author Kevin Ye
 *
 */
public class DocumentParserBolt implements IRichBolt {
	static Logger log = Logger.getLogger(DocumentParserBolt.class);

	String executorId = UUID.randomUUID().toString();
	Fields schema = new Fields("urls", "urlInfo");

	String[] workers;
	int workerIndex;
	String address;

	public static int S3SYNCCOUNTER = 0;

	public static int COUNTERMAX = 500;

	static boolean S3TRIGEER = true;

	private OutputCollector collector;

	TopologyContext context;

	String directory;
	
	static int counter = -1; // a global counter
	
	static long totalCounter = -1;

    static ReentrantLock counterLock = new ReentrantLock(true); // enable fairness policy

	@Override
	public String getExecutorId() {
		// TODO Auto-generated method stub
		return executorId;
	}

	@Override
	public void declareOutputFields(OutputFieldsDeclarer declarer) {
		// TODO Auto-generated method stub
		declarer.declare(schema);
	}

	@Override
	public void cleanup() {
		// TODO Auto-generated method stub
		// DBWrapper.close();
	}

	
	static void incrementCounter(){
        counterLock.lock();

        // Always good practice to enclose locks in a try-finally block
        try{
            System.out.println(Thread.currentThread().getName() + ": " + counter);
            counter++;
        }finally{
             counterLock.unlock();
        }
     }

	
	
	/*
	 * Store the crawled content into database
	 * 
	 * @see
	 * edu.upenn.cis.stormlite.bolt.IRichBolt#execute(edu.upenn.cis.stormlite.
	 * tuple.Tuple)
	 */
	@Override
	public void execute(Tuple input) {
		// TODO Auto-generated method stub

		String content = input.getStringByField("content");
		URLInfo urlInfo = input.getUrlByField("url");
		String filePath = urlInfo.getFilePath();
		String contentType = input.getStringByField("type");

		// check hash
		String hashContent = "";
		try {
			hashContent = getDocHash(content);
			if (DBWrapper.isContentSeen(hashContent)){
//				String lastUrl = DBWrapper.getContentUrl(hashContent);
//				if (DBWrapper.getDocs(lastUrl) != null && DBWrapper.getDocs(lastUrl).getContent() != null && 
//						DBWrapper.getDocs(lastUrl).getContent().length() == content.length()){
//					DBWrapper.addOneHits(lastUrl);
					//System.out.println(urlInfo.getUrl() + ": " + DBWrapper.getContentUrl(hashContent));
					//System.out.println("Content Seen!!!!!");
				return;
//				}

			}
		} catch (NoSuchAlgorithmException e1) {
			// TODO Auto-generated catch block
			e1.printStackTrace();
		}
//		//System.out.println(urlInfo.getUrl() + " : " + hashContent);
		Content con = new Content(hashContent, urlInfo.getUrl());
		DBWrapper.addContent(con);

		
		counterLock.lock();
		counter++;
		totalCounter++;
		if (counter ==  COUNTERMAX&& S3TRIGEER) {
			counter=-1;
			//create the S3 location if not exist
			String S3BucketName = "crawlerhistory" + Integer.toString(workerIndex);
			try {
				S3CreateBucket.createBucket(S3BucketName);
			} catch (IOException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}

			// upload all the file with-in the store directory
			try {
				String directory = "upload" + Integer.toString(workerIndex) + "/";
				File dir = new File("store/" + directory);
				S3FileWriter.WriteToS3(dir, S3BucketName);
			} catch (AmazonServiceException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			} catch (AmazonClientException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			} catch (InterruptedException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			} catch (IOException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}

			//System.out.println("S3 Uploading Finished!");
			return;
		} else {
			context.setCount(totalCounter);
			//System.out.println(S3SYNCCOUNTER);
			// after crawling all the links in that webpage
			// we need to set that this link has been exploited,
			// and add it back to the database with content
			if (DBWrapper.containXml(urlInfo.getUrl())) {
				String prepared = S3FileWriter.prepareFileLineDoc(urlInfo.getUrl(), content);
				S3FileWriter.writeToDocFile(prepared);
			} else {
				//String prepared = S3FileWriter.prepareFileLineDoc(urlInfo.getUrl(), new String(content.getBytes("UTF-8")));
				String prepared = S3FileWriter.prepareFileLineDoc(urlInfo.getUrl(), content);
				S3FileWriter.writeToDocFile(prepared);
			}

			List<String> list = new ArrayList<String>();
			if (isHtml(contentType, filePath))
				try {
					list = extractLinks(content, urlInfo);
				} catch (UnsupportedEncodingException e1) {
					// TODO Auto-generated catch block
					e1.printStackTrace();
				}
			S3FileWriter.writeToUrlFile(S3FileWriter.prepareFileLineUrlList(urlInfo.getUrl(), list));
			
			//System.out.println("Next!!");
			collector.emit(new Values<Object>(list, urlInfo));
		}
		counterLock.unlock();

		
	}

	public boolean isHtml(String contentType, String filePath) {
		if (contentType.equals("text/html") || filePath.endsWith(".html"))
			return true;

		return false;
	}

	/*
	 * to check if a string only contains ASCII 
	 */
	static CharsetEncoder asciiEncoder = 
			Charset.forName("US-ASCII").newEncoder(); // or "ISO-8859-1" for ISO Latin 1

	public static boolean isPureAscii(String v) {
		return asciiEncoder.canEncode(v);
	}


	/*
	 * extract links
	 */
	public List<String> extractLinks(String content, URLInfo urlInfo) throws UnsupportedEncodingException {
		Set<String> l = new HashSet<String>();
		String completeUrl = urlInfo.getUrl();
		if (completeUrl.endsWith(".html") || completeUrl.endsWith(".html/")) {
			int i = completeUrl.lastIndexOf(".html");
			int lastSlash = completeUrl.lastIndexOf("/", i);
			completeUrl = completeUrl.substring(0, lastSlash + 1);
		}
		String regex = "href\\s?=\\s?\"([^\"]+)\"";
		Pattern pattern = Pattern.compile(regex, Pattern.CASE_INSENSITIVE);
		Matcher matcher = pattern.matcher(content);
		int index = 0;
		while (matcher.find(index)) {
			String link = matcher.group(1);
			String linkEncoded = new String(link.getBytes("UTF-8"));
			
			String s = linkEncoded.toString();
			String newLink = s;
			// parse partial url
			if (s.startsWith("//")) {
				if (urlInfo.isHttps()) {
					newLink = "https:" + s;
				} else {
					newLink = "http:" + s;
				}
			} else if (!s.startsWith("http")) {
				newLink = completeUrl;
				if (!completeUrl.endsWith("/")) {
					newLink += "/";
				}
				newLink += s;
			}
			l.add(newLink);
			index = matcher.end();
			//System.out.println("!");
		}
		
		List<String> res = new ArrayList<>();
		for (String link: l){
			res.add(link);
		}
		
		return res;
	}

	@Override
	public void prepare(Map<String, String> stormConf, TopologyContext context, OutputCollector collector) {
		// TODO Auto-generated method stub

		this.collector = collector;
		this.context = context;
		workers = WorkerHelper.getWorkers(stormConf);
		workerIndex = Integer.valueOf(stormConf.get("workerIndex"));

	}

	@Override
	public void setRouter(StreamRouter router) {
		// TODO Auto-generated method stub
		collector.setRouter(router);
	}

	@Override
	public Fields getSchema() {
		// TODO Auto-generated method stub
		return this.schema;
	}

	// getting the hashchode of the content string

	/**
	 * 
	 * @param content
	 * @return - a string of integer hashcode, deliminator ","
	 * @throws NoSuchAlgorithmException 
	 */
	public String getDocHash(String content) throws NoSuchAlgorithmException {
		String bigHashCode= "";
		if (content.length() < 5000) 
		{
			bigHashCode = hashing(content);
		} 
		else
		{
			int chunckSize = content.length()/5000;
			for (int i = 0; i < chunckSize - 1; i++) {
				String subContent = content.substring(i*5000, (i+1)*5000);
				bigHashCode += hashing(subContent) + ".";
			}
		}
		return bigHashCode;
	}

	public String hashing(String s) throws NoSuchAlgorithmException {
		String plaintext = s;
		MessageDigest m;
		String hashtext="";
		m = MessageDigest.getInstance("MD5");
		m.reset();
		m.update(plaintext.getBytes());
		byte[] digest = m.digest();
		BigInteger bigInt = new BigInteger(1,digest);
		hashtext = bigInt.toString(16);
		// Now we need to zero pad it if you actually want the full 32 chars.
		while(hashtext.length() < 32){
			hashtext = "0"+hashtext;
		}
		return hashtext;
	}

}
