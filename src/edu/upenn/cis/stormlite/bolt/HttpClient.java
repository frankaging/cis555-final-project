package edu.upenn.cis.stormlite.bolt;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.io.PrintWriter;
import java.net.MalformedURLException;
import java.net.Socket;
import java.net.URL;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.net.ssl.HttpsURLConnection;

import org.apache.log4j.Logger;

import edu.upenn.cis455.storage.DBWrapper;
import edu.upenn.cis455.storage.WebDocs;

/**
 * This class is to send request to page being crawled
 * 
 * @author kevin ye
 *
 */
public class HttpClient {

	static Logger log = Logger.getLogger(HttpClient.class);
	final static String userAgent = "cis455crawler";

	public HttpClient() {

	}

	/*
	 * send head request first
	 */
	public HashMap<String, List<String>> sendHeadRequest(URLInfo url) {
		Socket socket = null;
		PrintWriter out = null;
		BufferedReader bf = null;
		HashMap<String, List<String>> headers = new HashMap<String, List<String>>();
		HttpsURLConnection connection = null;
		if (url.isHttps()) {
			try {
				URL httpsUrl = new URL(url.getUrl());
				connection = (HttpsURLConnection) httpsUrl.openConnection();
				connection.setDoOutput(true);
				connection.setDoInput(true);
				//connection.setConnectTimeout(10000);
				//connection.setReadTimeout(10000);
				connection.setRequestMethod("HEAD");
				connection.setRequestProperty("User-Agent", userAgent);
				
				String completeUrl = url.getUrl();
				if (DBWrapper.containDocs(completeUrl) || DBWrapper.containXml(completeUrl)) {
					WebDocs existedDoc = null;
					if (DBWrapper.containDocs(completeUrl)){
						existedDoc = DBWrapper.getDocs(completeUrl);
					} else {
						existedDoc = DBWrapper.getXml(completeUrl);
					}
					Date lastCrawled = existedDoc.getLastAccess();
					SimpleDateFormat format = new SimpleDateFormat("EEE, dd MMM yyyy HH:mm:ss zzz");
					String dateSent = format.format(lastCrawled);
					connection.setRequestProperty("If-Modified-Since", dateSent);
				}
				connection.setRequestProperty("Connection", "close");
				connection.connect();
				
				int status = connection.getResponseCode();
				if (status == 304) {
					System.out.println(url.getUrl() + ": Not modified");
					return null;
				}
				if (status == 404 || status == 500) {
					System.out.println(url.getUrl() + ": Not crawlable at this moment");
					return null;
				}
				
				Map<String, List<String>> tempMap = connection.getHeaderFields();
				for (String key : tempMap.keySet()) {
					if (key == null)
						continue;
					String newHeader = key.toLowerCase();
					headers.put(newHeader, tempMap.get(key));
				}

			} catch (MalformedURLException e) {
				// TODO Auto-generated catch block
				log.info(e);
			} catch (IOException e) {
				// TODO Auto-generated catch block
				log.info(e);
			}

		} else {
			try {
				socket = new Socket(url.getHostName(), url.getPortNo());
				//socket.setSoTimeout(10000);
				out = new PrintWriter(socket.getOutputStream(), true);
				bf = new BufferedReader(new InputStreamReader(socket.getInputStream()));

				String request = "HEAD " + url.getFilePath() + " HTTP/1.1\r\n";
				request += "Host: " + url.getHostName() + "\r\n";
				request += "User-Agent: cis455crawler\r\n";
				String completeUrl = url.getUrl();
				if (DBWrapper.containDocs(completeUrl) || DBWrapper.containXml(completeUrl)) {
					WebDocs existedDoc = null;
					if (DBWrapper.containDocs(completeUrl)){
						existedDoc = DBWrapper.getDocs(completeUrl);
					} else {
						existedDoc = DBWrapper.getXml(completeUrl);
					}
					Date lastCrawled = existedDoc.getLastAccess();
					SimpleDateFormat format = new SimpleDateFormat("EEE, dd MMM yyyy HH:mm:ss zzz");
					String dateSent = format.format(lastCrawled);
					request += "If-Modified-Since: " + dateSent + "\r\n";
				}
				request += "Connection: Close\r\n\r\n";
				out.print(request);
				out.flush();

				String line;
				line = bf.readLine();
				while (line != null) {
					if (line.toLowerCase().contains("http/")) {
						String[] arr = line.split("\\s+");
						List<String> l = new ArrayList<String>();
						l.add(arr[1].trim());
						headers.put("status", l);
					}
					if (line.contains(":")) {
						String[] arr = line.split(":");
						if (arr.length < 2) {
							continue;
						}
						String value = arr[1].trim();
						for (int i = 2; i < arr.length; i++) {
							value += ":" + arr[i].trim();
						}
						String[] allHeaders = value.split(";");
						headers.put(arr[0].toLowerCase(), Arrays.asList(allHeaders));
					}
					line = bf.readLine();
				}

			} catch (IOException e) {
				// error early return
				log.info(e);
				return headers;
			}
		}

		if (socket != null) {
			try {
				socket.close();
			} catch (IOException e) {
				// TODO Auto-generated catch block
				log.info(e);
			}
		} else if (connection != null) {
			connection.disconnect();
		}

		return headers;
	}

	/*
	 * sends get request and fetch the content
	 */
	public String crawl(URLInfo url) {
		Socket socket = null;
		PrintWriter out = null;
		BufferedReader bf = null;
		String result = "";
		HttpsURLConnection connection = null;

		if (url.isHttps()) {
			try {
				URL httpsUrl = new URL(url.getUrl());
				connection = (HttpsURLConnection) httpsUrl.openConnection();
				connection.setDoOutput(true);
				connection.setDoInput(true);
				//connection.setConnectTimeout(10000);
				//connection.setReadTimeout(10000);
				connection.setRequestMethod("GET");
				connection.setRequestProperty("User-Agent", userAgent);
				connection.setRequestProperty("Connection", "close");
				connection.connect();
				
				int status = connection.getResponseCode();
				if (status != 200){
					return "";
				}

				bf = new BufferedReader(new InputStreamReader(connection.getInputStream()));
				String line = bf.readLine();

				System.out.println(url.getUrl() + ": Downloading");
//				line = bf.readLine();
				while (line != null) {
					result += line;
					result += "\n";
					line = bf.readLine();
				}

			} catch (MalformedURLException e) {
				// TODO Auto-generated catch block
				log.info(e);
			} catch (IOException e) {
				// TODO Auto-generated catch block
				log.info(e);
			} finally {
				connection.disconnect();
			}
		} else {
			try {
				socket = new Socket(url.getHostName(), url.getPortNo());
				//socket.setSoTimeout(10000);
				out = new PrintWriter(socket.getOutputStream(), true);

				String path = url.getFilePath();
				String request = "GET " + path + " HTTP/1.1\r\n";
				request += "Host: " + url.getHostName() + "\r\n";
				request += "User-Agent: cis455crawler\r\n";
				request += "Connection: Close\r\n\r\n";
				
				out.print(request);
				out.flush();

				bf = new BufferedReader(new InputStreamReader(socket.getInputStream()));
				String line = bf.readLine();
				String status = "";
				while (line != null && !line.equals("")) {
					if (line.toLowerCase().contains("http/1")) {
						String[] arr = line.split("\\s+");
						status = arr[1];
					}
					line = bf.readLine();
				}
				
				if (!status.contains("200")){
					return result;
				}

				if (status.contains("304")) {
					System.out.println(url.getUrl() + ": Not modified");
					return result;
				}
				System.out.println(url.getUrl() + ": Downloading");
				line = bf.readLine();
				while (line != null) {
					result += line;
					result += "\n";
					line = bf.readLine();
				}

			} catch (IOException e) {
				// TODO Auto-generated catch block
				log.info(e);
			} finally {
				if (socket != null){
					try {
						socket.close();
					} catch (IOException e) {
						// TODO Auto-generated catch block
						log.info(e);
					}
				}
				
			}
		}

		return result;

	}

}
