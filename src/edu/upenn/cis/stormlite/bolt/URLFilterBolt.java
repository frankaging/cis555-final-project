package edu.upenn.cis.stormlite.bolt;

import java.io.File;
import java.io.IOException;
import java.io.OutputStream;
import java.math.BigInteger;
import java.net.HttpURLConnection;
import java.net.URL;
import java.security.MessageDigest;
import java.security.NoSuchAlgorithmException;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.UUID;

import com.amazonaws.AmazonClientException;
import com.amazonaws.AmazonServiceException;
import com.fasterxml.jackson.databind.ObjectMapper;

import edu.upenn.cis.stormlite.OutputFieldsDeclarer;
import edu.upenn.cis.stormlite.TopologyContext;
import edu.upenn.cis.stormlite.distributed.WorkerHelper;
import edu.upenn.cis.stormlite.routers.StreamRouter;
import edu.upenn.cis.stormlite.tuple.Fields;
import edu.upenn.cis.stormlite.tuple.Tuple;
import edu.upenn.cis.stormlite.tuple.Values;
import edu.upenn.cis455.storage.Content;
import edu.upenn.cis455.storage.DBWrapper;
import edu.upenn.cis455.storage.S3CreateBucket;
import edu.upenn.cis455.storage.S3FileWriter;

/**
 * This bolt filters out url that are restricted
 * 
 * @author Kevin Ye
 *
 */
public class URLFilterBolt implements IRichBolt {

	String executorId = UUID.randomUUID().toString();

	private OutputCollector collector;
	String[] workers;
	int workerIndex;
	String address;
	ObjectMapper mapper = new ObjectMapper();

	TopologyContext context;

	boolean isEndOfStream = false;

	Fields schema = new Fields("url");

	@Override
	public String getExecutorId() {
		// TODO Auto-generated method stub
		return executorId;
	}

	@Override
	public void declareOutputFields(OutputFieldsDeclarer declarer) {
		declarer.declare(schema);
	}

	@Override
	public void cleanup() {
		// TODO Auto-generated method stub
	}

	@Override
	public void execute(Tuple input) {
		// TODO Auto-generated method stub
		List<String> urls = input.getSetByField("urls");
		URLInfo urlInfo = input.getUrlByField("urlInfo");

		//String content = input.getStringByField("content");

		enqueueUrl(urls, urlInfo);
	}

	public void enqueueUrl(List<String> l, URLInfo urlInfo) {
		for (String s : l) {

			URLInfo newUrl = new URLInfo(s);
			
			if (DBWrapper.isVisit(newUrl)) continue;
			if (newUrl.getHostName() == null) continue;
			RobotsTxtInfo robot = DBWrapper.getRobot(newUrl.getHostName());
			if (!checkIfAllow(newUrl, robot)) {
				System.out.println(newUrl.getUrl() + ": Restricted. Not Downloading");
				continue;
			}
			
			//DBWrapper.addChildDoc(urlInfo.getUrl(), newUrl.getUrl());

			String host = newUrl.getHostName();
			int hash = host.hashCode();

			hash = hash % workers.length;
			if (hash < 0) {
				hash += workers.length;
			}

			//System.out.println("HashCode: " + hash);
			
			// send to other workers if necessary
			if (hash == workerIndex) {
				collector.emit(new Values<Object>(newUrl));
			} else {
				this.address = workers[hash];
				Fields sentSchema = new Fields("url");
				Values<Object> values = new Values<Object>(newUrl.getUrl());
				Tuple input = new Tuple(sentSchema, values);
				try {
					send(input);
				} catch (IOException e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
				}
			}

		}
	}

	/**
	 * Sends the data along a socket
	 * 
	 * @param stream
	 * @param tuple
	 * @throws IOException
	 */
	private synchronized void send(Tuple tuple) throws IOException {

		URL url = new URL(address + "/pushdata/" + "URLMAP_BOLT");

		HttpURLConnection conn = (HttpURLConnection) url.openConnection();
		conn.setRequestProperty("Content-Type", "application/json");
		String jsonForTuple = mapper.writerWithDefaultPrettyPrinter().writeValueAsString(tuple);

		// TODO: send this to /pushdata/{stream} as a POST!
		conn.setRequestMethod("POST");
		conn.setConnectTimeout(5000);
		conn.setRequestProperty("Content-Type", "application/json");
		conn.setDoOutput(true);
		OutputStream os = conn.getOutputStream();
		byte[] toSend = jsonForTuple.getBytes();
		os.write(toSend);
		os.flush();
		if (conn.getResponseCode() != HttpURLConnection.HTTP_OK) {
			throw new RuntimeException("Worker Communication fail");
		}
		os.close();
		conn.disconnect();
	}

	/*
	 * check if robot allow an url
	 */
	public boolean checkIfAllow(URLInfo url, RobotsTxtInfo robot) {
		if (robot == null)
			return true;
		String path = url.getFilePath();
		List<String> allowed = new ArrayList<String>();
		List<String> disallowed = new ArrayList<String>();

		if (robot.containsUserAgent(CrawlerBolt.userAgent)){
			if (robot.getAllowedLinks(CrawlerBolt.userAgent) != null){
				allowed.addAll(robot.getAllowedLinks(CrawlerBolt.userAgent));
			}
			if (robot.getDisallowedLinks(CrawlerBolt.userAgent) != null){
				disallowed.addAll(robot.getDisallowedLinks(CrawlerBolt.userAgent));
			}
		} else if (robot.containsUserAgent("*")){
			if (robot.getAllowedLinks("*") != null){
				allowed.addAll(robot.getAllowedLinks("*"));
			}
			if (robot.getDisallowedLinks("*") != null){
				disallowed.addAll(robot.getDisallowedLinks("*"));
			}
		}

		if (comparePath(allowed, path))
			return true;
		if (comparePath(disallowed, path))
			return false;

		return true;
	}

	/*
	 * compare two path and see if they match up
	 */
	public boolean comparePath(List<String> robots, String path) {
		if (robots == null || robots.size() == 0)
			return false;
		for (String robotPath : robots) {
			if (robotPath.equals("/"))
				return true;
			if (robotPath.equals(path))
				return true;

			// adding wildcard for matching purpose
			if (robotPath.endsWith("/"))
				robotPath += "*";
			String[] robotElements = robotPath.split("/");
			String[] urlElements = path.split("/");
			if (robotElements.length > urlElements.length)
				continue;

			int i = 0;
			boolean match = true;
			while (i < robotElements.length && i < urlElements.length) {
				if (!robotElements[i].equals(urlElements[i]) && !robotElements[i].equals("*")) {
					match = false;
					break;
				}
				i++;
			}

			if (match) {
				// if url has more sub path
				if (i >= robotElements.length && i <= urlElements.length) {
					return true;
				}
			}
		}

		return false;
	}

	@Override
	public void prepare(Map<String, String> stormConf, TopologyContext context, OutputCollector collector) {
		// TODO Auto-generated method stub
		this.collector = collector;
		this.context = context;
		workers = WorkerHelper.getWorkers(stormConf);
		workerIndex = Integer.valueOf(stormConf.get("workerIndex"));
		mapper.enableDefaultTyping(ObjectMapper.DefaultTyping.NON_FINAL);
	}

	@Override
	public void setRouter(StreamRouter router) {
		// TODO Auto-generated method stub
		this.collector.setRouter(router);
	}

	@Override
	public Fields getSchema() {
		// TODO Auto-generated method stub
		return schema;
	}



}
