package edu.upenn.cis.stormlite.bolt;

import java.io.IOException;
import java.util.Map;
import java.util.UUID;

import edu.upenn.cis.stormlite.OutputFieldsDeclarer;
import edu.upenn.cis.stormlite.TopologyContext;
import edu.upenn.cis.stormlite.routers.StreamRouter;
import edu.upenn.cis.stormlite.tuple.Fields;
import edu.upenn.cis.stormlite.tuple.Tuple;
import edu.upenn.cis455.mapreduce.worker.MainWorker;
import edu.upenn.cis455.storage.DBWrapper;
/**
 * This bolt maps the url based on its host
 * @author Kevin Ye
 *
 */
public class URLMapBolt implements IRichBolt {

	String executorId = UUID.randomUUID().toString();

	private OutputCollector collector;

	TopologyContext context;

	@Override
	public String getExecutorId() {
		// TODO Auto-generated method stub
		return this.executorId;
	}

	@Override
	public void declareOutputFields(OutputFieldsDeclarer declarer) {
		// TODO Auto-generated method stub

	}

	@Override
	public void cleanup() {
		// TODO Auto-generated method stub

	}

	@Override
	public void execute(Tuple input) {
		
		// TODO Auto-generated method stub
		URLInfo urlInfo = input.getUrlByField("url");
		try {
			if (!DBWrapper.isVisit(urlInfo))
				MainWorker.urlQueue.push(urlInfo.getUrl());
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}

	@Override
	public void prepare(Map<String, String> stormConf, TopologyContext context, OutputCollector collector) {
		// TODO Auto-generated method stub
		this.collector = collector;
	}

	@Override
	public void setRouter(StreamRouter router) {
		// TODO Auto-generated method stub
		this.collector.setRouter(router);
	}

	@Override
	public Fields getSchema() {
		// TODO Auto-generated method stub
		return null;
	}

}
