package edu.upenn.cis.stormlite.distributed;

import java.io.IOException;
import java.io.OutputStream;
import java.net.HttpURLConnection;
import java.net.URL;
import java.util.Map;
import java.util.UUID;

import org.apache.log4j.Logger;

import com.fasterxml.jackson.databind.ObjectMapper;

import edu.upenn.cis.stormlite.OutputFieldsDeclarer;
import edu.upenn.cis.stormlite.TopologyContext;
import edu.upenn.cis.stormlite.bolt.IRichBolt;
import edu.upenn.cis.stormlite.bolt.OutputCollector;
import edu.upenn.cis.stormlite.bolt.URLInfo;
import edu.upenn.cis.stormlite.routers.StreamRouter;
import edu.upenn.cis.stormlite.tuple.Fields;
import edu.upenn.cis.stormlite.tuple.Tuple;
import edu.upenn.cis.stormlite.tuple.Values;
import edu.upenn.cis455.mapreduce.worker.MainWorker;

/**
 * This is a virtual bolt that is used to route data to the WorkerServer on a
 * different worker.
 * 
 * @author zives
 *
 */
public class SenderBolt implements IRichBolt {

	static Logger log = Logger.getLogger(SenderBolt.class);

	/**
	 * To make it easier to debug: we have a unique ID for each instance of the
	 * WordCounter, aka each "executor"
	 */
	String executorId = UUID.randomUUID().toString();

	Fields schema = new Fields("url", "host");

	String stream;
	String address;
	ObjectMapper mapper = new ObjectMapper();
	URL url;
	String[] workers;
	int workerIndex;

	TopologyContext context;

	boolean isEndOfStream = false;

	public SenderBolt() {
	}

	public SenderBolt(String address, String stream) {
		this.stream = stream;
		this.address = address;
	}

	/**
	 * Initialization, just saves the output stream destination
	 */
	@Override
	public void prepare(Map<String, String> stormConf, TopologyContext context, OutputCollector collector) {
		mapper.enableDefaultTyping(ObjectMapper.DefaultTyping.NON_FINAL);
		this.context = context;

		workers = WorkerHelper.getWorkers(stormConf);
		workerIndex = Integer.valueOf(stormConf.get("workerIndex"));
	}

	/**
	 * Process a tuple received from the stream, incrementing our counter and
	 * outputting a result
	 */
	@Override
	public void execute(Tuple input) {
		URLInfo urlInfo = input.getUrlByField("url");
		String host = urlInfo.getHostName();
		int hash = host.hashCode();

		hash = hash % workers.length;
		if (hash < 0) {
			hash += workers.length;
		}
		
		// send to other workers if necessary
		if (hash == workerIndex){
			try {
				MainWorker.urlQueue.push(urlInfo.getUrl());
			} catch (IOException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
		} else {
			this.address = workers[hash];
			try {
				send(input);
			} catch (IOException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
		}
		
	}

	/**
	 * Sends the data along a socket
	 * 
	 * @param stream
	 * @param tuple
	 * @throws IOException
	 */
	private synchronized void send(Tuple tuple) throws IOException {
		isEndOfStream = tuple.isEndOfStream();
		URLInfo urlInfo = tuple.getUrlByField("url");
		Values<Object> values = new Values<Object>(urlInfo.getUrl(), urlInfo.getHostName());
		Tuple sentTuple = new Tuple(schema, values);
		url = new URL(address + "/pushdata/" + "CRAWLER_BOLT");

		HttpURLConnection conn = (HttpURLConnection) url.openConnection();
		conn.setRequestProperty("Content-Type", "application/json");
		String jsonForTuple = mapper.writerWithDefaultPrettyPrinter().writeValueAsString(sentTuple);

		// TODO: send this to /pushdata/{stream} as a POST!
		conn.setRequestMethod("POST");
		conn.setRequestProperty("Content-Type", "application/json");
		conn.setDoOutput(true);
		OutputStream os = conn.getOutputStream();
		byte[] toSend = jsonForTuple.getBytes();
		os.write(toSend);
		os.flush();
		if (conn.getResponseCode() != HttpURLConnection.HTTP_OK) {
			throw new RuntimeException("Worker Communication fail");
		}
		os.close();
		conn.disconnect();
	}

	/**
	 * Shutdown, just frees memory
	 */
	@Override
	public void cleanup() {
	}

	/**
	 * Lets the downstream operators know our schema
	 */
	@Override
	public void declareOutputFields(OutputFieldsDeclarer declarer) {
		declarer.declare(schema);
	}

	/**
	 * Used for debug purposes, shows our executor/operator's unique ID
	 */
	@Override
	public String getExecutorId() {
		return executorId;
	}

	/**
	 * Called during topology setup, sets the router to the next bolt
	 */
	@Override
	public void setRouter(StreamRouter router) {
		// NOP for this, since its destination is a socket
	}

	/**
	 * The fields (schema) of our output stream
	 */
	@Override
	public Fields getSchema() {
		return schema;
	}
}
