package edu.upenn.cis.stormlite.spout;

import java.io.IOException;
import java.util.Map;
import java.util.UUID;
import java.util.concurrent.TimeUnit;

import org.apache.log4j.Logger;

import edu.upenn.cis.stormlite.OutputFieldsDeclarer;
import edu.upenn.cis.stormlite.TopologyContext;
import edu.upenn.cis.stormlite.bolt.URLInfo;
import edu.upenn.cis.stormlite.routers.StreamRouter;
import edu.upenn.cis.stormlite.tuple.Fields;
import edu.upenn.cis.stormlite.tuple.Values;
import edu.upenn.cis455.mapreduce.worker.MainWorker;


public class CrawlerQueueSpout implements IRichSpout {

	static Logger log = Logger.getLogger(CrawlerQueueSpout.class);

	String executorId = UUID.randomUUID().toString();

	Fields schema = new Fields("url", "host");

	SpoutOutputCollector collector;

	@Override
	public String getExecutorId() {
		// TODO Auto-generated method stub
		return executorId;
	}

	@Override
	public void declareOutputFields(OutputFieldsDeclarer declarer) {
		// TODO Auto-generated method stub
		declarer.declare(schema);
	}

	@Override
	public void open(Map<String, String> config, TopologyContext topo, SpoutOutputCollector collector) {
		// TODO Auto-generated method stub
		this.collector = collector;
	}

	@Override
	public void close() {
		// TODO Auto-generated method stub
	}

	@Override
	public void nextTuple() {
		// TODO Auto-generated method stub
		URLInfo url;
		try {
			//url = MainWorker.urlQueue.poll(25, TimeUnit.SECONDS);
			url = new URLInfo(MainWorker.urlQueue.poll());
			if (url != null){
				this.collector.emit(new Values<Object>(url, url.getHostName()));
			}
		} catch (InterruptedException | IOException e) {
			// TODO Auto-generated catch block
			log.info(e);
		}
		
	}

	@Override
	public void setRouter(StreamRouter router) {
		this.collector.setRouter(router);
	}

}
