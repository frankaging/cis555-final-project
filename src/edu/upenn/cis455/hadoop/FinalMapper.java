package edu.upenn.cis455.hadoop;

import java.io.IOException;

import org.apache.hadoop.io.Text;
import org.apache.hadoop.mapreduce.Mapper;

import edu.upenn.cis455.storage.PageRankDB;

public class FinalMapper extends Mapper<Text, Text, Text, Text> {
	 
    public void map(Text key, Text value, Context context)
    throws IOException, InterruptedException {
        
    	String valueStr = value.toString();
		int indexOfStar = valueStr.indexOf("***");
		if (indexOfStar == -1)
			return;
		String rank = valueStr.substring(0, indexOfStar);
		context.write(key, new Text(rank));
		
		String url = key.toString();
		double rankDouble = Double.parseDouble(rank);
		int indexOfDoubleSlash = valueStr.indexOf("||");
		String strBeforeSlash = valueStr.substring(0, indexOfDoubleSlash);
		indexOfStar = strBeforeSlash.indexOf("***");
		String numberOfChildren = strBeforeSlash.substring(indexOfStar + 3);	
		int numOfChildren = Integer.parseInt(numberOfChildren);				
		PageRankDB.putRank(url, rankDouble, numOfChildren);

		System.out.println("FinalMapper: " + url);
    }

}
