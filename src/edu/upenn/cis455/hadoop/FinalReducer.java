package edu.upenn.cis455.hadoop;

import java.io.IOException;
import java.util.Iterator;

import org.apache.hadoop.io.Text;
import org.apache.hadoop.mapreduce.Reducer;

public class FinalReducer extends Reducer<Text, Text, Text, Text> {
    
    public void reduce(Text key, Iterable<Text> values, Context context)
    throws IOException, InterruptedException {
        
    	Iterator<Text> valuesIter = values.iterator();
		if (valuesIter.hasNext()) {
			context.write(key, valuesIter.next());
		}
		System.out.println("FinalReducer: " + key.toString());
    }
    
}
