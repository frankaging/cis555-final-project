package edu.upenn.cis455.hadoop;

import java.io.IOException;

import org.apache.hadoop.io.Text;
import org.apache.hadoop.mapreduce.Mapper;

public class InitMapper extends Mapper<Text, Text, Text, Text> {
	   
	public void map(Text key, Text value, Context context) 
			throws IOException, InterruptedException {
		// input key, value: url[\t]link1, link2, link3
		// String parent = key.toString().trim();
		String children = value.toString().trim();
		children = children.replaceAll("\n+", "");
		String childrenArray[] = children.split(", ");
		
		int numOfChildren = childrenArray.length;
		double initialValue = 1.0;
		// output key, value: url[\t]rank***numberOfChildren||link1, link2, link3
		String outputValue = initialValue + "***" + numOfChildren + "||" + children;
		context.write(key, new Text(outputValue));	
		System.out.println("InitMapper: ");
	}
	
}
