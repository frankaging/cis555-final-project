package edu.upenn.cis455.hadoop;

import java.io.IOException;
import java.util.Iterator;

import org.apache.hadoop.io.Text;
import org.apache.hadoop.mapreduce.Reducer;

public class InitReducer extends Reducer<Text, Text, Text, Text> {

	public void reduce(Text key, Iterable<Text> values, Context context) 
			throws IOException, InterruptedException {
		// input key, value: url[\t]rank***numberOfChildren||link1, link2, link3
		// output key, value: the same
		Iterator<Text> valuesIter = values.iterator();
		if (valuesIter.hasNext()) {
			context.write(key, valuesIter.next());
		}
		System.out.println("InitReducer: ");
	}

}

