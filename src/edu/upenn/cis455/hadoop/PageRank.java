package edu.upenn.cis455.hadoop;

import java.io.BufferedReader;
import java.io.BufferedWriter;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.FileWriter;
import java.io.IOException;
import java.util.HashSet;
import java.util.Scanner;
import java.util.Set;

import org.apache.hadoop.conf.Configuration;
import org.apache.hadoop.fs.Path;
import org.apache.hadoop.io.Text;
import org.apache.hadoop.mapreduce.Job;
import org.apache.hadoop.mapreduce.lib.input.FileInputFormat;
import org.apache.hadoop.mapreduce.lib.input.KeyValueTextInputFormat;
import org.apache.hadoop.mapreduce.lib.output.FileOutputFormat;
import org.apache.hadoop.mapreduce.lib.output.TextOutputFormat;
import org.apache.hadoop.security.UserGroupInformation;

import edu.upenn.cis455.storage.PageRankDB;

public class PageRank {
	// http://bnrg.cs.berkeley.edu/~randy/Courses/toce12-mr-assignment/WordCount.java
	static String time;
	static File inputDir;
	static File outputDir;
	static File sinkDir;

	@SuppressWarnings("deprecation")
	public static void main(String[] args)
			throws IOException, ClassNotFoundException, InterruptedException {

		 // Set user group information
	    UserGroupInformation realUser = UserGroupInformation.createRemoteUser("mapr");
	    UserGroupInformation.setLoginUser(realUser);
	    
		inputDir = new File("input");
		outputDir = new File("output");
		sinkDir = new File("sink");
		
		if(inputDir.exists()) {
			delDirectory(inputDir);
			inputDir.delete();
		}
		inputDir.mkdirs();
		if(outputDir.exists()) {
			delDirectory(outputDir);
			outputDir.delete();
		}
		outputDir.mkdirs();
		if(sinkDir.exists()) {
			delDirectory(sinkDir);
			sinkDir.delete();
		}
		sinkDir.mkdirs();

		// set up rank database
		File dbDir = new File("rankDB");
		if (dbDir.exists()) {
			delDirectory(dbDir);
			dbDir.delete();
		}
		PageRankDB.setUp("rankDB");

		System.out.println("Writing input files:");
		writeInput();

		// Init step
		System.out.println("Init step start...");
		Configuration initConfig = new Configuration();
//		initConfig.set("mapred.min.split.size", "10485760"); 
//		initConfig.set("mapred.max.split.size", "10485760"); 
		Job initJob = new Job(initConfig, "init");
		initJob.setJarByClass(PageRank.class);
		initJob.setMapOutputKeyClass(Text.class);
		initJob.setMapOutputValueClass(Text.class);
		initJob.setOutputKeyClass(Text.class);
		initJob.setOutputValueClass(Text.class);
		initJob.setMapperClass(InitMapper.class);
		initJob.setReducerClass(InitReducer.class);
		initJob.setInputFormatClass(KeyValueTextInputFormat.class);
		initJob.setOutputFormatClass(TextOutputFormat.class);
		
		// Add a Path to the list of inputs/outputs for the map-reduce job.
		FileInputFormat.addInputPath(initJob, new Path(inputDir.getPath()));
		FileOutputFormat.setOutputPath(initJob, new Path(outputDir.getPath(), "initOutput"));
		initJob.waitForCompletion(true);

		// PageRank step	
		int iters = 3;
		System.out.println("PageRank step start... " + "(" + iters + " iterations in total)");
		for (int i = 0; i < iters; i++) {
//			 if (i == 1)
//			 	delDirectory(new File("output/initOutput"));
//			 if (i > 1)
//			 	delDirectory(new File("output/rankOutput-" + Integer.toString(i-2)));
			Configuration rankConfig = new Configuration();
			Job rankJob = new Job(rankConfig, "pagerank");
			rankJob.setJarByClass(PageRank.class);
			rankJob.setMapOutputKeyClass(Text.class);
			rankJob.setMapOutputValueClass(Text.class);
			rankJob.setOutputKeyClass(Text.class);
			rankJob.setOutputValueClass(Text.class);
			rankJob.setMapperClass(RankMapper.class);
			rankJob.setReducerClass(RankReducer.class);
			rankJob.setInputFormatClass(KeyValueTextInputFormat.class);
			rankJob.setOutputFormatClass(TextOutputFormat.class);
			if (i == 0){
				FileInputFormat.addInputPath(rankJob, new Path(outputDir.getPath(), "initOutput"));
				FileOutputFormat.setOutputPath(rankJob, new Path(outputDir.getPath(), "rankOutput-" + i));
			}
			else{
				FileInputFormat.addInputPath(rankJob, new Path(outputDir.getPath(), "rankOutput-" + (i-1)));
				FileOutputFormat.setOutputPath(rankJob, new Path(outputDir.getPath(), "rankOutput-" + i));
			}
			rankJob.waitForCompletion(true);
			System.out.println("Iteration " + i + " finished...");
			
//			System.out.println("Writing iteration " + i + " ...");
//			Configuration writeConfig = new Configuration();
//			Job writeJob = new Job(writeConfig, "write");
//			writeJob.setJarByClass(PageRank.class);
//			writeJob.setMapOutputKeyClass(Text.class);
//			writeJob.setMapOutputValueClass(Text.class);
//			writeJob.setOutputKeyClass(Text.class);
//			writeJob.setOutputValueClass(Text.class);
//			writeJob.setMapperClass(FinalMapper.class);
//			writeJob.setReducerClass(FinalReducer.class);
//			writeJob.setInputFormatClass(KeyValueTextInputFormat.class);
//			writeJob.setOutputFormatClass(TextOutputFormat.class);
//			FileInputFormat.addInputPath(writeJob, new Path(outputDir.getPath(), "rankOutput-" + i));
//			FileOutputFormat.setOutputPath(writeJob, new Path(outputDir.getPath(), "finalOutput-" + i));
//			writeJob.waitForCompletion(true);
//			System.out.println("Writing iteration " + i + " finished.");
		}
		System.out.println("PageRank step finished.");

		// final step
		System.out.println("Final step start...");
		Configuration finalConfig = new Configuration();
		Job finalJob = new Job(finalConfig, "final");
		finalJob.setJarByClass(PageRank.class);
		finalJob.setMapOutputKeyClass(Text.class);
		finalJob.setMapOutputValueClass(Text.class);
		finalJob.setOutputKeyClass(Text.class);
		finalJob.setOutputValueClass(Text.class);
		finalJob.setMapperClass(FinalMapper.class);
		finalJob.setReducerClass(FinalReducer.class);
		finalJob.setInputFormatClass(KeyValueTextInputFormat.class);
		finalJob.setOutputFormatClass(TextOutputFormat.class);
		// Add a Path to the list of inputs/outputs for the map-reduce job.
		FileInputFormat.addInputPath(finalJob, new Path(outputDir.getPath(), "rankOutput-" + (iters-1)));
		FileOutputFormat.setOutputPath(finalJob, new Path(outputDir.getPath(), "finalOutput"));
		finalJob.waitForCompletion(true);
		System.out.println("Final step finished.");
		
		calculateSinkRank();
	}
	
	public static void delDirectory(File dir) {
	    for (File file: dir.listFiles()) {
	        if (file.isDirectory()) 
	        	delDirectory(file);
	        file.delete();
	    }
	}

	public static void writeInput() throws IOException {
		File storeDir = new File("store");
		
		File[] storeFiles = storeDir.listFiles();
		int fileCount = 0;
		int urlCount = 0;	
		int linkCount = 0;
		
		Set<String> sinkSet = new HashSet<String>();		
		for (File storeFile : storeFiles) {
			System.out.println(storeFile.getName());
			fileCount += 1;	
			File inputFile = new File(inputDir, fileCount + ".txt");
			inputFile.createNewFile();			
			BufferedWriter inputWriter = new BufferedWriter(new FileWriter(inputFile));

			Scanner s = new Scanner(storeFile);
			s.useDelimiter("\\Z");
			String content = s.next();
			s.close();					
			
			String splitted[] = content.split("\\#\\#\\#\\$\\$\\$\\@\\@\\@\\$\\$\\$\\#\\#\\#");
			for (int i = 0; i < splitted.length; i++) {
				String[] pair = splitted[i].split("\\[");
				if (pair.length < 2)
					continue;
				String parent = pair[0].trim();
				if (parent.endsWith("/"))
					parent = parent.substring(0, parent.length() - 1);
				String children = pair[1];
				if (children.length() < 1)
					continue;
				children = children.substring(0, children.length()-1).trim();
				children = children.replaceAll("(\r\n)+", "");
				children = children.replaceAll("\n+", "");
				String[] childList = children.split(", ");
				String childrenStr = "";
				Set<String> childSet = new HashSet<String>();
				for (String child : childList) {
					child = child.trim();
					if (child.endsWith("/"))
						child = child.substring(0, child.length() - 1);
					if (child.length() > 0 && !child.equals(parent) && !childSet.contains(child)) {
						childSet.add(child);
						childrenStr += child + ", ";
					}
				}
				if (childrenStr.length() > 0) {
					childrenStr = childrenStr.substring(0, childrenStr.length()-2);
					inputWriter.write(parent + "\t" + childrenStr + "\n"); 
					urlCount += 1;
					linkCount += childSet.size();
				}	
				else {
					System.out.print("Find sink: ");
					System.out.println(parent);
					sinkSet.add(parent);
				}				
			}
			inputWriter.close();				
		}	
		System.out.println("total number of input files: " + fileCount);
		System.out.println("total number of urls crawled (not a sink): " + urlCount);
		System.out.println("total number of links extracted: " + linkCount);
		
		File sinkFile = new File(sinkDir, "sink.txt");
		sinkFile.createNewFile();
		BufferedWriter sinkWriter = new BufferedWriter(new FileWriter(sinkFile));
		System.out.println("Writing sink file: ");
		for (File storeFile : storeFiles) {
			System.out.println(storeFile.getName());	

			Scanner s = new Scanner(storeFile);
			s.useDelimiter("\\Z");
			String content = s.next();
			s.close();					
			
			String splitted[] = content.split("\\#\\#\\#\\$\\$\\$\\@\\@\\@\\$\\$\\$\\#\\#\\#");
			for (int i = 0; i < splitted.length; i++) {
				String[] pair = splitted[i].split("\\[");
				if (pair.length < 2)
					continue;
				String parent = pair[0].trim();
				if (parent.endsWith("/"))
					parent = parent.substring(0, parent.length() - 1);
				String children = pair[1];
				if (children.length() < 1)
					continue;
				children = children.substring(0, children.length()-1).trim();
				children = children.replaceAll("(\r\n)+", "");
				children = children.replaceAll("\n+", "");
				String[] childList = children.split(", ");
				Set<String> childSet = new HashSet<String>();
				for (String child : childList) {
					child = child.trim();
					if (child.endsWith("/"))
						child = child.substring(0, child.length() - 1);
					if (sinkSet.contains(child) && child.length() > 0 && 
							!child.equals(parent) && !childSet.contains(child)) {
						childSet.add(child);
						sinkWriter.write(child + ", " + parent + "\n");
					}
				}				
			}			
		}
		sinkWriter.close();	
		System.out.println("Finished writing sink file.");
	}
	
	public static void calculateSinkRank() throws FileNotFoundException, IOException {
		System.out.println("Reading sink.txt...");
		File sinkFile = new File(sinkDir, "sink.txt");
		Set<String> sinkSet = new HashSet<String>();
		try (BufferedReader br = new BufferedReader(new FileReader(sinkFile))) {
		    String line;
		    while ((line = br.readLine()) != null) {
		       String[] splitted = line.split(", ");
		       String sink = splitted[0];
		       String parent = splitted[1];
		       PageRankDB.addSinkRank(sink, parent);
		       sinkSet.add(sink);
		       System.out.println("sink: " + sink);
		    }
		}
		File sinkOutputFile = new File(sinkDir, "sinkOutput.txt");
		sinkOutputFile.createNewFile();
		BufferedWriter sinkOutWriter = new BufferedWriter(new FileWriter(sinkOutputFile));
		System.out.println("Calculating rank of sinks: ");
		for (String sink : sinkSet) {
			double sinkRank = PageRankDB.getRank(sink);
			System.out.println(sink + " " + sinkRank);
			sinkOutWriter.write(sink + "\t" + sinkRank + "\n");
		}
		sinkOutWriter.close();
		System.out.println("Finished calculating rank of sinks.");
	}

}
