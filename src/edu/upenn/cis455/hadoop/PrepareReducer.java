package edu.upenn.cis455.hadoop;

import java.io.IOException;

import org.apache.hadoop.io.Text;
import org.apache.hadoop.mapreduce.Reducer;

public class PrepareReducer extends Reducer<Text, Text, Text, Text> {
	   
	public void reduce(Text key, Iterable<Text> values, Context context) 
			throws IOException, InterruptedException {
		
		// parent, child 0/1		
		String parent = key.toString() + " 1.0"; // initial rank value
		String children = "";
		for(Text value: values)
			children += value.toString() + "  "; // 2 spaces here
		context.write(new Text(parent), new Text(children));
		
	}
	
}