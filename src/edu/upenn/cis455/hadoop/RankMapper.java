package edu.upenn.cis455.hadoop;

import java.io.IOException;

import org.apache.hadoop.io.Text;
import org.apache.hadoop.mapreduce.Mapper;

public class RankMapper extends Mapper<Text, Text, Text, Text> {

	public void map(Text key, Text value, Context context) 
			throws IOException, InterruptedException {

		// input key: url
		// input value: rank***numberOfChildren||link1, link2, link3
//		String urlStr = key.toString();

		String valueStr = value.toString();
		int indexOfDoubleSlash = valueStr.indexOf("||");
		String strBeforeSlash = valueStr.substring(0, indexOfDoubleSlash);
		String strAfterSlash  = valueStr.substring(indexOfDoubleSlash + 2);
		int indexOfStar = strBeforeSlash.indexOf("***");

		String rank = strBeforeSlash.substring(0, indexOfStar);
		double rankValue = Double.parseDouble(rank);
		String numberOfChildren = strBeforeSlash.substring(indexOfStar + 3);	
		int numOfChildren = Integer.parseInt(numberOfChildren);

		if (numOfChildren > 0) {
			double weightAssigned = rankValue / (double)numOfChildren;
			String[] childrenList = strAfterSlash.split(", ");

			for (String child : childrenList) {
				String weightAssignedStr = String.valueOf(weightAssigned);
				context.write(new Text(child), new Text(weightAssignedStr));
			}
		}
		context.write(key, new Text("~" + valueStr));
		System.out.println("RankMapper: " + key.toString());
	}
}