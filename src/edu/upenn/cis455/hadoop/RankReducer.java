package edu.upenn.cis455.hadoop;

import java.io.IOException;
import java.util.Iterator;

import org.apache.hadoop.io.Text;
import org.apache.hadoop.mapreduce.Reducer;

public class RankReducer extends Reducer<Text, Text, Text, Text> {

	public void reduce(Text key, Iterable<Text> values, Context context)
			throws IOException, InterruptedException {
		// input key: url
		// input value: ~rank***numberOfChildren||link1, link2, link3    
		// input value: rankAssignedStr(double)

		boolean crawled = false;
		double newRank = 0.0;
		String newValueStr = "";

		Iterator<Text> valuesIter = values.iterator();
		while (valuesIter.hasNext()) {
			String valueStr = valuesIter.next().toString();
			if (valueStr.startsWith("~")) {
				int indexOfStar = valueStr.indexOf("***");
				if (indexOfStar == -1)
					continue;
				else
					newValueStr = valueStr.substring(indexOfStar);
				crawled = true;
			}
			else
				newRank += Double.parseDouble(valueStr);
		}

		if (crawled) {
			newRank = 0.85 * newRank + 0.15;
			newValueStr = String.valueOf(newRank) + newValueStr;
			context.write(key, new Text(newValueStr));
		}
		System.out.println("RankReducer: ");
	}
}
