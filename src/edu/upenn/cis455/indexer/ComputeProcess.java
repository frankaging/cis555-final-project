package edu.upenn.cis455.indexer;

import java.util.LinkedList;

import edu.upenn.cis455.storage.IndexDB;

public class ComputeProcess {

	public static void main(String args[]) throws InterruptedException{
		IndexDB.SetUptoCheck("IndexDB");
		LinkedList<ComputeThread> list = new LinkedList<ComputeThread>();
		
		for(int i=0;i<5;i++){
			ComputeThread c = new ComputeThread(3.533980582524272,i);
			list.add(c);
		}
		
		while(list.size()!=0){
			list.poll().start();
		}
	}
}
