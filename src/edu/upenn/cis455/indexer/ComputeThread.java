package edu.upenn.cis455.indexer;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileReader;
import java.io.IOException;
import java.util.HashSet;

import edu.upenn.cis455.storage.IndexDB;

public class ComputeThread extends Thread{
	private HashSet<String> blackset = new HashSet<String>();
	private double maxtf;
	private int index;
	
	public ComputeThread(double maxtf,int index){
		this.maxtf = maxtf;
		this.index = index;
	}
	
	public void run(){
		long start = System.currentTimeMillis();
		try {
			CaculateIDF(maxtf);
		} catch (IOException e) {
			e.printStackTrace();
		}
		long end = System.currentTimeMillis();
		System.out.println("compute-time: " + (end -start));
	}
	
	public void CaculateIDF(double maxTF) throws IOException{
		double a = 0.5;
		File outputdir = new File("output");
		
		File tfResultsPrint = new File(outputdir, "TFResultPrint" + index + ".txt");
		BufferedReader tfreader = new BufferedReader(new FileReader(tfResultsPrint));
		String line = tfreader.readLine();
		while(line!=null) {
			String[] splitted = line.split("\\s+");
			String word = splitted[0];
			String filename = splitted[1];
			double tf = Double.parseDouble(splitted[2]);
			if(!blackset.contains(word)){
				double dfnum = IndexDB.getNDF(word);
				if(dfnum == 1){
					blackset.add(word);
					IndexDB.addTFIDF(word,"##@@##", 0);
					
				}else{
					double normalizedtf = a+(1-a)*(tf/maxTF);
					double tfidf = IndexDB.computeTfIdf(normalizedtf,dfnum);
					IndexDB.addTFIDF(word,filename,tfidf);
				}
			}
			line = tfreader.readLine();
		}
		tfreader.close();
	}

}
