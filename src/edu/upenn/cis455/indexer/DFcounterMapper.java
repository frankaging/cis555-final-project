package edu.upenn.cis455.indexer;

import java.io.IOException;
import java.util.HashSet;
import java.util.Set;
import java.util.StringTokenizer;

import org.apache.hadoop.io.IntWritable;
import org.apache.hadoop.io.Text;
import org.apache.hadoop.mapreduce.Mapper;

/**
 * input: stemmed directory 
 * output: df result
 * word --> 1 document contains it
 * @author cis555
 *
 */
public class DFcounterMapper extends Mapper<Object, Text, Text, IntWritable>{
	private Text word = new Text();
	private final static IntWritable one = new IntWritable(1);
	public void map(Object key, Text value, Context context) throws IOException, InterruptedException 
	{
		StringTokenizer itr = new StringTokenizer(value.toString());
		Set<String> uniqueWords = new HashSet<String>();
		//for each input file, every unique word appear in this file got +1
		//which means 1 document contains this word
		while (itr.hasMoreTokens()) {
			word.set(itr.nextToken());
			uniqueWords.add(word.toString());
		}
		for(String x: uniqueWords){
			word.set(x);
			context.write(word, one);
		}
	}
}
