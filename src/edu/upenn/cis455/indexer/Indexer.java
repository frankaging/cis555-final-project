package edu.upenn.cis455.indexer;

import java.io.File;
import java.io.IOException;

import org.apache.hadoop.conf.Configuration;
import org.apache.hadoop.fs.Path;
import org.apache.hadoop.io.IntWritable;
import org.apache.hadoop.io.Text;
import org.apache.hadoop.mapreduce.Job;
import org.apache.hadoop.mapreduce.lib.input.FileInputFormat;
import org.apache.hadoop.mapreduce.lib.output.FileOutputFormat;


public class Indexer {
	
	private static boolean debugMode = true;
	private static String dfResult = "DFcounterResult";
	private static String indexerResult = "indexerResult";
	private static String tfResult = "TFCounterResult";
	
	static File inputdir = new File("input");
    static File outputdir = new File("output");
	static File unstemmedDir = new File(inputdir, "unstemmed");;
	static File stemmedDir = new File(inputdir, "stemmed");
	static File outputDir;
	
	protected static void printDebug(String str){
		if(debugMode){
			System.out.println(str);
		}
	}
	
	public static void main(String args[]) throws IOException, ClassNotFoundException, InterruptedException{
		if(outputdir.exists()){
	    	deleteDir(outputdir);
	    	outputdir.mkdirs();
	    	System.out.println("create new output dir");
	    }
		outputDir = new File(outputdir, "outputs");
		outputDir.mkdirs();
		
	    //Job indexerJob = IndexerMain();
		Job dfCounterJob = DFcounterMain();
		Job tfCounterJob = TFcounterMain();
	    printDebug("start to do jobs");
	    //indexerJob.waitForCompletion(true);
		//printDebug("indexer finished");
		dfCounterJob.waitForCompletion(true);
		printDebug("DFcounter finished");
		tfCounterJob.waitForCompletion(true);
		printDebug("TFcounter finished");
	}
	
//*************************************************************************//
	
	
	public static Job TFcounterMain() throws IOException{
		Configuration conf = new Configuration();
		Job job = new Job(conf, "TFcounter");
		job.setJarByClass(Indexer.class);
		job.setMapperClass(TFcounterMapper.class);
		job.setReducerClass(TFcounterReducer.class);
		job.setOutputKeyClass(Text.class);
		job.setOutputValueClass(IntWritable.class);
		job.setMapOutputKeyClass(Text.class);
		job.setMapOutputValueClass(IntWritable.class);
		FileInputFormat.addInputPath(job, new Path(stemmedDir.getPath()));
		FileOutputFormat.setOutputPath(job, new Path(outputDir.getPath(), tfResult));

		return job;
	}
	
	public static Job DFcounterMain() throws IOException{
		Configuration conf = new Configuration();
		Job job = new Job(conf, "DFcounter");
		job.setJarByClass(Indexer.class);
		job.setMapperClass(DFcounterMapper.class);
		job.setReducerClass(DFcounterReducer.class);
		job.setOutputKeyClass(Text.class);
		job.setOutputValueClass(IntWritable.class);
		job.setMapOutputKeyClass(Text.class);
		job.setMapOutputValueClass(IntWritable.class);
		FileInputFormat.addInputPath(job, new Path(stemmedDir.getPath()));
		FileOutputFormat.setOutputPath(job, new Path(outputDir.getPath(), dfResult));
		return job;
	}
	
	private static void deleteDir(File dir) {
		for(File f: dir.listFiles()) {
			if(f.isDirectory())
				deleteDir(f);
			else 
				f.delete();
		}
		dir.delete();
	}
	
}
