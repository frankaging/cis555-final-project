package edu.upenn.cis455.indexer;

import java.io.BufferedReader;
import java.io.BufferedWriter;
import java.io.File;
import java.io.FileReader;
import java.io.FileWriter;
import java.io.IOException;

import edu.upenn.cis455.storage.IndexDB;
import edu.upenn.cis455.storage.TermInfo;

public class PostProcess {
	private static String dfResult = "DFcounterResult";
	private static String tfResult = "TFCounterResult";
	
	public static void main(String args[]) throws IOException{
		int indextemp = Integer.parseInt(args[0]);
		long start=System.currentTimeMillis();
		IndexDB.SetUptoCheck("IndexDB"+indextemp);

		File outputDir = new File("outputs"+indextemp);
		File[] tfResults = new File(outputDir, tfResult).listFiles();
		File[] dfResults = new File(outputDir, dfResult).listFiles();
		//File[] indexerResults = new File(outputDir, indexerResult).listFiles();
        
		int count = 25000;
//		File dfResultsPrint = new File(outputDir, "DFResultPrint.txt");
//		dfResultsPrint.delete();
//		dfResultsPrint.createNewFile();
//		BufferedWriter dfwriter = new BufferedWriter(new FileWriter(dfResultsPrint));
//		StringBuffer dfbuffer = new StringBuffer();
		for(File dfResultFile : dfResults){
			// skip success file
			if(dfResultFile.getName().startsWith(".")){
				continue;
			}
			BufferedReader df = new BufferedReader(new FileReader(dfResultFile));
			String line = df.readLine();
			while(line != null){
//				if(dfbuffer.length() >= 1024){
//					dfwriter.write(dfbuffer.toString());
//					dfbuffer = new StringBuffer();
//				}
				String[] split = line.split("\t");
				String word = split[0];
				double dfnum = Double.parseDouble(split[1]);
				if(!word.matches("\\s+")){
					TermInfo term = new TermInfo(word);
					term.setNDF((double)count/dfnum);
					IndexDB.putTerm(term);
					System.out.println(word + " " + (double)count/dfnum);
//					String towrite = word + " " + (double)count/dfnum;
//					dfbuffer.append(towrite);
					line = df.readLine();
				}else{
					line = df.readLine();
				}
			}
//			if(dfbuffer.length()!=0){
//				dfwriter.write(dfbuffer.toString());
//				dfbuffer = new StringBuffer();
//			}
			df.close();
		}
//		dfwriter.close();
		System.out.println("read all df output");
		long df = System.currentTimeMillis();
		System.out.println("df-time: " + (df-start));
		
		File tfResultsPrint = new File(outputDir, "TFResultPrint.txt");
		tfResultsPrint.delete();
		tfResultsPrint.createNewFile();
		BufferedWriter tfwriter = new BufferedWriter(new FileWriter(tfResultsPrint));
		StringBuffer tfbuffer = new StringBuffer();
		double maxtf = 0;
		for(File tfResultFile : tfResults){
			if(tfResultFile.getName().startsWith(".")){
				continue;
			}
			BufferedReader tf = new BufferedReader(new FileReader(tfResultFile));
			String line = tf.readLine();
			while(line != null){
				if(tfbuffer.length() >= 1024){
					tfwriter.write(tfbuffer.toString());
					tfbuffer = new StringBuffer();
				}
				String[] split = line.split("\t");
				String[] keysplit = split[0].split("\\s+");
				String word = keysplit[0];
				String fakefilename = keysplit[1];
				String index = fakefilename.substring(0, fakefilename.indexOf("."));
				String filename = IndexDB.getIndexURL(index).getURL();
				int sum = Integer.parseInt(split[1]);
				if(!word.matches("\\s+") && !filename.matches("\\s+")){					
					int totalwords = IndexDB.getIndexURLSize(index);
					double tfnum = (double)sum/totalwords;
					if(tfnum>maxtf){
						maxtf = tfnum;
					}
					String towrite = word + " " + filename + " " + tfnum + "\n";
					tfbuffer.append(towrite);
					//IndexDB.addTF(word, filename, tfnum);
					line = tf.readLine();
				}else{
					line = tf.readLine();
				}
			}
			if(tfbuffer.length()!=0){
				tfwriter.write(tfbuffer.toString());
				tfbuffer = new StringBuffer();
			}
			tf.close();
		}
		tfwriter.close();
		System.out.println("read all tf output");
		System.out.println("maxTF:" + maxtf);
		long tf = System.currentTimeMillis();
		System.out.println("tf-time: " + (tf-df));
        IndexDB.CaculateAllIDF(maxtf);
		long end=System.currentTimeMillis();
		System.out.println("total-time: " + (end-tf));

//		for(File indexerResult:indexerResults){
//			if(indexResult.getName().startsWith(".")){
//				continue;
//			}
//			BufferedReader in = new BufferedReader(new FileReader(indexerResult));
//			String line = in.readLine();
//			while(line!=null){
//				String[] split = line.split("\t");
//				String[] keysplit = split[0].split(" ");
//				IndexDB.addFile(keysplit[0], keysplit[1]);
//			}
//			in.close();
//		}
//		System.out.println("read all indexer output");

	}
	

	
}
