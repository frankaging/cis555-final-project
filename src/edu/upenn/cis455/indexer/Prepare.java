package edu.upenn.cis455.indexer;

import java.io.BufferedWriter;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileWriter;
import java.io.IOException;
import java.io.UnsupportedEncodingException;
import java.util.LinkedList;
import java.util.Scanner;
import java.util.UUID;

import org.tartarus.snowball.SnowballStemmer;

import com.amazonaws.AmazonClientException;
import com.amazonaws.ClientConfiguration;
import com.amazonaws.auth.AWSCredentials;
import com.amazonaws.auth.profile.ProfileCredentialsProvider;
import com.amazonaws.regions.Region;
import com.amazonaws.regions.Regions;
import com.amazonaws.services.s3.AmazonS3;
import com.amazonaws.services.s3.AmazonS3Client;
import com.amazonaws.services.s3.model.PutObjectRequest;

import edu.upenn.cis455.storage.IndexDB;
import edu.upenn.cis455.storage.IndexURL;
import edu.upenn.cis455.storage.S3FileDownloader;

public class Prepare {
	
	private static boolean debugMode = true;
	
	//static File unstemmedDir;
	static File stemmedDir;
	static int index;
	protected static void printDebug(String str){
		if(debugMode){
			System.out.println(str);
		}
	}
	
	/**
	 * @ input
	 * @throws IOException
	 * @throws ClassNotFoundException
	 * @throws InterruptedException
	 */
	public static void main(String args[]) throws IOException, ClassNotFoundException, InterruptedException{
		
		// download all the file from crawler S3 to this EC2
		index = Integer.parseInt(args[0]);
		S3FileDownloader d = new S3FileDownloader(index);
		d.DownloadFromS3();
		
		// after we finish downloading the files from the S3
	    File inputdir = new File("input"+index);     
	    if(inputdir.exists()){
	    	deleteDir(inputdir);
	    	inputdir.mkdirs();
	    	System.out.println("create new input dir");
	    }else{
	    	inputdir.mkdirs();
	    	System.out.println("create new input dir");
	    }
	    
	    //set up database
	    IndexDB.SetUp("IndexDB"+index);
	    
//	    unstemmedDir = new File(inputdir, "unstemmed");
//		unstemmedDir.mkdirs();
		stemmedDir = new File(inputdir, "stemmed");
		stemmedDir.mkdirs();
		
		LinkedList<WriteFileThread> threadpool = new LinkedList<WriteFileThread>();
		for(int i=0; i<100; i++){
			String s = "store" + i;
			WriteFileThread w = new WriteFileThread(s);
			threadpool.add(w);
		}
		
	    printDebug("start to write input files");  
        while(threadpool.size()!=0){
        	threadpool.poll().start();
        }
	    printDebug("finished writing input files");

	}
	
	public static int WriteInput(String where) throws FileNotFoundException, UnsupportedEncodingException{
		File DBinput = new File(where);
		File[] allinput = DBinput.listFiles();
		int  urlcount = 0;
		Scanner s = null;
		for(File everyinput:allinput){
			if(everyinput.getName().startsWith(".")){
				continue;
			}
			String delimiter = "\\#\\#\\#\\$\\$\\$\\@\\@\\@\\$\\$\\$\\#\\#\\#";
			s = new Scanner(everyinput);
			s.useDelimiter(delimiter);
			while(s.hasNext()){
				String urlanddoc = s.next();
				String[] pair = urlanddoc.split("\t",2);
				if(pair[1] == null || pair[1].length()==0){
					printDebug("invalid file");
					continue;
				}
				
				//decode
				String encodeurl = pair[0].trim();
				byte[] urlbytes = encodeurl.getBytes("UTF-8");
				String url = new String(urlbytes, "UTF-8");
				
				String encodetextcontent = pair[1].trim();
				byte[] textbytes = encodetextcontent.getBytes("UTF-8");
				String  textcontent = new String(textbytes, "UTF-8");;
				
				@SuppressWarnings("rawtypes")
				Class stemClass;
				SnowballStemmer stemmer = null;

				try {
					stemClass = Class.forName("org.tartarus.snowball.ext.englishStemmer");
					stemmer = (SnowballStemmer) stemClass.newInstance();
				} catch (ClassNotFoundException | InstantiationException | IllegalAccessException e1) {
					// TODO Auto-generated catch block
					e1.printStackTrace();
				}
				
				String executorId = UUID.randomUUID().toString();
				File stemmed = new File(stemmedDir, executorId + ".txt");
				//File unstemmed = new File(unstemmedDir, urlcount+".txt");
				
				
				try {
					stemmed.createNewFile();
					//unstemmed.createNewFile();
				} catch (IOException e) {
					e.printStackTrace();
					continue;
				}

				BufferedWriter stemmedOut = null;
				//BufferedWriter unstemmedOut = null;
				try {
					stemmedOut = new BufferedWriter(new FileWriter(stemmed));
					//unstemmedOut = new BufferedWriter(new FileWriter(unstemmed));
				} catch (IOException e) {
					e.printStackTrace();
				}
				String afterstem = "";
				String text = textcontent.replaceAll("[^A-Za-z]", " ");
				String[] splitted = text.split("\\s+");
				
//				System.out.println("urlcount "+urlcount);
//				System.out.println("url "+url);
				IndexURL indexurl = new IndexURL(executorId,url);
				indexurl.setSize(splitted.length);
				IndexDB.putIndexURL(indexurl);
				System.out.println("write file: " + urlcount);
				urlcount++;
				
				for(int i=0; i<splitted.length; i++){
					stemmer.setCurrent(splitted[i].trim());
					stemmer.stem();
					afterstem += stemmer.getCurrent().toLowerCase() + " ";
				}

				try {
					//unstemmedOut.write(textcontent);
					stemmedOut.write(afterstem);
				} catch (IOException e) {
					e.printStackTrace();
					continue;
				}
				try {
					stemmedOut.close();
					//unstemmedOut.close();
				} catch (IOException e) {
					e.printStackTrace();
				}
				
				ClientConfiguration clientConfig = new ClientConfiguration();
				clientConfig.setConnectionTimeout(50000);
				clientConfig.setMaxConnections(5000);
				clientConfig.setSocketTimeout(50000);
				clientConfig.setMaxErrorRetry(10);
				
				// after finish writing to the local txt file
				// we upload to the S3 bucket
				System.out.println("===========================================");
				System.out.println("Uploading new file to S3-xiaoyan");
				System.out.println("===========================================\n");
				
				//Set up S3 connection
				AWSCredentials credentials = null;
				try {
					credentials = new ProfileCredentialsProvider("xiaoyan").getCredentials();
				} catch (Exception e) {
					throw new AmazonClientException(
							"Cannot load the credentials from the credential profiles file. " +
									"Please make sure that your credentials file is at the correct " +
									"location (/home/cis455/.aws/credentials), and is in valid format.",
									e);
				}

				AmazonS3 s3 = new AmazonS3Client(credentials, clientConfig);
				Region usStandard = Region.getRegion(Regions.US_EAST_1);
				s3.setRegion(usStandard);

				//Choose the bucket
				String buckName = "final-mapreduce-cis555-input4";
				String mrfileName = stemmed.getName();
				
				//Upload the file
				System.out.println("Uploading a new object to XiaoYan-S3 from a file\n");
				// if it is a single file
				s3.putObject(new PutObjectRequest(buckName, mrfileName, stemmed));
				
				
			}
			s.close();
		}
			
		return urlcount;
	}
	
	public static void deleteDir(File dir) {
		for(File f: dir.listFiles()) {
			if(f.isDirectory())
				deleteDir(f);
			else 
				f.delete();
		}
		dir.delete();
	}
	    

}
