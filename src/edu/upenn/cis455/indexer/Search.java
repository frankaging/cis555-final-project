package edu.upenn.cis455.indexer;

import java.io.BufferedWriter;
import java.io.FileWriter;
import java.io.IOException;
import java.util.Comparator;
import java.util.HashMap;
import java.util.HashSet;
import java.util.PriorityQueue;
import java.util.Stack;

import org.tartarus.snowball.SnowballStemmer;

import edu.upenn.cis455.storage.IndexDB;

class DocWeight {   
	String filename;  
	double weight;  
	public DocWeight(String name, double weight){  
		this.filename = name;  
		this.weight = weight;  
	}     
	
	public void change(){
		weight = 0;
	}
} 

class Mycomparator implements Comparator<DocWeight>{
	public int compare(DocWeight d1, DocWeight d2){
		if(d1.weight<d2.weight){
			return -1;
		}
		if(d1.weight>d2.weight){
			return 1;
		}
		return 0;
	}
}

public class Search {
	private static HashMap<String,Double> NDF = new HashMap<String,Double>(); 
	private static HashMap<String,Integer> QueryCount = new HashMap<String,Integer>();
	private static HashMap<String,Double> QueryTFIDF = new HashMap<String, Double>();
	private static HashSet<String> QueueSet = new HashSet<String>();
	      
	private static int size = 10;
	private static PriorityQueue<DocWeight> queue = new PriorityQueue<DocWeight>(size, new Mycomparator());        
	private static Stack<DocWeight> stack = new Stack<DocWeight>();
	
	public static void main(String args[]) throws IOException{
		IndexDB.SetUptoCheck("IndexDB");
		String query = "apple";
		
		@SuppressWarnings("rawtypes")
		Class stemClass;
		SnowballStemmer stemmer = null;
		try {
			stemClass = Class.forName("org.tartarus.snowball.ext.englishStemmer");
			stemmer = (SnowballStemmer) stemClass.newInstance();
		} catch (ClassNotFoundException | InstantiationException | IllegalAccessException e1) {
			e1.printStackTrace();
		}
		String afterstem = "";
		String[] splitted = query.split("\\s+");
		for(int i=0; i<splitted.length; i++){
			stemmer.setCurrent(splitted[i].trim());
			stemmer.stem();
			afterstem += stemmer.getCurrent().toLowerCase() + " ";
		}
		System.out.println(afterstem.trim());
		String[] splittedstemmed = afterstem.trim().split(" ");
		for(int j=0;j<splittedstemmed.length;j++){
			String word = splittedstemmed[j];
			double ndf = IndexDB.getNDF(word);
			NDF.put(word,ndf);
			if(QueryCount.keySet().contains(word)){
				QueryCount.put(word,QueryCount.get(word)+1);
			}else{
				QueryCount.put(word,1);
			}
		}
		
		double maxtf = 0;
		for(String word:QueryCount.keySet()){
			double querytf = (double)QueryCount.get(word)/splittedstemmed.length;
			QueryTFIDF.put(word, querytf);
			if(querytf>maxtf){
				maxtf = querytf;
			}
		}
		
		double a = 0.5;
		for(String word:QueryCount.keySet()){
			if(NDF.get(word)==0){
				QueryTFIDF.put(word, 0.0);
			}else{
			    QueryTFIDF.put(word, (a+(1-a)*QueryTFIDF.get(word)/maxtf)*Math.log(NDF.get(word)));
			}
		}
		
		double totalmaxtf = 2.0;
		for(String word:QueryTFIDF.keySet()){	
			//System.out.println("word: " + word);
			if(IndexDB.getFiles(word).isEmpty()){
				continue;
			}
			for(String filename:IndexDB.getFiles(word)){
//				System.out.println("word: " + word);
				double sum = 0;
//				double divide1 = 0;
//				double divide2 = 0;
				for(String friend:QueryTFIDF.keySet()){
//					System.out.println("friend: " + friend);
					double querytfidf = QueryTFIDF.get(friend);
					
					String which = null;
					if((friend.charAt(0) - 'a')<= 3){
						which = "0";
					}else if((friend.charAt(0) - 'e')<=3 ){
						which = "1";
					}else if(friend.charAt(0) - 'i'<=5){
						which = "2";
					}else if(friend.charAt(0) - 'o'<=5){
						which = "3";
					}else if(friend.charAt(0) - 'u'<=5){
						which = "4";
					}
					String whereDF = "DF" + which;
					String whereTF = "TF" + which;
					IndexDB.SetUptoCheck(whereDF);
					double friendndf = IndexDB.getNDF(friend);
					IndexDB.SetUptoCheck(whereTF);
					double friendtf = IndexDB.getTF(word, filename);
					double doctfidf = (a+(1-a)*friendtf/maxtf)*Math.log(friendndf);
					
//					System.out.println(filename);
//					System.out.println("query tfidf: " + querytfidf);
//					System.out.println("doc tfidf: " + doctfidf);
					sum += querytfidf*doctfidf;
//					divide1 += Math.pow(querytfidf,2);
//					divide2 += Math.pow(doctfidf, 2);

				}
//				System.out.println(sum);
//				System.out.println(Math.sqrt(divide1)*Math.sqrt(divide2));
//				double weight = sum/(Math.sqrt(divide1)*Math.sqrt(divide2));
				double weight = sum;
//				System.out.println(filename + " " + weight);
				if(filename.endsWith("/")){
					filename = filename.substring(0, filename.length()-1);
				}
				if(queue.size() == size){
					if(!QueueSet.contains(filename)){
						queue.add(new DocWeight(filename,weight));
						String name = queue.remove().filename;
						QueueSet.remove(name);
						QueueSet.add(filename);
					}
				}else{
					if(!QueueSet.contains(filename)){
						queue.add(new DocWeight(filename,weight));
						QueueSet.add(filename);
					}
				}
			}
		}
		
		while(queue.size()!=0){
			stack.push(queue.remove());
		}
		
		BufferedWriter bw = new BufferedWriter(new FileWriter("search/result.txt"));
		while(stack.size()!=0){
			DocWeight d = stack.pop();
			String filename = d.filename;
			double score = d.weight;
			bw.write(filename + " " + score + "\n");
		}
		bw.close();
	}

}
