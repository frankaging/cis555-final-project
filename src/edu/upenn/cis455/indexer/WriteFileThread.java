package edu.upenn.cis455.indexer;

import java.io.FileNotFoundException;
import java.io.UnsupportedEncodingException;

public class WriteFileThread extends Thread{
    private String where;
    public WriteFileThread(String where){
    	this.where = where;
    }
	public void run(){
		long start = System.currentTimeMillis();
		try {
			System.out.println("start: " + where);
			int count = Prepare.WriteInput(where);
			System.out.println("total docs number: " + count);
		} catch (FileNotFoundException e) {
			e.printStackTrace();
		} catch (UnsupportedEncodingException e) {
			e.printStackTrace();
		}
		long end  = System.currentTimeMillis();
		System.out.println("total time: " + (end - start));
	}
}
