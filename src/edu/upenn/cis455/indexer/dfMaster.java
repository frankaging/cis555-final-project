package edu.upenn.cis455.indexer;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileReader;
import java.io.IOException;
import java.net.HttpURLConnection;
import java.net.URL;
import java.util.HashMap;


public class dfMaster extends Thread{
	
	private boolean masterrun = true;
	private String IP0 = "54.147.214.196:9002";
	private String IP1 = "54.174.200.203:9002";
	private String IP2 = "54.152.191.123:9002";
	private String IP3 = "54.162.211.149:9002";
	private String IP4 = "54.209.47.238:9002";
	private String request = "/ndf";
	private HashMap<String,Integer> wordstoworker = new HashMap<String,Integer>();
	private HashMap<Integer, String> workertoip = new HashMap<Integer,String>();
	
	public dfMaster(){
		wordstoworker.put("a", 0);wordstoworker.put("b", 0);wordstoworker.put("c", 0);wordstoworker.put("d", 0);
		wordstoworker.put("e", 1);wordstoworker.put("f", 1);wordstoworker.put("g", 1);wordstoworker.put("h", 1);
		wordstoworker.put("i", 2);wordstoworker.put("j", 2);wordstoworker.put("k", 2);wordstoworker.put("l", 2);
		wordstoworker.put("m", 2);wordstoworker.put("n", 2);wordstoworker.put("o", 3);wordstoworker.put("p", 3);
		wordstoworker.put("q", 3);wordstoworker.put("r", 3);wordstoworker.put("s", 3);wordstoworker.put("t", 3);
		wordstoworker.put("u", 4);wordstoworker.put("v", 4);wordstoworker.put("w", 4);wordstoworker.put("x", 4);
		wordstoworker.put("y", 4);wordstoworker.put("z", 4);
		workertoip.put(0, IP0);
		workertoip.put(1, IP1);
		workertoip.put(2, IP2);
		workertoip.put(3, IP3);
		workertoip.put(4, IP4);
	}
	
	public boolean getRun(){
		return masterrun;
	}
	
	public void run(){
		long start = System.currentTimeMillis();
    	File outputdir = new File("output");
    	File[] dfResults = new File(outputdir,"dfResult").listFiles();
    	//remember to change
    	int count = 100000;
    	
    	for(File dfResultFile : dfResults){
    		// skip success file
    		if(dfResultFile.getName().startsWith(".")){
    			continue;
    		}
    		try{
    			BufferedReader df = new BufferedReader(new FileReader(dfResultFile));
    			String line = df.readLine();

    			while(line != null){
    				String[] split = line.split("\t");
    				String word = split[0];
    				double dfnum = Double.parseDouble(split[1]);
    				double ndf = (double)count/dfnum;

    				String ip = workertoip.get(wordstoworker.get(word.substring(0,1)));
    				String url = "http://" + ip + request; 
    				String query = "?"+"word="+word+"&"+"ndf="+ndf+"&"+"run="+"1";
    				if(!word.matches("\\s+")){
    					try{
    						URL urlobj = new URL(url+query);
    						System.out.println("master send: " + url+query);
    						HttpURLConnection conn = (HttpURLConnection)urlobj.openConnection();
    						conn.setDoOutput(true);
    						conn.setRequestMethod("GET");
    						conn.connect();
    						//System.out.println(conn.getResponseCode());
    						if(conn.getResponseCode()!=HttpURLConnection.HTTP_OK){
    							throw new RuntimeException("send request ro report status failed");
    						}
    						conn.disconnect();
    					}catch(Exception e){
    						e.printStackTrace();
    					}
    					line = df.readLine();
    				}else{
    					line = df.readLine();
    				}
    			}
    			df.close();
    		}catch(IOException e){
    			e.printStackTrace(); 
    		}
    	}
    	for(Integer index : workertoip.keySet()){
			String ip = workertoip.get(index);
			try{
				String url = "http://" + ip + request; 
				String query = "?word=null&ndf=null&run=0";
				URL urlobj = new URL(url+query);
				HttpURLConnection conn = (HttpURLConnection)urlobj.openConnection();
				conn.setDoOutput(true);
				conn.setRequestMethod("GET");
				conn.connect();
				if(conn.getResponseCode()!=HttpURLConnection.HTTP_OK){
					throw new RuntimeException("send request ro report status failed");
				}
				conn.disconnect();
			}catch(Exception e){
				e.printStackTrace();
			}
		}
    	masterrun = false;
		System.out.println("read all df output");
		long df = System.currentTimeMillis();
		System.out.println("df-time: " + (df-start));
    }

}
