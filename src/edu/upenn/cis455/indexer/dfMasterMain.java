package edu.upenn.cis455.indexer;

public class dfMasterMain {
	private static boolean start = false;
	
	public static void main(String[] args){
		dfMaster master = new dfMaster();
		while(true){
			if(!start){
				master.start();
				System.out.println("start master");
				start = true;
			}
			if(!master.getRun()){
				master.interrupt();
				System.out.println("close master");
				break;
			}
		}
		System.out.println("SUCCESS");
	}

}
