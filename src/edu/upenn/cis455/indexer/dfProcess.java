package edu.upenn.cis455.indexer;

import java.util.LinkedList;

import edu.upenn.cis455.storage.IndexDB;

public class dfProcess {
	
	public static void main(String args[]) throws InterruptedException{
		//IndexDB.SetUptoCheck("IndexDB");
		IndexDB.SetUp("IndexDB");
		LinkedList<dfThread> list = new LinkedList<dfThread>();
		
		for(int i=0;i<5;i++){
			dfThread df = new dfThread(i);
			list.add(df);
		}
		
		while(list.size()!=0){
			list.poll().start();
		}
	}

}
