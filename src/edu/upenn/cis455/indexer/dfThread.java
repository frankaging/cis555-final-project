package edu.upenn.cis455.indexer;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileReader;
import java.io.IOException;

import edu.upenn.cis455.storage.IndexDB;
import edu.upenn.cis455.storage.TermInfo;

public class dfThread extends Thread{
    private String dir;
	
	public dfThread(int index){
		//mapreduce df output
		//dfThread input
		dir = "dfResult" + index;
	}
    public void run(){
    	long start = System.currentTimeMillis();
    	File outputdir = new File("output");
    	File[] dfResults = new File(outputdir, dir).listFiles();
    	int count = 50;
    	
    	for(File dfResultFile : dfResults){
			// skip success file
			if(dfResultFile.getName().startsWith(".")){
				continue;
			}
			BufferedReader df = null;
			String line = null;
			try {
				df = new BufferedReader(new FileReader(dfResultFile));
				line = df.readLine();
			} catch (IOException e) {
				e.printStackTrace();
			}
			
			while(line != null){
				String[] split = line.split("\t");
				String word = split[0];
				double dfnum = Double.parseDouble(split[1]);
				if(!word.matches("\\s+")){
					TermInfo term = new TermInfo(word);
					term.setNDF((double)count/dfnum);
					IndexDB.putTerm(term);
					try {
						line = df.readLine();
					} catch (IOException e) {
						e.printStackTrace();
					}
				}else{
					try {
						line = df.readLine();
					} catch (IOException e) {
						e.printStackTrace();
					}
				}
			}
			try {
				df.close();
			} catch (IOException e) {
				e.printStackTrace();
			}
		}
		System.out.println("read all df output");
		long df = System.currentTimeMillis();
		System.out.println("df-time: " + (df-start));
    }
}
