package edu.upenn.cis455.indexer;

import java.net.MalformedURLException;
import java.util.HashMap;
import java.util.concurrent.BlockingQueue;
import java.util.concurrent.LinkedBlockingQueue;

import edu.upenn.cis455.storage.IndexDB;

public class dfWorkerMain {
	private static BlockingQueue<HashMap<String,String>> bq = new LinkedBlockingQueue<HashMap<String,String>>();
	
	
	public static void main(String args[]) throws MalformedURLException, InterruptedException{
		IndexDB.SetUp("IndexDFDB");
		int port0 = 9002;
		boolean start = false;
		dfWorkerThread df0 = new dfWorkerThread(port0);
		
		while(true){
			if(!start){
				df0.start();
				System.out.println("start worker");
				start = true;
			}
			if(!df0.getRun()){
				df0.interrupt();
				System.out.println("close worker");
				break;
			}
		}
		System.out.println("SUCCESS");

	}
	
	public static void enqueue(HashMap<String,String> map){
		try{
			bq.add(map);
		}catch(Exception e){
			e.printStackTrace();
			System.out.println("queue is full!!");
		}
	}
	
	
	public static int getSize(){
		return bq.size();
	}
	
	public static HashMap<String,String> dequeue(){
		return bq.poll();
	}

}
