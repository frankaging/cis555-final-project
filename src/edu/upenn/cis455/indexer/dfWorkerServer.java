package edu.upenn.cis455.indexer;

import static spark.Spark.setPort;

import java.net.MalformedURLException;
import java.util.HashMap;

import com.fasterxml.jackson.databind.ObjectMapper;

import spark.Request;
import spark.Response;
import spark.Route;
import spark.Spark;

public class dfWorkerServer {
	public dfWorkerServer(int myPort) throws MalformedURLException {
		setPort(myPort);
		final ObjectMapper om = new ObjectMapper();
		om.enableDefaultTyping(ObjectMapper.DefaultTyping.NON_FINAL);
		Spark.get(new Route("/ndf") {

			@Override
			public Object handle(Request arg0, Response arg1) {
				String word = arg0.queryParams("word");
				String ndf = arg0.queryParams("ndf");
				String ifrun = arg0.queryParams("run");
                HashMap<String,String> map = new HashMap<String,String>();
                map.put("word", word);
                map.put("ndf", ndf);
                map.put("run", ifrun);
                dfWorkerMain.enqueue(map);
				return "OK";
			}
		});
	}
	
}
