package edu.upenn.cis455.indexer;

import java.net.MalformedURLException;
import java.util.HashMap;

import edu.upenn.cis455.storage.IndexDB;
import edu.upenn.cis455.storage.TermInfo;

public class dfWorkerThread extends Thread{
	private boolean runflag = true;
	
	public dfWorkerThread(int port) throws MalformedURLException{
		new dfWorkerServer(port);
	}
	
	public void run(){
		while(runflag){
			HashMap<String,String> map = dfWorkerMain.dequeue();
			if(map!=null){
				String word = map.get("word");
				String ndf = map.get("ndf");
				Integer ifrun= Integer.parseInt(map.get("run"));
				if(ifrun == 0){
					System.out.println("Thread Stop!");
					runflag = false;
					break;
				}
				double ndfnum = Double.parseDouble(ndf);
				if(!word.matches("\\s+")){
					System.out.println("word in thread:" + word);
					TermInfo term = new TermInfo(word);
					term.setNDF(ndfnum);
					IndexDB.putTerm(term);
				}
			}
		}
        System.out.println("=================================end=================================");
	}
	
	public boolean getRun(){
		return runflag;
	}

}
