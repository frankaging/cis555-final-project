package edu.upenn.cis455.indexer;

import java.io.BufferedReader;
import java.io.BufferedWriter;
import java.io.File;
import java.io.FileReader;
import java.io.FileWriter;
import java.io.IOException;
import java.net.HttpURLConnection;
import java.net.URL;
import java.util.HashMap;

import edu.upenn.cis455.storage.IndexDB;



public class tfMaster extends Thread{
	
	private boolean masterrun = true;
//	private String IP0 = "54.209.157.65:9002";
//	private String IP1 = "54.164.144.8:9002";
//	private String IP2 = "54.91.10.144:9002";
//	private String IP3 = "54.146.154.36:9002";
//	private String IP4 = "54.209.189.104:9002";
	
	private String IP0 = "0.0.0.0:9002";
	private String IP1 = "0.0.0.0:9002";
	private String IP2 = "0.0.0.0:9002";
	private String IP3 = "0.0.0.0:9002";
	private String IP4 = "0.0.0.0:9002";
	private String request = "/tf";
	private HashMap<String,Integer> wordstoworker = new HashMap<String,Integer>();
	private HashMap<Integer, String> workertoip = new HashMap<Integer,String>();
	//LinkedList<IndexDBHelper> helpers = new LinkedList<IndexDBHelper>();
	
	public boolean getRun(){
		return masterrun;
	}
	
	public tfMaster(){
		wordstoworker.put("a", 0);wordstoworker.put("b", 0);wordstoworker.put("c", 0);wordstoworker.put("d", 0);
		wordstoworker.put("e", 1);wordstoworker.put("f", 1);wordstoworker.put("g", 1);wordstoworker.put("h", 1);
		wordstoworker.put("i", 2);wordstoworker.put("j", 2);wordstoworker.put("k", 2);wordstoworker.put("l", 2);
		wordstoworker.put("m", 2);wordstoworker.put("n", 2);wordstoworker.put("o", 3);wordstoworker.put("p", 3);
		wordstoworker.put("q", 3);wordstoworker.put("r", 3);wordstoworker.put("s", 3);wordstoworker.put("t", 3);
		wordstoworker.put("u", 4);wordstoworker.put("v", 4);wordstoworker.put("w", 4);wordstoworker.put("x", 4);
		wordstoworker.put("y", 4);wordstoworker.put("z", 4);
		workertoip.put(0, IP0);
		workertoip.put(1, IP1);
		workertoip.put(2, IP2);
		workertoip.put(3, IP3);
		workertoip.put(4, IP4);
		
//		IndexDBHelper d0 = new IndexDBHelper();
//		IndexDBHelper d1 = new IndexDBHelper();
//		IndexDBHelper d2 = new IndexDBHelper();
//		IndexDBHelper d3 = new IndexDBHelper();
//		IndexDBHelper d4 = new IndexDBHelper();
//		d0.SetUptoCheck("IndexDB0");
//		d1.SetUptoCheck("IndexDB1");
//		d2.SetUptoCheck("IndexDB2");
//		d3.SetUptoCheck("IndexDB3");
//		helpers.add(d0);
//		helpers.add(d1);
//		helpers.add(d2);
//		helpers.add(d3);
//		helpers.add(d4);
	}
	
	public void run(){
		long start = System.currentTimeMillis();
		File outputdir = new File("output");
		File[] tfResults = new File(outputdir, "tfResult").listFiles();


		File tfmax = new File(outputdir,"tfMax.txt");
		tfmax.delete();
		try {
			tfmax.createNewFile();
		} catch (IOException e) {
			e.printStackTrace();
		}
		BufferedWriter w = null ;
		try {
			w = new BufferedWriter(new FileWriter(tfmax,true));
		} catch (IOException e1) {
			e1.printStackTrace();
		}

		try{
			double maxtf = 0;
			for(File tfResultFile : tfResults){
				if(tfResultFile.getName().startsWith(".")){
					continue;
				}
				BufferedReader tf = new BufferedReader(new FileReader(tfResultFile));
				String line = tf.readLine();
				while(line != null){
					String[] split = line.split("\t");
					String[] keysplit = split[0].split("\\s+");
					String word = keysplit[0];
					
					boolean iflong = false;
	                boolean ifshort = false;
	                boolean ifmany = false;
	                if(word.length()>8) {iflong = true;}
	                if(word.length() >=2 && (word.substring(0,1).equals(word.substring(1,2)))){ ifmany = true;}
	                if(word.length() <=2) {ifshort = true;}
	                if(!(word.startsWith("do"))){
	                	line = tf.readLine();
	                	continue;
	                }
	                if(iflong || ifmany || ifshort){
	                	line = tf.readLine();
	                	continue;
	                }
	                
					String fakefilename = keysplit[1];
					String index = fakefilename.substring(0, fakefilename.indexOf("."));
//					String filename = null;
					//fa1c85fe-cdd5-43c1-bc13-6236ff749140
//					for(IndexDBHelper helper : helpers){
//						IndexURL tempurl = helper.getIndexURL(index);
//						System.out.println(index);
//						if(index.equals("fa1c85fe-cdd5-43c1-bc13-6236ff749140") && tempurl == null){
//							System.out.println("fuck");
//						}
//						if(tempurl!=null){
//							System.out.println(index);
//							filename = tempurl.getURL();
//							break;
//						}		
//					}
//					for(int temp=0;temp<4;temp++){
//						IndexDB.SetUptoCheck("IndexDB"+temp);
//						if(IndexDB.getIndexURL(index)!=null){
//							filename = IndexDB.getIndexURL(index).getURL();
//							break;
//						}
//					}
					int sum = Integer.parseInt(split[1]);
					
    				String ip = workertoip.get(wordstoworker.get(word.substring(0,1)));
    				String url = "http://" + ip + request; 
    				
					if(!word.matches("\\s+")){					
						Integer totalwords = null;
//						for(IndexDBHelper helper : helpers){
//							Integer tempnum = helper.getIndexURL(index).getSize();
//							if(tempnum!=null){
//								totalwords = tempnum;
//								break;
//							}		
//						}
						for(int temp=0;temp<4;temp++){
							IndexDB.SetUptoCheck("IndexDB"+temp);
							if(IndexDB.getIndexURL(index)!=null){
								totalwords = IndexDB.getIndexURL(index).getSize();
								break;
							}
						}
						double tfnum = (double)sum/totalwords;
						if(tfnum>maxtf){
							maxtf = tfnum;
						}
						
						
						String query = "?"+"word="+word+"&"+"index="+index+"&"+"tf="+tfnum+"&run=1";
						try{
    						URL urlobj = new URL(url+query);
    						System.out.println("master send: " + url+query);
    						HttpURLConnection conn = (HttpURLConnection)urlobj.openConnection();
    						conn.setDoOutput(true);
    						conn.setRequestMethod("GET");
    						conn.connect();
    						//System.out.println(conn.getResponseCode());
    						if(conn.getResponseCode()!=HttpURLConnection.HTTP_OK){
    							throw new RuntimeException("send request ro report status failed");
    						}
    						conn.disconnect();
    						
    					}catch(Exception e){
    						e.printStackTrace();
    					}
						line = tf.readLine();
					}else{
						line = tf.readLine();
					}
				}
				tf.close();
			}
			for(Integer index : workertoip.keySet()){
				String ip = workertoip.get(index);
				try{
					String url = "http://" + ip + request; 
					String query = "?word=null&index=null&tf=null&run=0";
					URL urlobj = new URL(url+query);
					HttpURLConnection conn = (HttpURLConnection)urlobj.openConnection();
					conn.setDoOutput(true);
					conn.setRequestMethod("POST");
					conn.connect();
					if(conn.getResponseCode()!=HttpURLConnection.HTTP_OK){
						throw new RuntimeException("send request ro report status failed");
					}
					conn.disconnect();
				
				}catch(Exception e){
					e.printStackTrace();
				}
			}
			w.write(String.valueOf(maxtf) + "\n");
			w.close();
			masterrun = false;
			long end = System.currentTimeMillis();
			System.out.println("tf-time:" + (end-start));
		}catch(IOException e){
			e.printStackTrace();
		}

	}

}
