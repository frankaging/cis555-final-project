package edu.upenn.cis455.indexer;

import java.net.MalformedURLException;


public class tfMasterMain {
	public static void main(String args[]) throws MalformedURLException, InterruptedException{
	
		tfMaster master = new tfMaster();

        boolean start = false;
		while(true){
			if(!start){
				master.start();
				System.out.println("start master");
				start = true;
			}
			if(!master.getRun()){
				master.interrupt();
				System.out.println("close master");
				break;
			}
		}
		System.out.println("SUCCESS");
	}

}
