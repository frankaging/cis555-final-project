package edu.upenn.cis455.indexer;

import java.net.MalformedURLException;
import java.util.HashMap;
import java.util.concurrent.BlockingQueue;
import java.util.concurrent.LinkedBlockingQueue;

import edu.upenn.cis455.storage.IndexDB;

public class tfWorkerMain {
	private static BlockingQueue<HashMap<String,String>> tfq = new LinkedBlockingQueue<HashMap<String,String>>();
	
	
	public static void main(String args[]) throws MalformedURLException, InterruptedException{
		IndexDB.SetUp("IndexTFDB");
		int port0 = 9002;	
		tfWorkerThread tf0 = new tfWorkerThread(port0);		
		tfMaster master = new tfMaster();

        boolean start = false;
		while(true){
			if(!start){
				tf0.start();
				master.start();
				System.out.println("start worker");
				start = true;
			}
			if(!tf0.getRun()){
				tf0.interrupt();
				master.interrupt();
				System.out.println("close worker");
				break;
			}
		}
		System.out.println("SUCCESS");
	}
	
	public static void enqueue(HashMap<String,String> map){
		try{
			tfq.add(map);
		}catch(Exception e){
			e.printStackTrace();
			System.out.println("queue is full!!");
		}
	}
	
	
	public static int getSize(){
		return tfq.size();
	}
	
	public static HashMap<String,String> dequeue(){
		return tfq.poll();
	}

}
