package edu.upenn.cis455.indexer;

import static spark.Spark.setPort;


import java.net.MalformedURLException;
import java.util.HashMap;

import com.fasterxml.jackson.databind.ObjectMapper;

import edu.upenn.cis455.storage.IndexDB;
import spark.Request;
import spark.Response;
import spark.Route;
import spark.Spark;

public class tfWorkerServer {
	public tfWorkerServer(int myPort) throws MalformedURLException {
		setPort(myPort);
		final ObjectMapper om = new ObjectMapper();
		om.enableDefaultTyping(ObjectMapper.DefaultTyping.NON_FINAL);
		Spark.get(new Route("/tf") {

			@Override
			public Object handle(Request arg0, Response arg1) {
				String word = arg0.queryParams("word");
				String index = arg0.queryParams("index");
				String tf = arg0.queryParams("tf");
				String ifrun = arg0.queryParams("run");
				String filename = null;
				for(int temp=0;temp<4;temp++){
					IndexDB.SetUptoCheck("IndexDB"+temp);
					if(IndexDB.getIndexURL(index)!=null){
						filename = IndexDB.getIndexURL(index).getURL();
						break;
					}
				}
				HashMap<String,String> map = new HashMap<String,String>();
                map.put("word", word);
                map.put("filename", filename);
                map.put("tf", tf);
                map.put("run", ifrun);
                tfWorkerMain.enqueue(map);
                return "OK";
			}
		});
		
		Spark.get(new Route("/indexing") {

			@Override
			public Object handle(Request arg0, Response arg1) {
				String word = arg0.queryParams("word");
				String filename = arg0.queryParams("filename");
				String tf = arg0.queryParams("tf");
				String ifrun = arg0.queryParams("run");
				HashMap<String,String> map = new HashMap<String,String>();
				map.put("word", word);
				map.put("filename",filename);
				map.put("tf", tf);
				map.put("run", ifrun);
                tfWorkerMain.enqueue(map);
				return "OK";
			}
		});
	}
	public void ShutDown(){
		
	}
	
}
