package edu.upenn.cis455.indexer;

import java.net.MalformedURLException;
import java.util.HashMap;

import edu.upenn.cis455.storage.IndexDB;

public class tfWorkerThread extends Thread{
	private boolean runflag = true;
	
	public tfWorkerThread(int port) throws MalformedURLException{
		new tfWorkerServer(port);
	}
	
	public void run(){
		while(runflag){
			HashMap<String,String> map = tfWorkerMain.dequeue();
			if(map!=null){
				
                String word = map.get("word");
                String filename = map.get("filename");
				String tf = map.get("tf");
				Integer ifrun= Integer.parseInt(map.get("run"));
				if(ifrun == 0){
					System.out.println("Thread Stop!");
					runflag = false;
					break;
				}
				double tfnum = Double.parseDouble(tf);
				if(!word.matches("\\s+")){
					System.out.println("word in thread:" + word + "filename: " + filename);
					IndexDB.addTF(word, filename, tfnum);
				}
			}
		}
        System.out.println("=================================end=================================");
	}
	
	public boolean getRun(){
		return runflag;
	}
}
