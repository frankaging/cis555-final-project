package edu.upenn.cis455.indexer;

import java.io.File;
import java.util.LinkedList;

import edu.upenn.cis455.storage.IndexDBHelper;

public class tfidfProcess {
	
    public static void deleteDir(File dir) {
		for(File f: dir.listFiles()) {
			if(f.isDirectory())
				deleteDir(f);
			else 
				f.delete();
		}
		dir.delete();
	}

	public static void main(String args[]) throws InterruptedException{
		IndexDBHelper d0 = new IndexDBHelper();
		IndexDBHelper d1 = new IndexDBHelper();
		IndexDBHelper d2 = new IndexDBHelper();
		IndexDBHelper d3 = new IndexDBHelper();
		IndexDBHelper d4 = new IndexDBHelper();
		d0.SetUptoCheck("IndexDB0");
		d1.SetUptoCheck("IndexDB1");
		d2.SetUptoCheck("IndexDB2");
		d3.SetUptoCheck("IndexDB3");
		LinkedList<IndexDBHelper> helpers = new LinkedList<IndexDBHelper>();
		helpers.add(d0);
		helpers.add(d1);
		helpers.add(d2);
		helpers.add(d3);
		helpers.add(d4);
		LinkedList<tfidfThread> list = new LinkedList<tfidfThread>();
		
		File outputdir = new File("output");
		File outputDir = new File(outputdir,"TFResultPrint");
		deleteDir(outputDir);
		outputDir.mkdirs();
		
		File tfmax = new File(outputdir,"tfMax.txt");
		if(tfmax.exists()){
			tfmax.delete();
		}
		
		for(int i=0;i<4;i++){
			tfidfThread tfidf = new tfidfThread(i,helpers);
			list.add(tfidf);
		}
		
		while(list.size()!=0){
			list.poll().start();
		}
	}
}
