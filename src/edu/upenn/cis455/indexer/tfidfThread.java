package edu.upenn.cis455.indexer;

import java.io.BufferedReader;
import java.io.BufferedWriter;
import java.io.File;
import java.io.FileReader;
import java.io.FileWriter;
import java.io.IOException;
import java.util.LinkedList;
import edu.upenn.cis455.storage.IndexDBHelper;
import edu.upenn.cis455.storage.IndexURL;

public class tfidfThread extends Thread{
	private int index;
    private String dir;
    private LinkedList<IndexDBHelper> helpers;
    
	
    public tfidfThread(int i,LinkedList<IndexDBHelper> helpers){
		index = i;
		dir = "tfResult" + i;
		this.helpers = helpers;
	}
    
	public void run(){
		long start = System.currentTimeMillis();
		File outputdir = new File("output");
		File outputDir = new File(outputdir,"TFResultPrint");
		File[] tfResults = new File(outputdir, dir).listFiles();
		
		File tfResultsPrint = new File(outputDir, "TFResultPrint" + index + ".txt");
		
		File tfmax = new File(outputdir,"tfMax.txt");
		if(!tfmax.exists()){
			try {
				tfmax.createNewFile();
			} catch (IOException e) {
				e.printStackTrace();
			}
		}
		BufferedWriter w = null ;
		try {
			w = new BufferedWriter(new FileWriter(tfmax,true));
		} catch (IOException e1) {
			e1.printStackTrace();
		}

		try{
			tfResultsPrint.createNewFile();
			BufferedWriter tfwriter = new BufferedWriter(new FileWriter(tfResultsPrint));
			StringBuffer tfbuffer = new StringBuffer();
			double maxtf = 0;
			for(File tfResultFile : tfResults){
				if(tfResultFile.getName().startsWith(".")){
					continue;
				}
				BufferedReader tf = new BufferedReader(new FileReader(tfResultFile));
				String line = tf.readLine();
				while(line != null){
					if(tfbuffer.length() >= 1024){
						tfwriter.write(tfbuffer.toString());
						tfbuffer = new StringBuffer();
					}
					String[] split = line.split("\t");
					String[] keysplit = split[0].split("\\s+");
					String word = keysplit[0];
					String fakefilename = keysplit[1];
					String index = fakefilename.substring(0, fakefilename.indexOf("."));
					String filename = null;
					for(IndexDBHelper helper : helpers){
						IndexURL tempurl = helper.getIndexURL(index);
						if(tempurl!=null){
							filename = tempurl.getURL();
							break;
						}		
					}
					int sum = Integer.parseInt(split[1]);
					if(!word.matches("\\s+") && !filename.matches("\\s+")){					
						Integer totalwords = null;
						for(IndexDBHelper helper : helpers){
							Integer tempnum = helper.getIndexURL(index).getSize();
							if(tempnum!=null){
								totalwords = tempnum;
								break;
							}		
						}
						double tfnum = (double)sum/totalwords;
						if(tfnum>maxtf){
							maxtf = tfnum;
						}
						String towrite = word + " " + filename + " " + tfnum + "\n";
						tfbuffer.append(towrite);
						line = tf.readLine();
					}else{
						line = tf.readLine();
					}
				}
				if(tfbuffer.length()!=0){
					tfwriter.write(tfbuffer.toString());
					tfbuffer = new StringBuffer();
				}
				tf.close();
			}
			w.write(String.valueOf(maxtf) + "\n");
			w.close();
			tfwriter.close();
			long end = System.currentTimeMillis();
			System.out.println("tf-time:" + (end-start));
		}catch(IOException e){
			e.printStackTrace();
		}
	}
}
