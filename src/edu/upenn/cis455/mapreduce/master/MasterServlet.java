package edu.upenn.cis455.mapreduce.master;

import java.io.File;
import java.io.IOException;
import java.io.OutputStream;
import java.io.PrintWriter;
import java.net.HttpURLConnection;
import java.net.URL;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Scanner;

import javax.servlet.ServletException;
import javax.servlet.annotation.MultipartConfig;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;

import edu.upenn.cis.stormlite.Config;
import edu.upenn.cis.stormlite.Topology;
import edu.upenn.cis.stormlite.TopologyBuilder;
import edu.upenn.cis.stormlite.bolt.CrawlerBolt;
import edu.upenn.cis.stormlite.bolt.DocumentParserBolt;
import edu.upenn.cis.stormlite.bolt.URLFilterBolt;
import edu.upenn.cis.stormlite.bolt.URLInfo;
import edu.upenn.cis.stormlite.bolt.URLMapBolt;
import edu.upenn.cis.stormlite.distributed.WorkerHelper;
import edu.upenn.cis.stormlite.distributed.WorkerJob;
import edu.upenn.cis.stormlite.spout.CrawlerQueueSpout;
import edu.upenn.cis.stormlite.tuple.Fields;
import edu.upenn.cis.stormlite.tuple.Tuple;
import edu.upenn.cis.stormlite.tuple.Values;
import edu.upenn.cis455.mapreduce.worker.MainWorker;

/**
 * This is the master that could send job to workers and render the curren
 * status of workers
 * 
 * @author Kevin Ye
 *
 */
@MultipartConfig
public class MasterServlet extends HttpServlet {

	static final long serialVersionUID = 455555001;
	HashMap<Integer, WorkerStatus> workerMap;
	final static String queue_spout = "CRAWLER_QUEUE_SPOUT";
	final static String crawler_bolt = "CRAWLER_BOLT";
	final static String parser_bolt = "DOCUMENT_PARSER_BOLT";
	final static String url_bolt = "URLFILTER_BOLT";
	final static String urlMap_bolt = "URLMAP_BOLT";
	List<String> workerList;

	@Override
	public void init() {
		workerMap = new HashMap<Integer, WorkerStatus>();
		WorkerStatus w1 = new WorkerStatus("54.87.152.13", 8001);
		WorkerStatus w2 = new WorkerStatus("52.90.1.183", 8002);
		WorkerStatus w3 = new WorkerStatus("54.87.135.165", 8003);
		WorkerStatus w4 = new WorkerStatus("54.160.118.31", 8004);
//		WorkerStatus w1 = new WorkerStatus("54.242.154.100", 8001);
//		WorkerStatus w2 = new WorkerStatus("54.147.194.54", 8002);
//		WorkerStatus w3 = new WorkerStatus("54.145.146.93", 8003);
//		WorkerStatus w4 = new WorkerStatus("54.221.169.60", 8004);
//		WorkerStatus w1 = new WorkerStatus("127.0.0.1", 8001);
//		WorkerStatus w2 = new WorkerStatus("127.0.0.1", 8002);
//		WorkerStatus w3 = new WorkerStatus("127.0.0.1", 8003);
//		WorkerStatus w4 = new WorkerStatus("127.0.0.1", 8004);
		workerList = new ArrayList<>();
//		workerList.add("127.0.0.1:8001");
//		workerList.add("127.0.0.1:8002");
//		workerList.add("127.0.0.1:8003");
//		workerList.add("127.0.0.1:8004");
//		workerList.add("54.242.154.100:8001");
//		workerList.add("54.147.194.54:8002");
//		workerList.add("54.145.146.93:8003");
//		workerList.add("54.221.169.60:8004");
		workerList.add("54.87.152.13:8001");
		workerList.add("52.90.1.183:8002");
		workerList.add("54.87.135.165:8003");
		workerList.add("54.160.118.31:8004");

		workerMap.put(8001, w1);
		workerMap.put(8002, w2);
		workerMap.put(8003, w3);
		workerMap.put(8004, w4);
		
	}

	@Override
	public void doPost(HttpServletRequest request, HttpServletResponse response) throws IOException, ServletException {

		String uri = request.getRequestURI();
		Config config = new Config();

		config.put("job", "crawler");
		// IP:port for /workerstatus to be sent
//		config.put("master", "127.0.0.1:8000");
//
		config.put("master", "54.92.232.109:8000");
		
		config.put("workerList", MainWorker.generateWorkerList(workerList));

		if (uri.equals("/create")){

			WorkerJob wj = createJob(config);

			startSending(wj, config);
		} else {
			for (int port : workerMap.keySet()) {
				WorkerStatus worker = workerMap.get(port);
				String dest = "http://" + worker.ipAddress + ":" + worker.port;
				if (sendJob(dest, "POST", config, "update", "").getResponseCode() != HttpURLConnection.HTTP_OK) {
					throw new RuntimeException("Job update request failed");
				}

			}
		}

		response.sendRedirect("status");
	}

	@Override
	public void doGet(HttpServletRequest request, HttpServletResponse response) throws java.io.IOException {
		response.setContentType("text/html");

		String uri = request.getRequestURI();
		if (uri.endsWith("/workerstatus")) {
			processWorkerStatus(request, response);
		} else if (uri.endsWith("/status")) {
			renderStatus(request, response);
		} else if (uri.endsWith("/shutdown")) {
			shutdown();
		}
	}

	/*
	 * handle checkin from workers
	 */
	public synchronized void processWorkerStatus(HttpServletRequest request, HttpServletResponse response) throws IOException {

		int port = Integer.parseInt(request.getParameter("port"));
		long count = Long.parseLong(request.getParameter("count"));
		String ip = request.getRemoteAddr();

		WorkerStatus ws = new WorkerStatus(ip, port);
		ws.count = count;
		
		workerMap.put(port, ws);
		
		response.sendRedirect("status");

	}

	public void startSending(WorkerJob wj, Config config) throws IOException {
		ObjectMapper mapper = new ObjectMapper();
		mapper.enableDefaultTyping(ObjectMapper.DefaultTyping.NON_FINAL);
		try {
			int i = 0;
			for (int port : workerMap.keySet()) {
				WorkerStatus worker = workerMap.get(port);
				config.put("workerIndex", String.valueOf(i++));
				String dest = "http://" + worker.ipAddress + ":" + worker.port;
				if (sendJob(dest, "POST", config, "definejob",
						mapper.writerWithDefaultPrettyPrinter().writeValueAsString(wj))
								.getResponseCode() != HttpURLConnection.HTTP_OK) {
					throw new RuntimeException("Job definition request failed");
				}

			}
			for (int port : workerMap.keySet()) {
				WorkerStatus worker = workerMap.get(port);
				String dest = "http://" + worker.ipAddress + ":" + worker.port;
				if (sendJob(dest, "POST", config, "runjob", "").getResponseCode() != HttpURLConnection.HTTP_OK) {
					throw new RuntimeException("Job execution request failed");
				}

			}
			File urls = new File("../urls.txt");
			Scanner in = new Scanner(urls);
			while (in.hasNextLine()){
				String crawlUrl = in.nextLine();
				if (crawlUrl.equals("")) continue;
				URLInfo wiki = new URLInfo(crawlUrl);
				int hash = wiki.getHostName().hashCode();
				Fields schema = new Fields("url", "host");
				Values<Object> values = new Values<Object>(wiki.getUrl(), wiki.getHostName());
				Tuple tuple = new Tuple(schema, values);
				tuple.setEndOfStream(false);
				hash = hash % workerList.size();
				if (hash < 0) {
					hash += workerList.size();
				}
				String[] workers = WorkerHelper.getWorkers(config);
				URL url = new URL(workers[hash] + "/pushdata/" + urlMap_bolt);
				System.out.println(workers[hash]);
				HttpURLConnection conn = (HttpURLConnection) url.openConnection();
				conn.setRequestProperty("Content-Type", "application/json");
				String jsonForTuple = mapper.writerWithDefaultPrettyPrinter().writeValueAsString(tuple);

				// TODO: send this to /pushdata/{stream} as a POST!
				conn.setRequestMethod("POST");
				conn.setRequestProperty("Content-Type", "application/json");
				conn.setDoOutput(true);
				OutputStream os = conn.getOutputStream();
				byte[] toSend = jsonForTuple.getBytes();
				os.write(toSend);
				os.flush();
				if (conn.getResponseCode() != HttpURLConnection.HTTP_OK) {
					throw new RuntimeException("Worker Communication fail");
				}
				os.close();
				conn.disconnect();
			}
			in.close();
			
		} catch (JsonProcessingException e) {
			e.printStackTrace();
		}
	}

	/*
	 * send request to all workers
	 */
	private HttpURLConnection sendJob(String dest, String reqType, Config config, String job, String parameters)
			throws IOException {
		URL url = new URL(dest + "/" + job);

		HttpURLConnection conn = (HttpURLConnection) url.openConnection();
		conn.setDoOutput(true);
		conn.setRequestMethod(reqType);

		if (reqType.equals("POST")) {
			conn.setRequestProperty("Content-Type", "application/json");

			OutputStream os = conn.getOutputStream();
			byte[] toSend = parameters.getBytes();
			os.write(toSend);
			os.flush();
		} else
			conn.getOutputStream();

		return conn;
	}

	/*
	 * handle /status request
	 */
	public synchronized void renderStatus(HttpServletRequest request, HttpServletResponse response) throws IOException {
		PrintWriter out = response.getWriter();
		out.println("<html><head><title>Master</title></head>");
		out.println("<body>");

		out.println("<h2>Name: Yufei Ye</h2>");
		out.println("<h2>SEAS login: yeyufei</h2>");
		out.println("<table border=\"1\">");
		out.println("<tr><th>IP:port</th><th>Number of pages crawled</th></tr>");
		for (int i : workerMap.keySet()) {
			WorkerStatus ws = workerMap.get(i);
			out.println("<tr>");
			out.println("<td>" + i + "</td>");
			out.println("<td>" + ws.count + "</td>");
			out.println("</tr>");
		}
		out.println("<h3>Fill in the info below to launch a new job</h3>");
		out.println("<form method=\"post\" action=\"create\"><br>");
		out.println("<button type=\"submit\">submit</button>");
		out.println("</form>");
		out.println("<form method=\"post\" action=\"sendupdate\"><br>");
		out.println("<button type=\"submit\">Request Update</button>");
		out.println("</form>");

		out.println("</body></html>");
	}

	/*
	 * handle shutdown request
	 */
	private void shutdown() throws IOException {

		for (int port : workerMap.keySet()) {
			WorkerStatus worker = workerMap.get(port);
			String dest = "http://" + worker.ipAddress + ":" + worker.port;
			URL url = new URL(dest + "/shutdown");

			HttpURLConnection conn = (HttpURLConnection) url.openConnection();
			conn.setDoOutput(true);
			conn.setRequestMethod("POST");
			conn.setRequestProperty("Content-Type", "application/json");

			OutputStream os = conn.getOutputStream();
			byte[] toSend = "".getBytes();
			os.write(toSend);
			os.flush();

			break;
		}
		System.exit(0);
	}

	private WorkerJob createJob(Config config) {

		CrawlerQueueSpout crawlerQueueSpout = new CrawlerQueueSpout();
		CrawlerBolt crawlerBolt = new CrawlerBolt();
		DocumentParserBolt documentParserBolt = new DocumentParserBolt();
		URLFilterBolt urlFilterBolt = new URLFilterBolt();
		URLMapBolt urlMapBolt = new URLMapBolt();

		TopologyBuilder builder = new TopologyBuilder();

		builder.setSpout(queue_spout, crawlerQueueSpout, 1);
		builder.setBolt(crawler_bolt, crawlerBolt, 4).fieldsGrouping(queue_spout, new Fields("host"));
		builder.setBolt(parser_bolt, documentParserBolt, 4).shuffleGrouping(crawler_bolt);
		builder.setBolt(url_bolt, urlFilterBolt, 4).shuffleGrouping(parser_bolt);
		builder.setBolt(urlMap_bolt, urlMapBolt, 1).shuffleGrouping(url_bolt);
		//
		// // Only one source ("spout") for the words
		// builder.setSpout(WORD_SPOUT, spout,
		// Integer.valueOf(config.get("spoutExecutors")));
		//
		// // Parallel mappers, each of which gets specific words
		// builder.setBolt(MAP_BOLT, bolt,
		// Integer.valueOf(config.get("mapExecutors"))).fieldsGrouping(WORD_SPOUT,
		// new Fields("value"));
		//
		// // Parallel reducers, each of which gets specific words
		// builder.setBolt(REDUCE_BOLT, bolt2,
		// Integer.valueOf(config.get("reduceExecutors"))).fieldsGrouping(MAP_BOLT,
		// new Fields("key"));
		//
		// // Only use the first printer bolt for reducing to a single point
		// builder.setBolt(WRITE_BOLT, writer, 1).firstGrouping(REDUCE_BOLT);

		Topology topo = builder.createTopology();

		WorkerJob job = new WorkerJob(topo, config);

		return job;
	}

}
