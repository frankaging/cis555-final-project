package edu.upenn.cis455.mapreduce.master;
/**
 * The status of each worker containing the number of pages it has crawled
 * @author kevin ye
 *
 */
public class WorkerStatus {
	public String ipAddress;
	public int port;
	public long count;
	
	public WorkerStatus(String ip, int port){
		this.ipAddress = ip;
		this.port = port;
	}

}
