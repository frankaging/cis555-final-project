package edu.upenn.cis455.mapreduce.worker;

import java.io.File;
import java.util.ArrayList;
import java.util.List;

import org.apache.log4j.Logger;

import edu.upenn.cis.stormlite.Config;
import edu.upenn.cis455.storage.DBWrapper;
import edu.upenn.cis455.storage.Queue;
import edu.upenn.cis455.storage.S3FileWriter;;

/**
 * Initialize worker node in here
 * 
 * @author Kevin Ye
 *
 */
public class MainWorker {
	static Logger log = Logger.getLogger(MainWorker.class);

	public static Queue urlQueue;

	/**
	 * Command line parameters:
	 * 
	 * Argument 0: worker index in the workerList (0, 1, ...) Argument 1:
	 * non-empty parameter specifying that this is the requestor (so we know to
	 * send the JSON)
	 * 
	 * @param args
	 * @throws Exception
	 */
	public static void main(String[] args) throws Exception {
		// BasicConfigurator.configure();
		List<String> workerList = new ArrayList<>();
//
//		workerList.add("127.0.0.1:8001");
//		workerList.add("127.0.0.1:8002");
//		workerList.add("127.0.0.1:8003");
//		workerList.add("127.0.0.1:8004");

//		workerList.add("54.242.154.100:8001");
//		workerList.add("54.147.194.54:8002");
//		workerList.add("54.145.146.93:8003");
//		workerList.add("54.221.169.60:8004");
		
		workerList.add("54.87.152.13:8001");
		workerList.add("52.90.1.183:8002");
		workerList.add("54.87.135.165:8003");
		workerList.add("54.160.118.31:8004");

		Config config = new Config();

		// IP:port for /workerstatus to be sent

		 config.put("master", "54.92.232.109:8000");
//		//

		config.put("workerList", generateWorkerList(workerList));

		// IP:port for /workerstatus to be sent
//		config.put("master", "127.0.0.1:8000");
		// Build the local worker

		if (args.length >= 1) {
			config.put("workerIndex", args[0]);
		} else
			config.put("workerIndex", "0");

		WorkerServer.createWorker(config);
		@SuppressWarnings("unused")
		DBWrapper db = DBWrapper.getInstance("crawl" + config.get("workerIndex"));

		urlQueue = new Queue("store/urlQueue" + config.get("workerIndex"), "urlQueue", 1);

		String directory = "upload" + config.get("workerIndex") + "/";
		File dir = new File("store/" + directory);
		// create directory if not existed
		if (!dir.exists()) {
			dir.mkdirs();
		}

		S3FileWriter.setDocFileWriter(dir);
		S3FileWriter.setUrlFileWriter(dir);

	}

	public static String generateWorkerList(List<String> workers) {
		String res = "[";
		for (int i = 0; i < workers.size() - 1; i++) {
			res += workers.get(i) + ",";
		}
		res += workers.get(workers.size() - 1) + "]";

		return res;
	}

}
