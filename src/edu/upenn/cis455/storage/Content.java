package edu.upenn.cis455.storage;

import com.sleepycat.persist.model.Entity;
import com.sleepycat.persist.model.PrimaryKey;
/**
 * Stores hashcode of crawled content to check for content seen
 * @author kevin ye
 *
 */
@Entity
public class Content {
	
	@PrimaryKey
	private String content;
	
	private String url;
	
	public Content(){}
	
	public Content(String content, String url){
		this.content = content;
		this.url = url;
	}
	
	public String getContent(){
		return this.content;
	}
	
	public String getUrl() {
		return this.url;
	}

}
