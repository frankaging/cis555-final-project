package edu.upenn.cis455.storage;

import java.io.File;
import java.util.HashSet;
import java.util.Iterator;

import com.sleepycat.je.DatabaseConfig;
import com.sleepycat.je.Environment;
import com.sleepycat.je.EnvironmentConfig;
import com.sleepycat.persist.EntityCursor;
import com.sleepycat.persist.EntityStore;
import com.sleepycat.persist.PrimaryIndex;
import com.sleepycat.persist.StoreConfig;

import edu.upenn.cis.stormlite.bolt.DocumentParserBolt;
import edu.upenn.cis.stormlite.bolt.RobotsTxtInfo;
import edu.upenn.cis.stormlite.bolt.URLFilterBolt;
import edu.upenn.cis.stormlite.bolt.URLInfo;

/**
 * This is the database wrapper that interacts with Berkeley DB
 * 
 * @author cis555
 *
 */
public class DBWrapper {

	private static String envDirectory = null;

	private static DBWrapper instance = null;
	private static Environment myEnv;
	private static EntityStore store;
	private static PrimaryIndex<String, VisitedUrl> visitedUrl;
	private static PrimaryIndex<String, Docs> docsIndex;
	private static PrimaryIndex<String, RobotsTxtInfo> robotsIndex;
	private static PrimaryIndex<String, XMLDoc> xmlDocIndex;
	private static PrimaryIndex<String, Content> contentIndex;
	static int cachedSize = 500;
	static int docCounter = 0;

	public static DBWrapper getInstance(String directory) {
		if (instance == null) {
			instance = new DBWrapper(directory);
		}

		return instance;
	}

	/* TODO: write object store wrapper for BerkeleyDB */
	public DBWrapper(String directory) {
		
		File dir = new File("store/" + directory);
		EnvironmentConfig envConfig = new EnvironmentConfig();
		envConfig.setAllowCreate(true);
		envConfig.setTransactional(false);
		//envConfig.setSharedCache(true);
		//envConfig.setTxnNoSync(true);
		
		StoreConfig storeConfig = new StoreConfig();
		storeConfig.setAllowCreate(true);
		storeConfig.setTransactional(false);
		storeConfig.setDeferredWrite(true);
		// create directory if not existed
		if (!dir.exists()) {
			dir.mkdirs();
		}

		myEnv = new Environment(dir, envConfig);

		store = new EntityStore(myEnv, "EntityStore", storeConfig);

		DatabaseConfig dbConfig = new DatabaseConfig();
		dbConfig.setAllowCreate(true);

		docsIndex = store.getPrimaryIndex(String.class, Docs.class);
		robotsIndex = store.getPrimaryIndex(String.class, RobotsTxtInfo.class);
		xmlDocIndex = store.getPrimaryIndex(String.class, XMLDoc.class);
		contentIndex = store.getPrimaryIndex(String.class, Content.class);

		visitedUrl = store.getPrimaryIndex(String.class, VisitedUrl.class);

	}

	public static synchronized void syncWithLock() {
		docCounter++;
		if (docCounter >= cachedSize) {
			//System.out.println("Lock Number: " + docCounter);
			docCounter = 0;
			myEnv.sync();
			store.sync();
			myEnv.cleanLog();
		}
			
	}

	public static void updateXmlContent(String key, String content){
		XMLDoc xml = xmlDocIndex.get(key);
		xml.setContent(content);
		xmlDocIndex.put(xml);
		syncWithLock();
	}

	public static void putXmlDOM(XMLDoc xml) {
		xmlDocIndex.put(xml);
	}

	public static boolean containXml(String key) {
		return xmlDocIndex.contains(key);
	}

	public static XMLDoc getXml(String key) {
		return xmlDocIndex.get(key);
	}

	public static EntityCursor<String> getAllXml() {
		return xmlDocIndex.keys();
	}

	public static long getNextCrawlTime(String host){
		return robotsIndex.get(host).getTime();
	}

	public static boolean containRobot(String host){
		return robotsIndex.contains(host);
	}

	public static void setCrawlTime(String host, long date){
		RobotsTxtInfo robot = robotsIndex.get(host);
		robot.setTime(date);

		robotsIndex.put(robot);
	}

	public static RobotsTxtInfo getRobot(String host) {
		return robotsIndex.get(host);
	}

	/*
	 * put robot.txt into database
	 */
	public static void storeRobot(RobotsTxtInfo robot) {
		robotsIndex.put(robot);
	}

	public static boolean isVisit(URLInfo url) {
		return visitedUrl.contains(url.getUrl());
	}

	/*
	 * put visited url to the database on this session
	 */
	public static void storeUrl(VisitedUrl v) {
		visitedUrl.put(v);
	}

	public static void deleteDocs(String key) {
		docsIndex.delete(key);
	}

	public static void putDocs(Docs doc) {
		docsIndex.put(doc);
	}

	public static void addChildDoc(String key, String url){

		VisitedUrl visit = visitedUrl.get(key);
		visit.addChild(url);
	}

	public static void updateDocContent(String key, String content){
		Docs doc = docsIndex.get(key);
		doc.setContent(content);
		docsIndex.put(doc);
		syncWithLock();
	}

	public static boolean containDocs(String key) {
		return docsIndex.contains(key);
	}

	public static Docs getDocs(String url) {
		return docsIndex.get(url);
	}

	public static long getDocNum() {
		return docsIndex.count();
	}

	public static void close() {
	}

	// add one hit to a seen url
	public static void addOneHits(String key) {
		Docs doc = docsIndex.get(key);
		doc.addOneHit();
		docsIndex.put(doc);
	}

	public static void addContent(Content content){
		contentIndex.put(content);
	}

	public static boolean isContentSeen(String content){
		return contentIndex.contains(content);
	}

	public static String getContentUrl(String content){
		return contentIndex.get(content).getUrl();
	}

	
	 public static HashSet<String> getAllURLs(){
         HashSet<String> urls = new HashSet<String>();
         EntityCursor<Docs> cursor = docsIndex.entities();
         try{
                 Iterator<Docs> iter = cursor.iterator();
                 while(iter.hasNext()){
                         urls.add(iter.next().getUrl());
                 }
         }finally{
                 cursor.close();
         }
         return urls;
 }

	//	static class dateComparator implements Comparator {
	//
	//		@Override
	//		public int compare(Channel c1, Channel c2) {
	//			Date d1 = c1.getDate();
	//			Date d2 = c2.getDate();
	//		}
	//		
	//	}
}
