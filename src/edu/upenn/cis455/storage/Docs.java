package edu.upenn.cis455.storage;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import com.sleepycat.persist.model.Entity;
import com.sleepycat.persist.model.PrimaryKey;
/**
 * This class contains the content and info of url that has been crawled.
 * @author cis555
 *
 */
@Entity
public class Docs implements WebDocs {
	
	@PrimaryKey
	private String url;
	
	private String content;
	private Date lastCrawl;
	private String contentType;
	private int hits;
	
	public Docs () {
		
	}
	
	public Docs (String url, String contentType, Date date){
		this.url = url;
		this.contentType = contentType;
		this.lastCrawl = date;
		this.hits = 0;
	}
	
	public Docs (String url, String content, Date date, String contentType){
		this.url = url;
		this.content = content;
		this.lastCrawl = date;
		this.contentType = contentType;
	}
	
	public void addOneHit() {
		this.hits++;
	}
	
	public int getHits() {
		return this.hits;
	}
	
	public String getUrl() {
		return this.url;
	}
	
	public String getContent() {
		return this.content;
	}
	
	public Date getLastAccess() {
		return this.lastCrawl;
	}
	
	public String getContentType() {
		return this.contentType;
	}
	
	public void setContent(String content){
		this.content = content;
	}
	
	public void setDate(Date date){
		this.lastCrawl = date;
	}

}
