package edu.upenn.cis455.storage;

import java.util.Date;

import com.sleepycat.persist.model.Entity;
import com.sleepycat.persist.model.PrimaryKey;

@Entity
public class Domain {

	@PrimaryKey
	private String name;

	private Date date;
	
	public Domain() {
		
	}

	public Domain(String name, Date date) {
		this.name = name;
		this.date = date;
	}

	public String getName() {
		return this.name;
	}

	public Date getDate() {
		return this.date;
	}

	public void setDate(Date date) {
		this.date = date;
	}
}
