package edu.upenn.cis455.storage;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileReader;
import java.io.IOException;
import java.util.HashSet;
import java.util.Set;

import com.sleepycat.je.Environment;
import com.sleepycat.je.EnvironmentConfig;
import com.sleepycat.persist.EntityStore;
import com.sleepycat.persist.PrimaryIndex;
import com.sleepycat.persist.StoreConfig;


public class IndexDB {
	
	private static String DBroot = null;
	private static File DBDirectory;
	private static Environment myEnv;
	private static EntityStore store;	
	private static PrimaryIndex<String,TermInfo> TermIndex;
	private static PrimaryIndex<String,IndexURL> urlIndex;
	private static HashSet<String> blackset = new HashSet<String>();
	
	public IndexDB(){
		
	}
	
	public static void SetUptoCheck(String root){
		if(root.equals(DBroot) && myEnv != null && myEnv.isValid()){
			//already exist
			return;
		}
		DBroot = root;
		File dir = new File(DBroot);
		if(dir.exists()){
			//System.out.println("Directory exists!");
			DBDirectory = dir;
		    EnvironmentConfig envConf = new EnvironmentConfig();
		    StoreConfig storeConf = new StoreConfig();
		    envConf.setAllowCreate(true);
		    envConf.setTransactional(false);
		    storeConf.setTransactional(false);	  
		    storeConf.setAllowCreate(true);
		    myEnv = new Environment(DBDirectory, envConf);
		    store = new EntityStore(myEnv,"EntityStore",storeConf);
		    TermIndex = store.getPrimaryIndex(String.class, TermInfo.class);
		    urlIndex = store.getPrimaryIndex(String.class, IndexURL.class);
		}else{
			System.out.println("There is no files, cannot check");
			System.exit(1);
		}
		
	}
	
	public static void SetUp(String root){
		if(root.equals(DBroot) && myEnv != null && myEnv.isValid()){
			//already exist
			return;
		}
		DBroot = root;
		File dir = new File(DBroot);
		if(dir.exists()){
			deleteDir(dir);
			System.out.println("Directory already exists,delete it");
		}
		dir.mkdirs();
		DBDirectory = dir;
		System.out.println("Create a directory for database");

	    EnvironmentConfig envConf = new EnvironmentConfig();
	    StoreConfig storeConf = new StoreConfig();
	    envConf.setAllowCreate(true);
	    envConf.setTransactional(false);
	    storeConf.setTransactional(false);	  
	    storeConf.setAllowCreate(true);
	    myEnv = new Environment(DBDirectory, envConf);
	    store = new EntityStore(myEnv,"EntityStore",storeConf);
	    TermIndex = store.getPrimaryIndex(String.class, TermInfo.class);
	    urlIndex = store.getPrimaryIndex(String.class, IndexURL.class);
	}

    
    public static void ShutDown(){
    	if(store != null)
    		store.close();
    	if(myEnv != null)
    		myEnv.close();
    }

	public synchronized static void putTerm(TermInfo term){
		TermIndex.put(term);
		store.sync();
		myEnv.sync();
	}
	public  static  TermInfo getTerm(String key){
		return TermIndex.get(key);
	}

	public static boolean containsTerm(String key){
		return TermIndex.contains(key);
	}

//	public static void addFile(String key, String filename){
//		TermInfo term = TermIndex.get(key);
//		term.addfile(filename);
//		putTerm(term);
//	}
	
	public static void addTF(String word, String filename, double tf){
		if(TermIndex.sortedMap().containsKey(word)){
			TermInfo term = TermIndex.get(word);
			if(term == null || filename == null){
				return;
			}
			term.addTF(filename, tf);
			putTerm(term);
		}else{
			TermInfo term = new TermInfo(word);
			term.addTF(filename, tf);
			putTerm(term);
		}
	}
	
	public static double getTF(String word, String filename){
		if(TermIndex.sortedMap().keySet().contains(word)){
			return TermIndex.get(word).getTF(filename);
		}else{
			return 0;
		}
	}
	
	public static Set<String> getFiles(String word){
		TermInfo term = getTerm(word);
		if(term!=null){
			return term.getFiles();
		}else{
			return new HashSet<String>();
		}
	}
	
	public static void addTFIDF(String word, String filename, double tfidf){
		TermInfo term = TermIndex.get(word);
		term.addTFIDF(filename, tfidf);
		putTerm(term);
	}
	
	
	public static double getNDF(String word){
		if(TermIndex.sortedMap().keySet().contains(word)){
			return TermIndex.get(word).getNDF();
		}else{
			return 0;
		}
	}
	
	public static double getTFIDF(String word, String filename){
		if(TermIndex.contains(word)){
		    return TermIndex.get(word).getTFIDF(filename);
		}else{
			return 0;
		}
	}
	
	public static void CaculateAllIDF(double maxTF) throws IOException{
		double a = 0.5;
		File outputdir = new File("output");
		File outputDir = new File(outputdir, "outputs");
		
		File tfResultsPrint = new File(outputDir, "TFResultPrint.txt");
		BufferedReader tfreader = new BufferedReader(new FileReader(tfResultsPrint));
		String line = tfreader.readLine();
		while(line!=null) {
			String[] splitted = line.split("\\s+");
			String word = splitted[0];
			String filename = splitted[1];
			double tf = Double.parseDouble(splitted[2]);
			TermInfo term = TermIndex.get(word);
			if(!blackset.contains(word)){
				double dfnum = term.getNDF();
				if(dfnum == 1){
					blackset.add(word);
					term.addTFIDF("##@@##", 0);
					putTerm(term);
					
				}else{
					double normalizedtf = a+(1-a)*(tf/maxTF);
					double tfidf = computeTfIdf(normalizedtf,dfnum);
					term.addTFIDF(filename, tfidf);
					putTerm(term);
				}
			}
			line = tfreader.readLine();
		}
		tfreader.close();
	}
	
	//This method is for me to test
	public static void PrintTFIDF(){
		for (String word : TermIndex.sortedMap().keySet()) {
			TermInfo term = TermIndex.get(word);
			for(String filename:term.getFiles()){
				System.out.println(word+" "+filename+" "+term.getTFIDF(filename));
			}
		}
	}
	
	//This method is for test
	public static void PrintIndex(){
		int count = 0;
		for (String num : urlIndex.sortedMap().keySet()) {
			count++;
			IndexURL url = urlIndex.get(num);
			if(num.equals("00430de6-5774-410e-83d6-f804424c121a")){
				System.out.println(url.getURL());
				break;
			}
//			if(num.equals("fa1c85fe-cdd5-43c1-bc13-6236ff749140")){
//				System.out.println(url.getURL());
//				break;
//			}
//			if(num.equals("bcd8b287-a958-4fc0-a8d0-9a83117e6e28")){
//				System.out.println("haha");
//				break;
//			}
			System.out.println("count " + count + "index: "+num + " " + url.getURL());
		}
	}
	
	//This method is for test
	public static void PrintNDF(){
		for (String word : TermIndex.sortedMap().keySet()){
			TermInfo term = TermIndex.get(word);
			System.out.println(word + " " + term.getNDF());	
		}
	}
	
	//This method is for test
	public static void PrintTF(){
		for (String word : TermIndex.sortedMap().keySet()){
			TermInfo term = TermIndex.get(word);
			for(String filename : term.getFiles()){
				System.out.println(word + " " + filename + " " + term.getTF(filename));
			}
		}
	}
	
	//IndexURL
	public synchronized static void putIndexURL(IndexURL url){
		urlIndex.put(url);
		myEnv.sync();
		store.sync();
		//System.out.println(urlIndex.sortedMap().keySet().size());
	}
	
	public static IndexURL getIndexURL(String index){
		return urlIndex.get(index);
	}
	
	public static int getIndexURLSize(String index){
		return urlIndex.get(index).getSize();
	}
	
	public static void deleteDir(File dir) {
		for(File f: dir.listFiles()) {
			if(f.isDirectory())
				deleteDir(f);
			else 
				f.delete();
		}
	}
	
	public static double computeTfIdf(double tf, double df){
		return tf * Math.log(df);
		
	}
	
}
