package edu.upenn.cis455.storage;

import java.io.File;

import com.sleepycat.je.Environment;
import com.sleepycat.je.EnvironmentConfig;
import com.sleepycat.persist.EntityStore;
import com.sleepycat.persist.PrimaryIndex;
import com.sleepycat.persist.StoreConfig;

public class IndexDBHelper {

	private static String DBroot = null;
	private static File DBDirectory;
	private static Environment myEnv;
	private static EntityStore store;	
	private static PrimaryIndex<String,IndexURL> urlIndex;

	public IndexDBHelper(){

	}

	public void SetUptoCheck(String root){
		if(root.equals(DBroot) && myEnv != null && myEnv.isValid()){
			//already exist
			return;
		}
		DBroot = root;
		File dir = new File(DBroot);
		if(dir.exists()){
			System.out.println("Directory exists!");
			DBDirectory = dir;
			EnvironmentConfig envConf = new EnvironmentConfig();
			StoreConfig storeConf = new StoreConfig();
			envConf.setAllowCreate(true);
			envConf.setTransactional(false);
			storeConf.setTransactional(false);	  
			storeConf.setAllowCreate(true);
			myEnv = new Environment(DBDirectory, envConf);
			store = new EntityStore(myEnv,"EntityStore",storeConf);
			urlIndex = store.getPrimaryIndex(String.class, IndexURL.class);
		}else{
			System.out.println("There is no files, cannot check");
			System.exit(1);
		}

	}


	//This method is for test
	public void PrintIndex(){
		System.out.println(urlIndex.sortedMap().keySet().size());
		for (String num : urlIndex.sortedMap().keySet()) {
			IndexURL url = urlIndex.get(num);
			System.out.println("index: "+num + " " + url.getURL());
		}
	}

	//IndexURL
	public synchronized void putIndexURL(IndexURL url){
		urlIndex.put(url);
		myEnv.sync();
		store.sync();
		//System.out.println(urlIndex.sortedMap().keySet().size());
	}

	public IndexURL getIndexURL(String index){
//	    if(urlIndex.sortedMap().containsKey(index)){
//	    	System.out.println("haha");
//	    }
		return urlIndex.get(index);
	}

	public Integer getIndexURLSize(String index){
		if(urlIndex.get(index)==null){
			return null;
		}
		return urlIndex.get(index).getSize();
	}
}

