package edu.upenn.cis455.storage;

import com.sleepycat.persist.model.Entity;
import com.sleepycat.persist.model.PrimaryKey;

@Entity
public class IndexURL {
	@PrimaryKey
	private String index;
	private String url;
	private int contentsize;
	
	

	public IndexURL(){

	}
	
	public IndexURL(String index, String url){
		this.index = index;
		this.url = url;
	}
	
	public void setURL(String u){
		url = u;
	}
	
	public String getURL(){
		return url;
	}
	public void setIndex(String in){
		index = in;
	}

	public String getIndex(){
		return index;
	}
	
	public void setSize(int size){
		this.contentsize = size;
	}
	
	public int getSize(){
		return contentsize;
	}
}
