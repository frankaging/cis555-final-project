package edu.upenn.cis455.storage;

import java.io.File;

import com.sleepycat.je.Environment;
import com.sleepycat.je.EnvironmentConfig;
import com.sleepycat.persist.EntityStore;
import com.sleepycat.persist.PrimaryIndex;
import com.sleepycat.persist.StoreConfig;

public class PageRankDB {

	private static String envDirectory = "";
	private static Environment myEnv;
	private static EntityStore store;
	private static PrimaryIndex<String, UrlRank> rankIndex;

	public PageRankDB() {
		// TODO Auto-generated constructor stub
	}

	public static void setUp(String directory) {
		if(envDirectory.equals(directory) && myEnv != null && myEnv.isValid())
			return; //already exist

		envDirectory = directory;
		File file = new File(envDirectory);
		if (file.exists())
			file.delete();
		file.mkdirs();

		EnvironmentConfig envConfig = new EnvironmentConfig();
		StoreConfig storeConfig = new StoreConfig();
		envConfig.setAllowCreate(true);	
		envConfig.setTransactional(true);
		storeConfig.setAllowCreate(true);
		storeConfig.setTransactional(true);
		myEnv = new Environment(file, envConfig);
		store = new EntityStore(myEnv, "Entity Store", storeConfig);
		rankIndex = store.getPrimaryIndex(String.class, UrlRank.class);
	}
	
	public synchronized static void putRank(String url, double rank, int numOfChildren) {
		UrlRank urlRank = new UrlRank();
		urlRank.setUrl(url);
		urlRank.setRank(rank);
		urlRank.setNumOfChildren(numOfChildren);
		rankIndex.put(urlRank);
		syncDB();
//		System.out.println("put: " + url + " " + rank + " " + numOfChildren);
	}
	
	public static double getRank(String url) {
		if (!containsRank(url))
			return 0.2;
		return rankIndex.get(url).getRank();
	}
	
	public static int getNumOfChildren(String url) {
		return rankIndex.get(url).getNumOfChildren();
	}
	
	public static boolean containsRank(String url) {
		return rankIndex.contains(url);
	}
	
	public static void addSinkRank(String sink, String parent) {
		double rankAssigned;
		if (!containsRank(sink)) {
			double parentRank = getRank(parent);
			rankAssigned = parentRank / (double)getNumOfChildren(parent);
			putRank(sink, rankAssigned, 0);
		}
		else {
			double parentRank = getRank(parent);
			rankAssigned = parentRank / (double)getNumOfChildren(parent);
			putRank(sink, getRank(sink) + rankAssigned, 0);
		}
		// TODO:
//		System.out.println("Assigned to sink:" + sink + ", " + rankAssigned);
	}
	
	public void closeDB() {
		if (store != null) 
			store.close();
		if (myEnv != null) 
			myEnv.close();
	}
	
	public static void syncDB() {
		if (store != null)
			store.sync();
		if (myEnv != null)
			myEnv.sync();
	}
	
	public static void main(String[] args) {
//		PageRankDB.setUp("rankDB_X");
//		System.out.println(rankIndex.map().keySet().size());
	}

}
