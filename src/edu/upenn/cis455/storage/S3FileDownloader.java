package edu.upenn.cis455.storage;

import java.io.File;
import java.io.IOException;
import java.util.HashMap;
import java.util.LinkedList;

import com.amazonaws.AmazonClientException;
import com.amazonaws.ClientConfiguration;
import com.amazonaws.auth.AWSCredentials;
import com.amazonaws.auth.profile.ProfileCredentialsProvider;
import com.amazonaws.regions.Region;
import com.amazonaws.regions.Regions;
import com.amazonaws.services.s3.AmazonS3;
import com.amazonaws.services.s3.AmazonS3Client;
import com.amazonaws.services.s3.model.GetObjectRequest;
import com.amazonaws.services.s3.model.ListObjectsRequest;
import com.amazonaws.services.s3.model.ObjectListing;
import com.amazonaws.services.s3.model.ObjectMetadata;
import com.amazonaws.services.s3.model.S3ObjectSummary;
import com.amazonaws.services.s3.model.S3Object;

/**
 * Download the file into local database for using.
 * 
 * @author Zhengxuan Wu
 *
 */

// TODO: add a bucketName to adapt to other download request

public class S3FileDownloader {

	protected static HashMap<S3Object, String> objectWithFileName = new HashMap<>();
	private LinkedList<S3Object> s3FilesList = new LinkedList<S3Object>();
	boolean keepRunning = true;
	private AmazonS3 s3;

	public void DownloadFromS3(String localDir) {

		ClientConfiguration clientConfig = new ClientConfiguration();
		clientConfig.setConnectionTimeout(50000);
		clientConfig.setMaxConnections(50000);
		clientConfig.setSocketTimeout(50000);
		clientConfig.setMaxErrorRetry(10);

		// create if not exist, a dir
		// to save the file
		File dir = new File(localDir);
		// create directory if not existed
		if (!dir.exists()) {
			dir.mkdirs();
		}
	}

	public S3FileDownloader(int index) {
		// TODO Auto-generated constructor stub
		this.workerIndex = index;
	}

	/**
	 * Pulls files from S3 that contain documents that need to be indexed.
	 */

	private void extractObject() {

		int folderIndex = 0;
		int fileIndex = 50;
		System.out.println("===========================================");
		System.out.println("Entering BucketNumber: #" + Integer.toString(workerIndex));
		System.out.println("===========================================\n");

		String bucketName = "crawlerhistory" + Integer.toString(workerIndex);
		ObjectListing objectListing = s3
				.listObjects(new ListObjectsRequest().withBucketName(bucketName).withPrefix("DocS3batch:"));
		for (S3ObjectSummary objectSummary : objectListing.getObjectSummaries()) {
			if (folderIndex == 100) {
				keepRunning = false;
				break;
			}
			if (!keepRunning)
				break;
			String key = objectSummary.getKey();
			if (!key.startsWith("DocS3batch")) {
				System.out.println("key " + key);
				continue;
			}

			// mkdir if not exist
			String mapFolderName = "./store" + Integer.toString(folderIndex);
			File mapFolder = new File(mapFolderName);
			if (!mapFolder.exists())
				mapFolder.mkdirs();

			System.out.println("===========================================");
			System.out.println("Downloading new file to S3-zhengxuan, File: #" + Integer.toString(folderIndex));
			System.out.println("===========================================\n");

			System.out.println("Getting object: " + key + " of size: " + objectSummary.getSize());
			// adding to the linkedList
			S3Object object = s3.getObject(new GetObjectRequest(bucketName, key));
			objectWithFileName.put(object, key);
			s3FilesList.add(object);
			String tempFileName = mapFolderName + "/crawlerdata" + Integer.toString(fileIndex) + ".txt";
			File localFile = new File(tempFileName);
			ObjectMetadata objectMeta = s3.getObject(new GetObjectRequest(bucketName, key), localFile);
			fileIndex++;
			folderIndex++;
			try {
				object.close();
			} catch (IOException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}

		}

		keepRunning = false;

	}

	public static void main(String[] args) {
		S3FileDownloader d = new S3FileDownloader();
		d.DownloadFromS3("./store/");
	}

}