package edu.upenn.cis455.storage;

import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileWriter;
import java.io.IOException;
import java.io.RandomAccessFile;
import java.nio.channels.FileChannel;
import java.nio.channels.FileLock;
import java.nio.channels.OverlappingFileLockException;
import java.nio.file.DirectoryStream;
import java.nio.file.FileSystems;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.util.ArrayList;
import java.util.List;

import com.amazonaws.AmazonClientException;
import com.amazonaws.AmazonServiceException;
import com.amazonaws.auth.AWSCredentials;
import com.amazonaws.auth.DefaultAWSCredentialsProviderChain;
import com.amazonaws.auth.profile.ProfileCredentialsProvider;
import com.amazonaws.regions.Region;
import com.amazonaws.regions.Regions;
import com.amazonaws.services.s3.AmazonS3;
import com.amazonaws.services.s3.AmazonS3Client;
import com.amazonaws.services.s3.model.PutObjectRequest;
import com.amazonaws.services.s3.transfer.MultipleFileUpload;
import com.amazonaws.services.s3.transfer.TransferManager;
import com.amazonaws.services.s3.transfer.Upload;


/**
 * This S3 Writer assume the credential is set up
 * locally. Refer the Amazon AWS for setting access
 * for your computer before you initiate this program
 * @author Zhengxuan Wu
 */

public class S3FileWriter {

	/**
	 * call this static method to write a file to S3
	 * @param file - file to be written
	 * @throws InterruptedException 
	 * @throws AmazonClientException 
	 * @throws AmazonServiceException 
	 * @throws IOException 
	 */


	static File docFile;
	static File urlFile;
	static FileWriter docWriter;
	static FileWriter urlWriter;
	
	
	public static void setDocFileWriter(File directory) {
		
		System.out.println("===========================================");
		System.out.println("Creating New File!!!!!!");
		System.out.println("===========================================\n");
		
		
		try {
			docFile = new File(directory +"/DocS3batch:" + System.currentTimeMillis() + ".txt");
			docWriter = new FileWriter(docFile);
		} catch (IOException e) {
			e.printStackTrace();
		}
	}
	

	public static void setUrlFileWriter(File directory) {
		try {
			urlFile = new File(directory +"/UrlS3batch:" + System.currentTimeMillis() + ".txt");
			urlWriter = new FileWriter(urlFile);
		} catch (IOException e) {
			e.printStackTrace();
		}
	}
	
	public static synchronized void writeToDocFile(String line) {
		try {
			docWriter.append(line);
			docWriter.flush();
		} catch (IOException e) {
			e.printStackTrace();
		}
	}
	
	public static synchronized void writeToUrlFile(String line) {
		try {
			urlWriter.append(line);
			urlWriter.flush();
		} catch (IOException e) {
			e.printStackTrace();
		}
	}
	
	public static String prepareFileLineDoc(String url, String doc) {
		String s = url +  "\t" + doc + "###$$$@@@$$$###";
		return s;
	}
	
	public static String prepareFileLineUrlList(String url, List<String> list) {
		String s = url +  "\t" + list.toString() + "###$$$@@@$$$###";
		return s;
	}


	public static void WriteToS3(File directory, String bucketName) throws AmazonServiceException, AmazonClientException, InterruptedException, IOException {

		
		
		File urlFileToWrite;
		File docFileToWrite;
		
		synchronized(docWriter) {
			try {
				docWriter.close();
			} catch (IOException e1) {
				// TODO Auto-generated catch block
				e1.printStackTrace();
			}
			docFileToWrite = new File(docFile.getAbsolutePath());
			setDocFileWriter(directory);
		} 
		
		synchronized(urlWriter) {
			try {
				urlWriter.close();
			} catch (IOException e1) {
				// TODO Auto-generated catch block
				e1.printStackTrace();
			}
			urlFileToWrite = new File(urlFile.getAbsolutePath());
			setUrlFileWriter(directory);
		}
		
		
		
		
		//Set up S3 connection
		AWSCredentials credentials = null;
		try {
			credentials = new ProfileCredentialsProvider("default").getCredentials();
		} catch (Exception e) {
			throw new AmazonClientException(
					"Cannot load the credentials from the credential profiles file. " +
							"Please make sure that your credentials file is at the correct " +
							"location (/home/cis455/.aws/credentials), and is in valid format.",
							e);
		}

		AmazonS3 s3 = new AmazonS3Client(credentials);
		Region usStandard = Region.getRegion(Regions.US_EAST_1);
		s3.setRegion(usStandard);

		//Choose the bucket
		String buckName = bucketName;
		String docfileName = docFileToWrite.getName();
		String urlfileName = urlFileToWrite.getName();

			System.out.println("===========================================");
			System.out.println("Getting Started with Amazon S3");
			System.out.println("===========================================\n");

			System.out.println("Uploading a new object to S3 from a file\n");
			// if it is a single file
			s3.putObject(new PutObjectRequest(buckName, docfileName, docFileToWrite));
			s3.putObject(new PutObjectRequest(buckName, urlfileName, urlFileToWrite));
	}

}
