package edu.upenn.cis455.storage;

import java.io.ByteArrayInputStream;
import java.io.ByteArrayOutputStream;
import java.io.IOException;
import java.io.ObjectInputStream;
import java.io.ObjectOutputStream;

public class Serializer {

	public byte[] serialize(Object object) {
		ByteArrayOutputStream baos = new ByteArrayOutputStream();
		ObjectOutputStream output;
		try {
			output = new ObjectOutputStream(baos);
			output.writeObject(object);
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		
		return baos.toByteArray();
	}

	/*
	 * conver byte array to object
	 */
	public Object deserialize(byte[] bytes) throws ClassNotFoundException, IOException {
		ByteArrayInputStream baos = new ByteArrayInputStream(bytes);
		ObjectInputStream output = null;
		try {
			output = new ObjectInputStream(baos);
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		return output.readObject();
	}
}
