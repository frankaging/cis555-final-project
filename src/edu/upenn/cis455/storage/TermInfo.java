package edu.upenn.cis455.storage;

import java.util.HashMap;
import java.util.Set;

import com.sleepycat.persist.model.Entity;
import com.sleepycat.persist.model.PrimaryKey;

@Entity
public class TermInfo {
	
	@PrimaryKey
	private String key;
	//total num/num contain this word
	private double ndf;
	//how many times this word appear in certain file
	private HashMap<String,Double> filetonum = new HashMap<String,Double>();
	//TFIDF
	private HashMap<String,Double> fileTFIDF = new HashMap<String,Double>();
	
	public TermInfo(){
		
	}
	
	public TermInfo(String key){
		this.key = key;
	}
	
	public String getWord(){
		return key;
	}
	
	public void setNDF(double ndf){
		this.ndf = ndf;
	}
	
	public double getNDF(){
		return ndf;
	}

	public Set<String> getFiles(){
		if(!filetonum.keySet().isEmpty()){
			return filetonum.keySet();
		}else{
			return fileTFIDF.keySet();
		}
		
	}
	
//	public void addfile(String filename){
//		files.add(filename);
//	}
	
	public void addTF(String filename, double freq){
		filetonum.put(filename, freq);
	}
	
	public double getTF(String filename){
		return filetonum.get(filename);
	}
	
	public void addTFIDF(String filename, double tfidf){
		fileTFIDF.put(filename, tfidf);
	}
	
	public double getTFIDF(String filename){
		if(fileTFIDF.containsKey(filename)){
		    return fileTFIDF.get(filename);
		}else{
			return 0;
		}
	}
}
