package edu.upenn.cis455.storage;

import java.util.LinkedList;
import java.util.List;

import com.sleepycat.persist.model.Entity;
import com.sleepycat.persist.model.PrimaryKey;
/**
 * The user has a username and password that gets stored into database
 * @author cis555
 *
 */
@Entity
public class User {
	
	@PrimaryKey
	private String name;
	
	private String password;
	private List<String> channels;
	
	public User() {
		this.channels = new LinkedList<String>();
	}
	
	public User(String name, String password){
		this.name = name;
		this.password = password;
		this.channels = new LinkedList<String>();
	}
	
	public String getName() {
		return this.name;
	}
	
	public String getPassword() {
		return this.password;
	}
	
	public void addChannel(String s){
		this.channels.add(s);
	}
	
	public List<String> getChannel() {
		return this.channels;
	}
	
	public boolean containChannel(String name){
		return this.channels.contains(name);
	}
	
	public void removeChannel(String name){
		this.channels.remove(name);
	}
}
