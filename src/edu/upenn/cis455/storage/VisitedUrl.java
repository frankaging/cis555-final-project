package edu.upenn.cis455.storage;

import java.util.HashSet;
import java.util.Set;

import com.sleepycat.persist.model.Entity;
import com.sleepycat.persist.model.PrimaryKey;

@Entity
public class VisitedUrl {
	
	@PrimaryKey
	private String url;
	
	private Set<String> children  = new HashSet<>();
	
	public VisitedUrl() {
		
	}
	
	public VisitedUrl(String url){
		this.url = url;
	}
	
	public String getUrl() {
		return this.url;
	}
	
	public Set<String> getChild() {
		return this.children;
	}
	
	public void addChild(String child) {
		this.children.add(child);
	}

}
