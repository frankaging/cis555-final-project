package edu.upenn.cis455.storage;

import java.util.Date;

public interface WebDocs {

	public String getUrl();
	
	public String getContent();
	
	public Date getLastAccess();
	
	public String getContentType();
	
	public void setContent(String content);
	
	public void setDate(Date date);
}
