package edu.upenn.cis455.storage;

import java.util.Date;

import com.sleepycat.persist.model.Entity;
import com.sleepycat.persist.model.PrimaryKey;

@Entity
public class XMLDoc implements WebDocs {

	@PrimaryKey
	private String url;
	
	private Date lastAccessed;
	private String content;
	private String contentType;
	
	public XMLDoc() {
		
	}
	
	public XMLDoc(String url, Date date, String contentType){
		this.url = url;
		this.lastAccessed = date;
		this.contentType = contentType;
	}
	
	public XMLDoc(String url, String content, Date date, String contentType){
		this.url = url;
		this.lastAccessed = date;
		this.content = content;
		this.contentType = contentType;
	}
	
	public String getUrl(){
		return this.url;
	
	}
	
	public String getContent() {
		return this.content;
	}
	
	public String getContentType() {
		return this.contentType;
	}
	
	public void setContent(String content) {
		this.content = content;
	}

	@Override
	public Date getLastAccess() {
		// TODO Auto-generated method stub
		return this.lastAccessed;
	}

	@Override
	public void setDate(Date date) {
		this.lastAccessed = date;
	}
}
