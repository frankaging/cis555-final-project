package search.edu.upenn.cis.engine;

import java.io.FileNotFoundException;
import java.io.IOException;
import java.util.ArrayList;
import java.util.HashMap;

interface SearchEngine {
	/**
	 * Loading TFIDF Results from .txt file input to 3D Map
	 * @throws FileNotFoundException 
	 * @throws IOException 
	 */
	public HashMap<String, HashMap<String, Double> > loadTFIDF (String directory) throws FileNotFoundException, IOException;
	/**
	 * Loading Pagerank results from .txt input to 2D Map
	 * @param directory
	 * @return
	 */
	public HashMap<String, Float> loadPageRank (String directory);
	/**
	 * Getting search results
	 * @return
	 */
	public ArrayList<String> getUrlSearchResults ();
	/**
	 * Getting search results with contents
	 * @return
	 */
	public HashMap<String, String> getContentSearchResults (ArrayList<String> urlResults);
}
