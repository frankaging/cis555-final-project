package search.edu.upenn.cis.engine;

import java.io.BufferedReader;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.io.InputStreamReader;
import java.util.ArrayList;
import java.util.HashMap;

public class SearchEngineImpl implements SearchEngine{

	@Override
	public HashMap<String, HashMap<String, Double>> loadTFIDF(String directory) throws IOException {
		// reading in from the directory
		HashMap<String, HashMap<String, Double>> results = new HashMap<String, HashMap<String, Double>>();

		// Open the file
		FileInputStream fstream = new FileInputStream(directory);
		BufferedReader br = new BufferedReader(new InputStreamReader(fstream));

		String strLine;

		//Read File Line By Line
		int lineIndex = 0;
		while ((strLine = br.readLine()) != null)   {
			// read in the line and parse the results
			lineIndex++;
			String[] parsedLine = null;
			try {
				parsedLine = strLine.split(" ");
			} catch (Exception e) {
				System.out.println("Wrong Format: line #" + lineIndex);
			}
			double tfidf;
			try {
				tfidf = Double.parseDouble(parsedLine[2]);
			} catch (Exception e) {
				System.out.println("Wrong Format: line #" + lineIndex);
				tfidf = 0.01;
			}
			if (results.containsKey(parsedLine[0])) {
				// if it is a old result, we simple add to the map
				results.get(parsedLine[0]).put(parsedLine[1], tfidf);
			} else {
				// if it is a new result, we need to put a new elemenet in it
				HashMap<String, Double> newMapping = new HashMap<>();
				newMapping.put(parsedLine[1], tfidf);
				results.put(parsedLine[0], newMapping);
			}
		}
		//Close the input stream
		br.close();

		return results;
	}

	@Override
	public HashMap<String, Float> loadPageRank(String directory) {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public ArrayList<String> getUrlSearchResults() {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public HashMap<String, String> getContentSearchResults(ArrayList<String> urlResults) {
		// TODO Auto-generated method stub
		return null;
	}

}
