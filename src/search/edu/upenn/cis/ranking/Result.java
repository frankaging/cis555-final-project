package search.edu.upenn.cis.ranking;
/**
 * The result that renders on the web from search
 * @author kevin ye
 *
 */
public class Result {
	
	private String url;
	private String title;
	private String snippet;
	
	public Result(String url, String title, String snippet){
		this.url = url;
		this.title = title;
		this.snippet = snippet;
	}
	
	public String getUrl() {
		return this.url;
	}
	
	public String getTitle() {
		return this.title;
	}
	
	public String getSnippet() {
		return this.snippet;
	}

}
