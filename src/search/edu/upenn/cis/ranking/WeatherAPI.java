package search.edu.upenn.cis.ranking;

import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.io.Reader;
import java.io.StringWriter;
import java.net.HttpURLConnection;
import java.net.URL;
import org.json.simple.JSONArray;
import org.json.simple.JSONObject;
import org.json.simple.parser.JSONParser;
import org.json.simple.parser.ParseException;
/**
 * A helper class to call openweather api
 * @author kevin ye
 *
 */
public class WeatherAPI {

	private static final String APPID_HEADER = "fb6f84449a540ac2c62dd80a8e292b2b";

	private String baseOwmUrl = "http://api.openweathermap.org/data/2.5/";

	public WeatherAPI() {

	}

	/**
	 * Find current city weather
	 * 
	 * @param cityName
	 *            is the name of the city
	 * @return the array of weather condition
	 * 
	 * @throws IOException
	 *             if there's some network error or the OWM server replies with
	 *             a error.
	 */
	public String[] currentWeatherAtCity(String cityName) throws IOException {
		String subUrl = "weather?q=" + cityName + "&appid=" + WeatherAPI.APPID_HEADER + "&units=Imperial";
		// doQuery(subUrl);
		JSONObject response = doQuery(subUrl);
		JSONParser parser = new JSONParser();
		JSONObject temp = null;
		String[] results = new String[2];
		try {
			temp = (JSONObject) parser.parse(response.get("main").toString());
			results[0] = temp.get("temp").toString();
			JSONArray weather = (JSONArray) response.get("weather");
			JSONObject obj = (JSONObject) weather.get(0);
			results[1] = obj.get("description").toString();
		} catch (ParseException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		
		return results;
	}


	/*
	 * Send request to API and parse response into a JSONObject
	 */
	private JSONObject doQuery(String subUrl) throws IOException {
		String responseBody = null;

		InputStream contentStream = null;
		URL url = new URL(this.baseOwmUrl + subUrl);

		HttpURLConnection conn = (HttpURLConnection) url.openConnection();
		conn.setRequestMethod("GET");

		if (conn.getResponseCode() != HttpURLConnection.HTTP_OK) {
			throw new RuntimeException("Weather api fails");
		}

		contentStream = conn.getInputStream();

		Reader isReader = new InputStreamReader(contentStream);
		int contentSize = conn.getContentLength();
		if (contentSize < 0)
			contentSize = 8 * 1024;
		StringWriter strWriter = new StringWriter(contentSize);
		char[] buffer = new char[8 * 1024];
		int n = 0;
		while ((n = isReader.read(buffer)) != -1) {
			strWriter.write(buffer, 0, n);
		}
		responseBody = strWriter.toString();
		contentStream.close();
		
		JSONParser parser = new JSONParser();
		JSONObject response = null;
		try {
			response = (JSONObject) parser.parse(responseBody);
		} catch (ParseException pe) {
			System.out.println("Error: could not parse JSON response:");
			System.exit(1);
		}

		return response;
	}
}
