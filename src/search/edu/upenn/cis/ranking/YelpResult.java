package search.edu.upenn.cis.ranking;
/**
 * Stores the info needed to be displayed from yelp
 * @author kevin ye
 *
 */
public class YelpResult {
	
	private String name;
	private String url;
	private String imageUrl;
	private String snippet;
	private double rating;
	
	public YelpResult(String name, String url, String imageUrl, double rating, String snippet){
		this.name = name;
		this.url = url;
		this.imageUrl = imageUrl;
		this.rating = rating;
		this.snippet = snippet;
	}
	
	public void setName(String name){
		this.name = name;
	}
	
	public String getName(){
		return this.name;
	}
	
	public String getUrl() {
		return url;
	}

	public void setUrl(String url) {
		this.url = url;
	}

	public String getImageUrl() {
		return imageUrl;
	}

	public void setImageUrl(String imageUrl) {
		this.imageUrl = imageUrl;
	}

	public String getSnippet() {
		return snippet;
	}

	public void setSnippet(String snippet) {
		this.snippet = snippet;
	}

	public double getRating() {
		return rating;
	}

	public void setRating(double rating) {
		this.rating = rating;
	}
}
