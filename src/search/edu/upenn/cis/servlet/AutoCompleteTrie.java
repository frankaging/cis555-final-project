package search.edu.upenn.cis.servlet;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.IOException;
import java.util.ArrayList;
/**
 * A trie data structure to store dictionary for autocomplete
 * @author kevin ye
 *
 */
public class AutoCompleteTrie {

	private static AutoCompleteTrie instance;
	private TrieNode root;
	
//	public static void main(String[] args){
//		AutoCompleteTrie t = AutoCompleteTrie.getInstance();
//		ArrayList<String> l = t.startsWith("su");
//		for (String s: l){
//			System.out.println(s);
//		}
//	}

	private AutoCompleteTrie() {
		root = new TrieNode();
		File file = new File("../words.txt");
		try {
			BufferedReader bf = new BufferedReader(new FileReader(file));
			String s = bf.readLine();
			while (s != null) {
				this.insert(s);
				s = bf.readLine();
			}
			bf.close();
		} catch (FileNotFoundException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}

	}

	public static AutoCompleteTrie getInstance() {
		if (instance == null) {
			instance = new AutoCompleteTrie();
		}

		return instance;
	}

	/*
	 * Insert a new word into the trie
	 */
	public void insert(String word) {
		TrieNode p = root;
		
		for (int i = 0; i < word.length(); i++) {
			char c = word.charAt(i);
			int index = c - 'a';
			if (p.arr[index] == null) {
				TrieNode temp = new TrieNode();
				p.arr[index] = temp;
				p = temp;
			} else {
				p = p.arr[index];
			}

		}
		p.word = word;
		p.isEnd = true;
	}
	
	/*
	 * Retrieve list of words from dictionary start with prefix
	 */
	public ArrayList<String> startsWith(String prefix) {
		ArrayList<String> l = new ArrayList<>();
        
        if (prefix == null || prefix.length() == 0) return l;

        TrieNode p = root;
        for (int i = 0; i < prefix.length(); i++){
            char c = prefix.charAt(i);
            int index = c - 'a';
            if (p.arr[index] == null){
                return null;
            } else {
                p = p.arr[index];
            }
        }
        
        getAll(p, l, 10);

        return l;
    }
	
	/*
	 * Helper method to retrieve all words
	 */
	public void getAll(TrieNode p, ArrayList<String> l, int limit){
		if (l.size() == limit) {
			return;
		}
		if (p.isEnd) {
			l.add(p.word);
		}
		for (TrieNode next: p.arr){
        	if (next != null){
        		getAll(next, l, limit);
        	}
        }
	}

    public TrieNode searchNode(String s){
        if (s == null) return null;

        TrieNode p = root;

        for (int i = 0; i < s.length(); i++){
            char c = s.charAt(i);
            int index = c - 'a';
            if (p.arr[index] == null){
                return null;
            } else {
                p = p.arr[index];
            }
        }

        return p;
    }

}
