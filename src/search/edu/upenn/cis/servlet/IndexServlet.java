package search.edu.upenn.cis.servlet;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileReader;
import java.io.IOException;
import java.io.PrintWriter;
import java.net.InetAddress;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.HashSet;
import java.util.List;
import java.util.Set;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import com.google.gson.Gson;
import com.maxmind.geoip2.WebServiceClient;
import com.maxmind.geoip2.exception.GeoIp2Exception;
import com.maxmind.geoip2.model.CityResponse;
import com.maxmind.geoip2.record.City;
import com.maxmind.geoip2.record.Subdivision;

import search.edu.upenn.cis.ranking.Result;
import search.edu.upenn.cis.ranking.WeatherAPI;
import search.edu.upenn.cis.ranking.YelpAPI;
import search.edu.upenn.cis.ranking.YelpResult;

/**
 * Main servlet of the search engine, contains of three components 1. home page
 * 2. result page 3. search, which is a post request
 * 
 * @author kevin ye
 *
 */
public class IndexServlet extends HttpServlet {

	/**
	 * 
	 */
	public static String[] stopwords = {"a", "as", "able", "about", "above", "according", "accordingly", "across", "actually", "after", "afterwards", "again", "against", "aint", "all", "allow", "allows", "almost", "alone", "along", "already", "also", "although", "always", "am", "among", "amongst", "an", "and", "another", "any", "anybody", "anyhow", "anyone", "anything", "anyway", "anyways", "anywhere", "apart", "appear", "appreciate", "appropriate", "are", "arent", "around", "as", "aside", "ask", "asking", "associated", "at", "available", "away", "awfully", "be", "became", "because", "become", "becomes", "becoming", "been", "before", "beforehand", "behind", "being", "believe", "below", "beside", "besides", "best", "better", "between", "beyond", "both", "brief", "but", "by", "cmon", "cs", "came", "can", "cant", "cannot", "cant", "cause", "causes", "certain", "certainly", "changes", "clearly", "co", "com", "come", "comes", "concerning", "consequently", "consider", "considering", "contain", "containing", "contains", "corresponding", "could", "couldnt", "course", "currently", "definitely", "described", "despite", "did", "didnt", "different", "do", "does", "doesnt", "doing", "dont", "done", "down", "downwards", "during", "each", "edu", "eg", "eight", "either", "else", "elsewhere", "enough", "entirely", "especially", "et", "etc", "even", "ever", "every", "everybody", "everyone", "everything", "everywhere", "ex", "exactly", "example", "except", "far", "few", "ff", "fifth", "first", "five", "followed", "following", "follows", "for", "former", "formerly", "forth", "four", "from", "further", "furthermore", "get", "gets", "getting", "given", "gives", "go", "goes", "going", "gone", "got", "gotten", "greetings", "had", "hadnt", "happens", "hardly", "has", "hasnt", "have", "havent", "having", "he", "hes", "hello", "help", "hence", "her", "here", "heres", "hereafter", "hereby", "herein", "hereupon", "hers", "herself", "hi", "him", "himself", "his", "hither", "hopefully", "how", "howbeit", "however", "i", "id", "ill", "im", "ive", "ie", "if", "ignored", "immediate", "in", "inasmuch", "inc", "indeed", "indicate", "indicated", "indicates", "inner", "insofar", "instead", "into", "inward", "is", "isnt", "it", "itd", "itll", "its", "its", "itself", "just", "keep", "keeps", "kept", "know", "knows", "known", "last", "lately", "later", "latter", "latterly", "least", "less", "lest", "let", "lets", "like", "liked", "likely", "little", "look", "looking", "looks", "ltd", "mainly", "many", "may", "maybe", "me", "mean", "meanwhile", "merely", "might", "more", "moreover", "most", "mostly", "much", "must", "my", "myself", "name", "namely", "nd", "near", "nearly", "necessary", "need", "needs", "neither", "never", "nevertheless", "new", "next", "nine", "no", "nobody", "non", "none", "noone", "nor", "normally", "not", "nothing", "novel", "now", "nowhere", "obviously", "of", "off", "often", "oh", "ok", "okay", "old", "on", "once", "one", "ones", "only", "onto", "or", "other", "others", "otherwise", "ought", "our", "ours", "ourselves", "out", "outside", "over", "overall", "own", "particular", "particularly", "per", "perhaps", "placed", "please", "plus", "possible", "presumably", "probably", "provides", "que", "quite", "qv", "rather", "rd", "re", "really", "reasonably", "regarding", "regardless", "regards", "relatively", "respectively", "right", "said", "same", "saw", "say", "saying", "says", "second", "secondly", "see", "seeing", "seem", "seemed", "seeming", "seems", "seen", "self", "selves", "sensible", "sent", "serious", "seriously", "seven", "several", "shall", "she", "should", "shouldnt", "since", "six", "so", "some", "somebody", "somehow", "someone", "something", "sometime", "sometimes", "somewhat", "somewhere", "soon", "sorry", "specified", "specify", "specifying", "still", "sub", "such", "sup", "sure", "ts", "take", "taken", "tell", "tends", "th", "than", "thank", "thanks", "thanx", "that", "thats", "thats", "the", "their", "theirs", "them", "themselves", "then", "thence", "there", "theres", "thereafter", "thereby", "therefore", "therein", "theres", "thereupon", "these", "they", "theyd", "theyll", "theyre", "theyve", "think", "third", "this", "thorough", "thoroughly", "those", "though", "three", "through", "throughout", "thru", "thus", "to", "together", "too", "took", "toward", "towards", "tried", "tries", "truly", "try", "trying", "twice", "two", "un", "under", "unfortunately", "unless", "unlikely", "until", "unto", "up", "upon", "us", "use", "used", "useful", "uses", "using", "usually", "value", "various", "very", "via", "viz", "vs", "want", "wants", "was", "wasnt", "way", "we", "wed", "well", "were", "weve", "welcome", "well", "went", "were", "werent", "what", "whats", "whatever", "when", "whence", "whenever", "where", "wheres", "whereafter", "whereas", "whereby", "wherein", "whereupon", "wherever", "whether", "which", "while", "whither", "who", "whos", "whoever", "whole", "whom", "whose", "why", "will", "willing", "wish", "with", "within", "without", "wont", "wonder", "would", "would", "wouldnt", "yes", "yet", "you", "youd", "youll", "youre", "youve", "your", "yours", "yourself", "yourselves", "zero"};
	public static Set<String> stopWordSet = new HashSet<String>(Arrays.asList(stopwords));
	private static final long serialVersionUID = 1L;
	private final int itemPerPage = 10;
	private static AutoCompleteTrie autocomplete;
	private String location = "Philadelphia, PA";
	private String cityName = "Philadelphia";
	private WeatherAPI weather;
	String sky = "";
	String temperature = "";
	String searchWord = "";
	List<Result> searchResults;
	List<String> queries;
	List<YelpResult> yelpResults;
	YelpAPI yelpApi;

	@Override
	public void init() {
		searchResults = new ArrayList<>();
		queries = new ArrayList<>();
		yelpApi = YelpAPI.getInstance();
		yelpResults = new ArrayList<>();
		autocomplete = AutoCompleteTrie.getInstance();
		weather = new WeatherAPI();

		Result r1 = new Result("www.google.com", "google", "best search engine ever");
		Result r2 = new Result("www.facebook.com", "facebook", "best social media");
		for (int i = 0; i < 21; i++){
			searchResults.add(r1);
			searchResults.add(r2);
		}
	}

	@Override
	public void doGet(HttpServletRequest request, HttpServletResponse response) throws IOException, ServletException {
		String uri = request.getRequestURI();
		if (uri.contains("/home")) {
			PrintWriter out = response.getWriter();
			response.setContentType("text/html");
			File file = new File("../src/search/edu/upenn/cis/servlet/index.html");
			BufferedReader bf = new BufferedReader(new FileReader(file));
			String line = bf.readLine();
			while (line != null) {
				out.println(line);
				line = bf.readLine();
			}
			bf.close();
		} else if (uri.contains("controller")) {
			response.setContentType("application/json");
			String term = request.getParameter("term").trim();
            System.out.println("Data from ajax call " + term);
            
            if (term.length() > 1){
            	ArrayList<String> l = autocomplete.startsWith(term);
        		String json = new Gson().toJson(l);
        		
        		response.getWriter().write(json);
            }
		} else {
			System.out.println(uri);
			if (uri.contains(".result")) {
				int index = uri.indexOf(".result");
				renderSearchResult(request, response, Integer.parseInt(uri.substring(index - 1, index)));
			}

		}

	}

	@Override
	public void doPost(HttpServletRequest request, HttpServletResponse response) throws IOException {
		String uri = request.getRequestURI();
		if (uri.contains("/searching")) {
			String query = request.getParameter("search").trim().toLowerCase();
			searchWord = query;
			String[] keyWords = query.split(" ");
			// do ranking algorithm
			for (String key: keyWords){
				if (!stopWordSet.contains(key)){
					queries.add(key);
				}
			}
//			queries = Arrays.asList(keyWords);
			// testing needed for location finder
			if (!request.getRemoteAddr().equals("127.0.0.1")) {
				try (WebServiceClient client = new WebServiceClient.Builder(118479, "7IoWQlNFprHe").build()) {

					InetAddress ipAddress = InetAddress.getByName(request.getRemoteAddr());

					// Do the lookup
					CityResponse cityResponse;
					try {
						cityResponse = client.city(ipAddress);

						Subdivision subdivision = cityResponse.getMostSpecificSubdivision();
						System.out.println(subdivision.getIsoCode()); // 'MN'

						City city = cityResponse.getCity();
						System.out.println(city.getName()); // 'Minneapolis'
						location = city.getName() + ", " + subdivision.getIsoCode();
						cityName = city.getName();
					} catch (GeoIp2Exception e) {
						// TODO Auto-generated catch block
						e.printStackTrace();
					}

				}
			}
			String[] weatherResults = weather.currentWeatherAtCity(cityName);
			this.temperature = weatherResults[0];
			this.sky = weatherResults[1];
			yelpResults = new ArrayList<>(yelpApi.queryAPI(yelpApi, query, location));
		}
		response.sendRedirect("1.result");
	}

	/*
	 * renders the results found based on our algorithm and yelpAPI
	 */
	private void renderSearchResult(HttpServletRequest request, HttpServletResponse response, int index)
			throws IOException, ServletException {
		// PrintWriter out = response.getWriter();

		List<Result> display = new ArrayList<>();
		int pos = (index - 1) * itemPerPage;
		int size = this.searchResults.size();
		for (int i = pos; i < size && i < pos + itemPerPage; i++) {
			display.add(this.searchResults.get(i));
		}
		if (size % itemPerPage == 0) {
			size /= itemPerPage;
		} else {
			size = 1 + size / itemPerPage;
		}
		int start = index - 5;
		if (start < 0) {
			start = 1;
		}
		int diff = index - start;

		size = Math.min(size, index + 10 - diff);
		request.setAttribute("temperature", temperature);
		request.setAttribute("sky", sky);
		request.setAttribute("keyword", searchWord);
		request.setAttribute("start", start);
		request.setAttribute("next", index + 1);
		request.setAttribute("current", index);
		request.setAttribute("size", size);
		request.setAttribute("results", display);
		request.setAttribute("yelp", yelpResults);
		request.getRequestDispatcher("result.jsp").forward(request, response);
	}

}
