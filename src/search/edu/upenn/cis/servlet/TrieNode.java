package search.edu.upenn.cis.servlet;
/**
 * Node that stores in AutoCompleteTrie
 * @author kevin ye
 *
 */
public class TrieNode {
	TrieNode[] arr;

    boolean isEnd;
    String word;

    public TrieNode() {

        arr = new TrieNode[26];
    }
}
