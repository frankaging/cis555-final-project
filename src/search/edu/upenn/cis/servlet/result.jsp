<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<%@taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@page import="java.util.ArrayList"%>
<%@page import="java.util.List"%>
<%@page import="search.edu.upenn.cis.ranking.YelpResult"%>
<%@page import="search.edu.upenn.cis.ranking.Result"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
<link rel="stylesheet"
	href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css"
	integrity="sha384-BVYiiSIFeK1dGmJRAkycuHAHRg32OmUcww7on3RYdg4Va+PmSTsz/K68vbdEjh4u"
	crossorigin="anonymous">

<script src="//code.jquery.com/jquery-1.10.2.js"></script>
<script src="//code.jquery.com/ui/1.10.4/jquery-ui.js"></script>
<link rel="stylesheet"
	href="//code.jquery.com/ui/1.10.4/themes/smoothness/jquery-ui.css">
<style>
body {
	padding-top: 70px;
	overflow: auto;
	/* Required padding for .navbar-fixed-top. Remove if using .navbar-static-top. Change if height of navigation changes. */
}

.footer {
	position: relative;
	width: 100%;
	/* Set the fixed height of the footer here */
	height: 60px;
	bottom: 0px;
	margin-top: 10px; /* negative value of footer height */
 	clear: both;
	background-color: #f5f5f5;
}

.footer-box {
	width: auto;
	max-width: 680px;
	padding: 2px 15px;
}

.footer-box .text-muted {
	margin: 20px 0;
}

.search-result {
	margin-bottom: 15px;
	margin-left: 15px;
	maring-right: 15px;
}

td {
	padding-right: 5px;
}

.generic {
	border-bottom-style: outset;
}

.api {
	margin-bottom: 15px; 
	padding-right: 15px; 
	padding-left: 15px;
}

.url {
	color: #3D9970;
	line-height: 90%;
}

.index-table {
	
}
</style>

<title>Result</title>
</head>

<body>
	<nav class="navbar navbar-inverse navbar-fixed-top" role="navigation">
	<div class="container">
		<!-- Brand and toggle get grouped for better mobile display -->
		<div class="navbar-header">
			<button type="button" class="navbar-toggle" data-toggle="collapse"
				data-target="#bs-example-navbar-collapse-1">
				<span class="sr-only">Toggle navigation</span> <span
					class="icon-bar"></span> <span class="icon-bar"></span> <span
					class="icon-bar"></span>
			</button>
			<p class="navbar-brand">CIS555 Web Search Engine</p>
		</div>
	</div>
	</nav>
	<div class="container search-container" style="padding-top: 15px">
		<form class="form-inline" method="POST" action="searching">
			<div class="form-group ui-widget">
				<%
					String keyword = (String) request.getAttribute("keyword");
				%>
				<label class="sr-only" for="searchQuery">query</label> <input
					type="text" class="form-control" name="search" id="search"
					placeholder="keyword" value=<%=keyword%> style="width: 500px;">
			</div>
			<button type="submit" class="btn btn-primary">Search</button>
		</form>
	</div>

	<div class="container" style="padding-top: 50px">
		<div class="col-md-8">
			<c:if test="${size == 0 }">
				<h4>No result found, sorry!</h4>
			</c:if>
			<c:forEach items="${results}" var="value">
				<div class="row search-result generic">
					<h3>
						<a href=<c:out value="${value.url}" />> <c:out
								value="${value.title}" />
						</a>
					</h3>
					<p class="url">
						<c:out value="${value.url}" />
					</p>
					<p class="mb-0">
						<c:out value="${value.snippet}" />
					</p>
				</div>
			</c:forEach>
		</div>
		<div class="col-md-4">
			<h4>The weather is <%= request.getAttribute("temperature") %> degree, <%= request.getAttribute("sky") %></h4>
			<br />
			<c:forEach items="${yelp}" var="yelpValue">
				<div class="row api">
					<figure class="figure"> <img
						src=<c:out value="${yelpValue.imageUrl}"/>
						class="figure-img img-fluid rounded" width="200" height="200"
						alt="A generic square placeholder image with rounded corners in a figure.">
					<figcaption class="figure-caption text-xs-right"> <a
						href=<c:out value="${yelpValue.url}"/>><c:out
							value="${yelpValue.name}" /></a></figcaption> </figure>
				</div>
			</c:forEach>
		</div>
	</div>

	<div class="container table-responsive">
		<div class="col-md-8" style="margin-left: 15px;">
			<table sytle="width: 10%">
				<tbody>
					<tr>
						<c:forEach begin="${start}" end="${size}" varStatus="loop">
							<c:choose>
								<c:when test="${loop.index != current}">
									<td><a href=<c:out value="${loop.index}"  />.result>${loop.index}</a></td>
								</c:when>
								<c:otherwise>
									<td>${loop.index}</td>
								</c:otherwise>
							</c:choose>
							<c:if test="${loop.index == size }">
								<td><a href=<c:out value="${next}" />.result>
										<p class="glyphicon glyphicon-chevron-right"></p>
								</a></td>
							</c:if>
						</c:forEach>

					</tr>
				</tbody>
			</table>
		</div>
	</div>

	<footer class="footer">
	<div class="footer-box">
		<p class="text-muted">Embeded with MaxMind, OpenWeather and Yelp API</p>
	</div>
	</footer>

</body>
<script>
	$(document).ready(function() {
		$(function() {
			$("#search").autocomplete({
				source : function(request, response) {
					$.ajax({
						url : "controller",
						type : "GET",
						data : {
							term : request.term
						},
						dataType : "json",
						success : function(data) {
							console.log(data);
							response(data);
						}
					});
				}
			});
		});
	});
</script>
<script
	src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/js/bootstrap.min.js"
	integrity="sha384-Tc5IQib027qvyjSMfHjOMaLkfuWVxZxUPnCJA7l2mCWNIpG9mGCD8wGNIcPD7Txa"
	crossorigin="anonymous"></script>
</html>